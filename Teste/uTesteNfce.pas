unit uTesteNfce;

interface
uses
  DUnitX.TestFramework, uCupomVO, uCupomCTR, uNfceVO ,uNfceCtr , UBanco,
  SysUtils, ACBrNFe;

type

  [TestFixture]
  TTesteCupom = class(TObject)
  strict private
    CupomCtr: TCupomCTR;
    Cupom: TCupomVO;
    NfceCtr: TNfceCtr;
    Nfce: TNfceVO;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    function GetBanco: TBanco;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure Test1;
    // Test with TestCase Attribute to supply parameters.
    [Test]
    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);
  end;

implementation

{ TTesteCupom }

function TTesteCupom.GetBanco: TBanco;
begin
  Result := TBanco.Create;
  Result.usuario := 'sysdba';
  Result.senha := 'masterkey';
  Result.host := 'localhost';
  Result.banco := GetCurrentDir + '\DB\DB_PAF_ECF.FDB';
end;

procedure TTesteCupom.Setup;
begin
  CupomCtr := TCupomCTR.Create(GetBanco);
  Cupom := TCupomVO.Create;
  NfceCtr := TNfceCtr.Create(GetBanco);
  Nfce := TNfceVO.Create;
end;

procedure TTesteCupom.TearDown;
begin

end;

procedure TTesteCupom.Test1;
begin
  Cupom := CupomCtr.SelectCupom('FCuponsId', '134');
  Assert.IsNotNull(Cupom);
end;

procedure TTesteCupom.Test2(const AValue1, AValue2: Integer);
var
  ACBrNFe: TACBrNFe;
begin
  Cupom := CupomCtr.SelectCupom('FCuponsId', '126');
  ACBrNFe := NfceCtr.PreencheNfce(Cupom);
  Assert.IsNotNull(ACBrNFe);
  ACBrNFe.NotasFiscais.Assinar;
end;

end.
