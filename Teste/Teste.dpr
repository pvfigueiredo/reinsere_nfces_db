program Teste;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}{$STRONGLINKTYPES ON}
uses
  System.SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ENDIF }
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  DUnitX.TestFramework,
  uTesteReinsereNfce in 'uTesteReinsereNfce.pas',
  uTesteNfce in 'uTesteNfce.pas',
  JsonToObj in 'C:\Users\programacao05\Documents\Embarcadero\Studio\Projects\JsonTeste\JsonToObj.pas',
  uTesteCupom in 'uTesteCupom.pas',
  uAreceberVO in '..\models\uAreceberVO.pas',
  uClientesVO in '..\models\uClientesVO.pas',
  uCupomVo in '..\models\uCupomVo.pas',
  uEmpresaVO in '..\models\uEmpresaVO.pas',
  uFormasPagamentoVO in '..\models\uFormasPagamentoVO.pas',
  uItensCuponsVO in '..\models\uItensCuponsVO.pas',
  uMovimentosVO in '..\models\uMovimentosVO.pas',
  uNfceAnteriorVO in '..\models\uNfceAnteriorVO.pas',
  UNfceVO in '..\models\UNfceVO.pas',
  uSerieNfceVO in '..\models\uSerieNfceVO.pas',
  uVendasCartoesVO in '..\models\uVendasCartoesVO.pas',
  uCupomDao in '..\dao\uCupomDao.pas',
  uEmpresaDao in '..\dao\uEmpresaDao.pas',
  uNfceDao in '..\dao\uNfceDao.pas',
  uSerieNfceDAO in '..\dao\uSerieNfceDAO.pas',
  uCupomCtr in '..\controllers\uCupomCtr.pas',
  uEmpresaCtr in '..\controllers\uEmpresaCtr.pas',
  uMainCtr in '..\controllers\uMainCtr.pas',
  uManipulaNfce in '..\controllers\uManipulaNfce.pas',
  uNfceCtr in '..\controllers\uNfceCtr.pas',
  uSerieNfceCtr in '..\controllers\uSerieNfceCtr.pas',
  uXml in '..\controllers\uXml.pas',
  UAureliusConnection in '..\..\classes_facil_sistemas\MVC\aurelius_connection\UAureliusConnection.pas' {AureliusConnection: TDataModule},
  Database.Session in '..\..\classes_facil_sistemas\MVC\implementation\Database.Session.pas',
  uConnection in '..\..\classes_facil_sistemas\MVC\implementation\uConnection.pas',
  UGenericoDAO in '..\..\classes_facil_sistemas\MVC\implementation\UGenericoDAO.pas',
  Database.Session.Types in '..\..\classes_facil_sistemas\MVC\Interfaces\Database.Session.Types.pas',
  UICTR in '..\..\classes_facil_sistemas\MVC\Interfaces\UICTR.pas',
  UIDAO in '..\..\classes_facil_sistemas\MVC\Interfaces\UIDAO.pas',
  uOperadorasCartaoVO in '..\models\uOperadorasCartaoVO.pas',
  uProdutosVO in '..\models\uProdutosVO.pas',
  uCodigoAnpVO in '..\models\uCodigoAnpVO.pas',
  uProdutosSeriaisVO in '..\models\uProdutosSeriaisVO.pas',
  uItensCuponsCtr in '..\controllers\uItensCuponsCtr.pas',
  uVendasCartoesCTR in '..\controllers\uVendasCartoesCTR.pas',
  UInutilizacoes_CTR in '..\unitsLegadas\UInutilizacoes_CTR.pas',
  UInutilizacoes_DAO in '..\unitsLegadas\UInutilizacoes_DAO.pas',
  UInutilizacoes_VO in '..\unitsLegadas\UInutilizacoes_VO.pas',
  uUnidadesVO in '..\models\uUnidadesVO.pas',
  uAreceberCtr in '..\controllers\uAreceberCtr.pas',
  uOperadorasCartaoCTR in '..\controllers\uOperadorasCartaoCTR.pas',
  uContadoresVO in '..\models\uContadoresVO.pas',
  uLogs in '..\..\classes_facil_sistemas\MVC\log\uLogs.pas',
  UConfiguracoes_CTR in '..\unitsLegadas\UConfiguracoes_CTR.pas',
  UConfiguracoes_DAO in '..\unitsLegadas\UConfiguracoes_DAO.pas',
  UConfiguracoes_VO in '..\unitsLegadas\UConfiguracoes_VO.pas',
  UConfiguracoesSistema in '..\unitsLegadas\UConfiguracoesSistema.pas',
  uItensCuponsDao in '..\dao\uItensCuponsDao.pas',
  uAReceberDao in '..\dao\uAReceberDao.pas',
  uVendasCartoesDao in '..\dao\uVendasCartoesDao.pas',
  uConfiguracoesVO in '..\models\uConfiguracoesVO.pas',
  uConfiguracoesCtr in '..\controllers\uConfiguracoesCtr.pas';

var
  runner : ITestRunner;
  results : IRunResults;
  logger : ITestLogger;
  nunitLogger : ITestLogger;
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
  exit;
{$ENDIF}
  try
    //Check command line options, will exit if invalid
    TDUnitX.CheckCommandLine;
    //Create the test runner
    runner := TDUnitX.CreateRunner;
    //Tell the runner to use RTTI to find Fixtures
    runner.UseRTTI := True;
    //tell the runner how we will log things
    //Log to the console window
    logger := TDUnitXConsoleLogger.Create(true);
    runner.AddLogger(logger);
    //Generate an NUnit compatible XML File
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);
    runner.FailsOnNoAsserts := False; //When true, Assertions must be made during tests;

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
end.
