unit uTesteCupom;

interface
uses
  DUnitX.TestFramework, uCupomVO, uCupomCTR, UBanco, SysUtils;

type

  [TestFixture]
  TTesteCupom = class(TObject)
  strict private
    Cupom: TCupomCTR;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    function GetBanco: TBanco;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure Test1;
    // Test with TestCase Attribute to supply parameters.
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);
  end;

implementation

function TTesteCupom.GetBanco: TBanco;
begin
  Result := TBanco.Create;
  Result.usuario := 'sysdba';
  Result.senha := 'masterkey';
  Result.host := 'localhost';
  Result.banco := GetCurrentDir + '\DB\DB_PAF_ECF.FDB';
end;

procedure TTesteCupom.Setup;
begin
  Cupom := TCupomCTR.Create(GetBanco);
end;

procedure TTesteCupom.TearDown;
begin
end;

procedure TTesteCupom.Test1;
begin
end;

procedure TTesteCupom.Test2(const AValue1 : Integer;const AValue2 : Integer);
begin
  Cupom.SelectCupom('FCuponsId', '513')
end;

initialization
  TDUnitX.RegisterTestFixture(TTesteCupom);
end.
