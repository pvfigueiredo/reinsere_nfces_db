unit JsonToObj;

interface

uses
  SysUtils, Generics.Collections, Classes, Rest.Json;

type
  TOrder = class
    Atributo: string;
    Ordenacao: string;
  end;
  TFilter = class
    Item: string;
    Filtro: string;
  end;
  TClasse = class
    Order: TOrder;
    Filter: TFilter;
    Select: TArray<string>;

    function ObjetoParaJson(Objeto: TObject): TStringList;
  end;

implementation

{ TClasse }

function TClasse.ObjetoParaJson(Objeto: TObject): TStringList;
begin
  Result.Text := TJson.ObjectToJsonString(Objeto);
end;

end.
