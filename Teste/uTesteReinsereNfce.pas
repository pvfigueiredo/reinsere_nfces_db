unit uTesteReinsereNfce;

interface
uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TTesteReinsereNfce = class(TObject) 
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
  end;

implementation

procedure TTesteReinsereNfce.Setup;
begin
end;

procedure TTesteReinsereNfce.TearDown;
begin
end;


initialization
  TDUnitX.RegisterTestFixture(TTesteReinsereNfce);
end.
