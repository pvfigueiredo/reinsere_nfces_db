unit uMainCtr;

interface

uses
  SysUtils, Generics.Collections, uNfceVO, UBanco, uNfceCtr, uEmpresaCtr,
  uEmpresaVO, uCupomVO, uCupomCtr, uSerieNfceCTR, uSerieNfceVO,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq, ACBrNFe, uLogs, dialogs,
  uFormasPagamentoVO, SHdocVw, ACBrUtil, Forms, Classes;

type
  TMainCtr = class
  private
     FBanco: TBanco;
     FNfceCtr: TNfceCtr;
     FEmpresaCtr: TEmpresaCtr;
     FCupomCtr: TCupomCtr;
     FSerieNfce: TSerieNfceVO;
     procedure CarregaBanco(CaminhoDB: string);
     function GetHost(CaminhoDB: string): string;
     function ConfiguraCaminhoBanco(CaminhoDB: string): string;
     procedure AtualizaRetornoNfce(ACBrNFe: TACBrNFe);
     function ExtraiNumeroNfce(ChaveAcesso: string): integer;
  public
    constructor Create(CaminhoDB: string);
    destructor Destroy; override;
    function GetListaNfce: TObjectList<TNfceVO>;
    function GetListaNfceSemChave: TObjectList<TNfceVO>;
    function GetCnpj: string;
    function CalculaChaveAcesso(Nfce: TNfceVO): string;
    function SalvaNfce(Nfce: TNfceVO): boolean;
    function AtualizaNfce(Nfce: TNfceVO): boolean;
    function GetCupom(Nfce: TNfceVO; IdAnterior: integer): TCupomVO; overload;
    function SalvaCupom(Cupom: TCupomVO): boolean;
    function GetCupom(WhereKey, WhereValue: string): TCupomVO; overload;
    function DateTimeToDate(const Date: TDateTime): TDate;
    function GeraXml(Cupom: TCupomVO; DigValue, DirXml: string): boolean;
    function GetUltimaNumeracaoNfce: integer;
    function FormaPagamentoFactory: TFormasPagamentoVO;
    function GetRetornoNfce(Nfce: TNfceVO;
      var DigValue: string): TNfceVO; overload;
    function GetXmls(DirXml: string): TStringList;
    procedure AssinaXml(const DirXml: string);
    function ConsultaDigValue(DigValue: string): string;

  end;

implementation

{ TMainCtr }

procedure TMainCtr.AssinaXml(const DirXml: string);
var
  ACBrNFe: TACBrNFe;
  Xmls: TStringList;
  Xml: string;
  Arr: TArray<string>;
  DigValue: string;
begin
  try
    Xmls := GetXmls(DirXml);
    for Xml in Xmls do
    begin
      Arr := Xml.Split(['\', '-', '.']);
      DigValue := FNfceCtr.ConsultaPorChave(Arr[Length(Arr) - 3]).
        WebServices.Consulta.protNFe.digVal;
      ACBrNFe := FNfceCtr.ConsultaPorXml(Xml, DigValue);
      AtualizaRetornoNfce(ACBrNFe);
    end;
  except
    on E: Exception do
    begin
      raise;
    end;

  end;
end;

function TMainCtr.AtualizaNfce(Nfce: TNfceVO): boolean;
begin
  Result := FNfceCtr.Update(Nfce);
end;

procedure TMainCtr.AtualizaRetornoNfce(ACBrNFe: TACBrNFe);
var
  Nfce: TNfceVO;
  NfceNumero: integer;
begin
  try
    NfceNumero := ExtraiNumeroNfce(ACBrNFe.WebServices.Consulta.NFeChave);
    Nfce := FNfceCtr.SelectNfce(Linq['FNfceNumero'] = NfceNumero);
    Nfce.NfceCodigoRetorno := ACBrNFe.WebServices.Consulta.cStat.ToString;
    Nfce.NfceProtocolo := ACBrNFe.WebServices.Consulta.Protocolo;
    FNfceCtr.Save(Nfce);
  except
    on E: Exception do
    begin
      raise;
    end;
  end;
end;

function TMainCtr.CalculaChaveAcesso(Nfce: TNfceVO): string;
begin
  Result := FNfceCtr.CalculaChaveAcesso(Nfce);
end;

procedure TMainCtr.CarregaBanco(CaminhoDB: string);
begin
  FBanco := TBanco.Create;
  FBanco.usuario := 'sysdba';
  FBanco.senha := 'masterkey';
  FBanco.host := GetHost(CaminhoDB);
  FBanco.banco := ConfiguraCaminhoBanco(CaminhoDB);
  FBanco.charset := chsPDV;
end;

function TMainCtr.ConfiguraCaminhoBanco(CaminhoDB: string): string;
var
  Arr: TArray<string>;
begin
  Result := CaminhoDB;
  if CaminhoDB.StartsWith('\\') then
  begin
    Arr := CaminhoDB.Split(['\']);
    Result := Arr[3] + ':' + Copy(CaminhoDB, Arr[2].Length + 5, CaminhoDB.Length);
  end;

end;

function TMainCtr.ConsultaDigValue(DigValue: string): string;
begin

end;

constructor TMainCtr.Create(CaminhoDB: string);
begin
  CarregaBanco(CaminhoDB);
  FNfceCtr := TNfceCtr.Create(FBanco);
  FEmpresaCtr := TEmpresaCtr.Create(FBanco);
  FCupomCtr := TCupomCtr.Create(FBanco);
end;

destructor TMainCtr.Destroy;
begin
  FreeAndNil(FBanco);
  FreeAndNil(FNfceCtr);
  FreeAndNil(FEmpresaCtr);
  FreeAndNil(FCupomCtr);
  FreeAndNil(FSerieNfce);
  inherited;
end;

function TMainCtr.ExtraiNumeroNfce(ChaveAcesso: string): integer;
begin
  Result := StrToInt(Copy(ChaveAcesso, 26, 9));
end;

function TMainCtr.FormaPagamentoFactory: TFormasPagamentoVO;
begin
  Result := TFormasPagamentoVO.Create;
end;

function TMainCtr.GeraXml(Cupom: TCupomVO; DigValue, DirXml: string): boolean;
var
  ACBrNFe: TACBrNFe;
  NfceCtr: TNfceCTR;
begin
  Result := False;
  try
    try
      NfceCtr := TNfceCTR.Create(FBanco, DirXml);
      ACBrNFe := NfceCtr.PreencheNfce(Cupom);

      ACBrNFe.NotasFiscais.Assinar;
      ACBrNFe.NotasFiscais.Validar;

      if (DigValue <> ACBrNFe.NotasFiscais[0].NFe.signature.DigestValue) and
       (DigValue <> EmptyStr) then
        ACBrNFe.NotasFiscais[0].NFe.signature.DigestValue := DigValue;

      ACBrNFe.Consultar;

      Result := True;
    except
      on E: Exception do
      begin
        Result := False;
        TLog.SalvaLog(Format('Erro ao gerar XML para o cupom %s!' + sLineBreak +
          'Erro: %s.', [Cupom.CuponsId.ToString, E.Message]), GetCurrentDir, 'Log.txt');
      end;

    end;
  finally
    FreeAndNil(NfceCtr);
  end;
end;

function TMainCtr.GetCnpj: string;
begin
  Result := FEmpresaCtr.GetEmpresa.EmpresaCnpj;
  FNfceCtr.Cnpj := Result;
end;

function TMainCtr.GetCupom(WhereKey, WhereValue: string): TCupomVO;
begin
  try
    Result := FCupomCtr.SelectCupom(WhereKey, WhereValue);
  except
    on E: EArgumentOutOfRangeException do
    begin
      Result := nil
    end;
  end;
end;

function TMainCtr.GetHost(CaminhoDB: string): string;
var
  Arr: TArray<string>;
begin
  Result := 'localhost';
  if CaminhoDB.StartsWith('\\') then
  begin
    Arr := CaminhoDB.Split(['\']);
    Result := Arr[2];
  end;
end;

function TMainCtr.GetCupom(Nfce: TNfceVO; IdAnterior: integer): TCupomVO;
var
  Aux: TCupomVO;
begin
  Aux := FCupomCtr.SelectCupom('FNfce', IdAnterior.ToString);
  if Assigned(Aux) then
    Result := FCupomCtr.SelectCupom('FCuponsId', IntToStr(Aux.CuponsId + 1));
end;

function TMainCtr.GetListaNfce: TObjectList<TNfceVO>;
var
  Order: TOrder;
  Filter: TCustomCriterion;
begin
  Filter := Linq['FNfceDataEmissao'] >= StrToDate('01/11/2019');
  Order := TOrder.Asc('FNfceNumero');
  Result := FNfceCtr.GetListaNfce(Filter, Order);
end;

function TMainCtr.GetListaNfceSemChave: TObjectList<TNfceVO>;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FNfceChaveAcesso'] = '';
  Result := FNfceCtr.GetListaNfce(Filter);
end;

function TMainCtr.GetRetornoNfce(Nfce: TNfceVO; var DigValue: string): TNfceVO;
var
  ACBrNFe: TACBrNFe;
begin
  ACBrNFe := FNfceCtr.ConsultaPorChave(Nfce.NfceChaveAcesso);
  Nfce.NfceCodigoRetorno := ACBrNFe.WebServices.Consulta.cStat.ToString;
  Nfce.NfceProtocolo := ACBrNFe.WebServices.Consulta.Protocolo;
  DigValue := ACBrNFe.WebServices.Consulta.protNFe.digVal;
  Nfce.NfceDataEmissao := ACBrNFe.WebServices.Consulta.DhRecbto;
  Nfce.NfceDataTransmissao := ACBrNFe.WebServices.Consulta.DhRecbto;
end;

function TMainCtr.GetUltimaNumeracaoNfce: integer;
var
  SerieNfceCtr: TSerieNfceCtr;
begin
  SerieNfceCtr := TSerieNfceCtr.Create(FBanco);
  try
    Result := SerieNfceCtr.GetSerieNfce.SerieNfceNumeracao;
  finally
    FreeAndNil(SerieNfceCtr);
  end;
end;

function TMainCtr.GetXmls(DirXml: string): TStringList;
var
  I: integer;
  SearchRec: TSearchRec;
begin
  try
    Result := TStringList.Create;
    I := FindFirst(DirXml+'\*.xml', 0, SearchRec);
    while I = 0 do
    begin
      if SearchRec.Name <> 'ConfigECFVirtual.xml' then
         Result.Add(DirXml + '\' + SearchRec.Name);
      I := FindNext(SearchRec);
    end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;

end;

function TMainCtr.SalvaCupom(Cupom: TCupomVO): boolean;
begin
  FCupomCtr.Update(Cupom);
end;

function TMainCtr.SalvaNfce(Nfce: TNfceVO): boolean;
begin
  Result :=  FNfceCtr.Save(Nfce);
end;

function TMainCtr.DateTimeToDate(const Date: TDateTime): TDate;
var
  Aux: string;
begin
  Aux := DateTimeToStr(Date);
  Result := StrToDate(Copy(Aux, 0, 10));
end;

end.
