unit uOperadorasCartaoCTR;

interface
uses
  SysUtils, uGenericoDao, Generics.Collections, UBanco,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq, uOperadorasCartaoVo;

type
  TOperadorasCartaoCTR = class
  private
    FBanco: TBanco;
    Dao: TGenericoDao<TOperadorasCartaoVo>;
  public
    constructor Create(Banco: TBanco);
    function getOperadora(OperadoraId: integer): TOperadorasCartaoVo; overload;
    function GetOperadora(
      OperadoraInstituicao: string): TOperadorasCartaoVo; overload;
  end;
implementation

{ TOperadorasCartaoCTR }

constructor TOperadorasCartaoCTR.Create(Banco: TBanco);
begin
  FBanco := Banco;
  Dao :=  TGenericoDAO<TOperadorasCartaoVo>.Create(Banco);
end;

function TOperadorasCartaoCTR.getOperadora(
  OperadoraId: integer): TOperadorasCartaoVo;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FOperadorasCartaoId'] = OperadoraId;
  Result := TOperadorasCartaoVo(Dao.Select(Filter));
end;

function TOperadorasCartaoCTR.getOperadora(
  OperadoraInstituicao: string): TOperadorasCartaoVo;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FOperadoraInstituicao'] = OperadoraInstituicao;
  Result := TOperadorasCartaoVo(Dao.Select(Filter));
end;

end.
