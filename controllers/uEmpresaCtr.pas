unit uEmpresaCtr;

interface
uses
  SysUtils, Generics.Collections,

  uEmpresaDao, uEmpresaVO, UBanco;

type
  TEmpresaCtr = class
    constructor Create(Banco: TBanco);
  private
    EmpresaDao: TEmpresaDao;
  public
    function GetEmpresa: TEmpresaVo;
  end;
implementation

{ TEmpresaCtr }

constructor TEmpresaCtr.Create(Banco: TBanco);
begin
  EmpresaDao := TEmpresaDao.Create(Banco);
end;

function TEmpresaCtr.GetEmpresa: TEmpresaVo;
begin
  Result := EmpresaDao.GetEmpresa;
end;

end.
