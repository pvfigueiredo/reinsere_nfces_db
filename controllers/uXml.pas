unit uXml;

interface

uses
  Generics.Collections, System.SysUtils, Dialogs, StrUtils, XMLIntf, ComCtrls,
  Classes, XMLDoc, ACBrNFe, pcnConversao, Windows;

type
  TXml = class
    constructor Create;
  private
    FACBrNFe: TACBrNFe;
    FChaveAcesso: string;
    FXml: string;
  public
    property Xml: string read FXml write FXml;
    property ChaveAcesso: string read FChaveAcesso write FChaveAcesso;
    property ACBrNFe: TACBrNFe read FACBrNFe write FACBrNFe;

    function CarregaXml: boolean;
  end;
implementation

{ TXml }

function TXml.CarregaXml: boolean;
begin
  try
    FACBrNFe.NotasFiscais.Clear;
    if FileExists(FXml) then
    begin
      FACBrNFe.NotasFiscais.LoadFromFile(FXml);
      exit(True);
    end;
    Result := False;
  except
    on E: Exception do
    begin
      Result := False;
      raise;
    end;
  end;
end;

constructor TXml.Create;
begin
  ACBrNFe := TACBrNFe.Create(nil);
end;

end.
