unit uCupomCtr;

interface
uses
  SysUtils, Generics.Collections, Aurelius.Criteria.base,

  uCupomVO, uCupomDao, UBanco;

type
  TCupomCtr = class
    constructor Create(Banco: TBanco);
    destructor Destroy; override;
  private
    FCupomDao: TCupomDao;
  public
    function SelectCupom(WhereKey, WhereValue: string): TCupomVO;
    function GetListaCupom(Filter: TCustomCriterion): TObjectList<TCupomVO>;
    function Save(Cupom: TCupomVO): boolean;
    function Update(Cupom: TCupomVO): boolean;
  end;

implementation

{ TCupomCtr }

constructor TCupomCtr.Create(Banco: TBanco);
begin
  FCupomDao := TCupomDao.Create(Banco);
end;

destructor TCupomCtr.Destroy;
begin
  FreeAndNil(FCupomDao);
  inherited;
end;

function TCupomCtr.GetListaCupom(
  Filter: TCustomCriterion): TObjectList<TCupomVO>;
begin
  Result := TObjectList<TCupomVO>(FCupomDao.GetObjectList(Filter));
end;

function TCupomCtr.Save(Cupom: TCupomVO): boolean;
begin
  Result := FCupomDao.Save(Cupom);
end;

function TCupomCtr.SelectCupom(WhereKey, WhereValue: string): TCupomVO;
begin
  Result := FCupomDao.SelectCupom(Wherekey, WhereValue);
end;

function TCupomCtr.Update(Cupom: TCupomVO): boolean;
begin
  Result := FCupomDao.Update(Cupom);
end;

end.
