unit uConfiguracoesCtr;

interface
uses
  SysUtils, uGenericoDao, Generics.Collections, UBanco,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq, uConfiguracoesVO;
type
  TConfiguracoesCtr = class
  private
    FBanco: TBanco;
    FDao: TGenericoDao<TConfiguracoesVO>;
  public
    constructor Create(Banco: TBanco);
    function GetConfiguracao(Id: integer): string;
  end;
implementation

{ TConfiguracoesCtr }

constructor TConfiguracoesCtr.Create(Banco: TBanco);
begin
  FBanco := Banco;
  FDao := TGenericoDAO<TConfiguracoesVO>.Create(FBanco);
end;

function TConfiguracoesCtr.GetConfiguracao(Id: integer): string;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FConfiguracoesId'] = Id;
  Result := TConfiguracoesVO(FDao.Select(Filter)).ConfigValor;
end;

end.
