unit uManipulaNfce;

interface

uses
  Generics.Collections, System.SysUtils, Dialogs, StrUtils, XMLIntf, ComCtrls,
  Classes, XMLDoc, ACBrNFe, pcnConversao, Windows, uCupomVO;

type
  TManipulaNfce = class
  private
    FACBrNFe: TACBrNFe;
  public
    property ACBrNFe: TACBrNFe read FACBrNFe write FACBrNFe;
    end;
implementation

{ TManipulaNfce }

end.
