unit uAreceberCtr;

interface
uses
  SysUtils, uAReceberDao, uAReceberVo, Generics.Collections, UBanco,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq;

type
  TAReceberCtr = class
  private
    FBanco: TBanco;
    Dao: TAReceberDao;
  public
    constructor Create(Banco: TBanco);
    function GetAreceber(CupomId: integer): TObjectList<TAReceberVo>;
  end;
implementation

{ TAReceberCtr }

constructor TAReceberCtr.Create(Banco: TBanco);
begin
  FBanco := Banco;
  Dao :=  TAReceberDao.Create(Banco);
end;

function TAReceberCtr.GetAreceber(CupomId: integer): TObjectList<TAReceberVo>;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FCuponsId'] = CupomId;
  Result := Dao.GetObjectList(Filter);
end;

end.
