unit uPreenchimentoNfce;

interface

uses
  SysUtils, uNfceDao, Generics.Collections, uNfceVO, UBanco, DateUtils,
  Aurelius.Criteria.base, uCupomVO, ACBrNFe, uItensCuponsVO, uItensCuponsCtr,
  uVendasCartoesVO, pcnConversao, uVendasCartoesCTR, pcnNFe, uEmpresaVo,
  uEmpresaCtr, pcnConversaoNFe, UInutilizacoes_CTR, uSerieNfceVO, uSerieNfceCtr,
  Math, uAreceberVO, uAreceberCtr, uOperadorasCartaoVO, uOperadorasCartaoCTR,
  Dialogs, IniFiles, ACBrDFeSSL, ACBrUtil, blckSock, Forms, ACBrMail,
  ACBrPosPrinter, UConfiguracoes_CTR, UConfiguracoes_VO;

type
  TPreenchimentoNfce = class
    constructor Create(Banco: TBanco);
    destructor Destroy; override;
  private
    FAmbiente: integer;
    ACBrNFe: TACBrNFe;
    ACBrMail: TACBrMail;
    ACBrPosPrinter: TACBrPosPrinter;
    Empresa: TEmpresaVo;
    FBanco: TBanco;
    FNfceDao: TNfceDao;
    FCnpj: string;
    function DefineInfNFe: TinfNFe;
    function DefineIde(Cupom: TCupomVO): TIde;
    function DefineEmit: TEmit;
    function DefineDest(cupom: TCupomVO): TDest;
    function SetCodigoNumerico: integer;
    function DefineNumeracao: integer;
    function VerificaCFOPCombustivel(CFOP: string): boolean;
    function ValidaEAN13(EAN: string): Boolean;
    function defineProd(Cupom: TCupomVO; ItemCupom: TItensCuponsVO; NumeroItem: Integer): TProd;
    function DefineImposto(ItemCupom: TItensCuponsVo): TImposto;
    function calculaImpostosItem(item: TItensCuponsVo): Currency;
    function DefineOrgCST(cst: string): TpcnOrigemMercadoria;
    function DefineCSOSN(ItemCupom: TItensCuponsVO): TICMS;
    function DefineICSM(ItemCupom: TItensCuponsVO): TICMS;
    function defineCOFINS(ItemCupom: TItensCuponsVO): TCOFINS;
    function definePIS(ItemCupom: TItensCuponsVo): TPIS;
    function defineTotal(ItemCupom: TItensCuponsVO): TTotal;
    function validaBandeira(bandeira: string): TpcnBandeiraCartao;
    function defineAutXML(cupom: TCupomVO): TautXMLCollection;
    function getOperadoraCartaoCNPJ(operadoraId: Integer): String; overload;
    function getOperadoraCartaoCNPJ(operadora_instituicao: string): string; overload;
    function defineTransp: TTransp;
    function Codifica(Action, Src: String): String;
    function GetCaminho: string;
    function GetAmbiente: TpcnTipoAmbiente;
    function GetFormaPagamento(Cupom: TCupomVO): TpcnIndicadorPagamento;
  public
    property Cnpj: string read FCnpj write FCnpj;
    function GetListaNfce(Filter: TCustomCriterion): TObjectList<TNfceVo>; overload;
    function GetListaNfce(Filter: TCustomCriterion; Order: TOrder): TObjectList<TNfceVO>; overload;
    function GetListaNfceOrdenada(Order: TOrder): TObjectList<TNfceVO>;
    function CalculaChaveAcesso(Nfce: TNfceVO): string;
    function Save(Item: TNfceVO): boolean;
    function Update(Item: TNfceVO): boolean;
    function PreencheNfce(Cupom: TCupomVO): TACBrNFe;
    function ConsultaPorChave(const Chave: string): TACBrNFe;
    function ConsultaPorXml(const Xml: string): TACBrNFe;
  end;

implementation

{ TNfceCtr }

function TPreenchimentoNfce.CalculaChaveAcesso(Nfce: TNfceVO): string;
var
  Chave: string;
  Mes, Ano, Dia: Word;
  peso: array of integer;
  Soma: integer;
  Verificador, I: integer;
begin
  DecodeDate(Nfce.NfceDataEmissao, Ano, Mes, Dia);
  Chave := '31' + Copy(Ano.ToString, 3, 4) + Format('%.*d', [2, Mes]) + FCnpj +
    Nfce.NfceModelo + Format('%.*d', [3, Nfce.NfceSerie]) + Format('%.*d',
    [9, Nfce.NfceNumero]) + '1' + Format('%.*d', [8, Nfce.NfceNumero + 1]);

  peso := [4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6,
    5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 0];

  Soma := 0;
  for I := 0 to Chave.Length - 1 do
  begin
    Soma := Soma + (StrToInt(Chave[I + 1]) * peso[I]);
  end;

  Soma := Soma - (11 * Trunc(Soma / 11));

  Verificador := 0;

  if (Soma <> 0) and (Soma <> 1) then
    Verificador := 11 - Soma;

  Chave := Chave + IntToStr(Verificador);

  Result := Chave;

end;

constructor TPreenchimentoNfce.Create(Banco: TBanco);
var
  EmpresaCtr: TEmpresaCtr;
begin
  FBanco := Banco;
  FNfceDao := TNfceDao.Create(FBanco);

  EmpresaCtr := TEmpresaCtr.Create(FBanco);
  Empresa := EmpresaCtr.GetEmpresa;

  ACBrNFe := TACBrNFe.Create(nil);
  ACBrMail := TACBrMail.Create(nil);
  ACBrPosPrinter := TACBrPosPrinter.Create(nil);
  ForceDirectories('C:\Facil\Xml');
end;

function TPreenchimentoNfce.DefineDest(cupom: TCupomVO): TDest;
var
  verClient: boolean;
begin

  try
    if cupom.CupomTotalLiquido >= 3000 then
      cupom.Cliente.ClientesId := 2;

    Result := TDest.Create;

    verClient := False;

    if ((cupom.Cliente.ClientesId <> 1) or (verClient)) then
    begin
      Result.xNome := IntToStr(cupom.cliente.Clientesid) + ' - ' +
        cupom.cliente.ClienteNome;
      Result.CNPJCPF := cupom.cliente.ClienteCpfCnpj;

      with Result.EnderDest do
      begin
        xMun := cupom.cliente.clienteCidade;
        xLgr := cupom.cliente.clienteEndereco;
        xBairro := cupom.cliente.clienteBairro;
        cMun := cupom.cliente.clienteCodIbgeCidade;
        UF := cupom.cliente.clienteUf;
        CEP := cupom.cliente.clienteCep.ToInteger;
        nro := cupom.cliente.clienteNumero.ToString;
        cPais := 1058;
        xPais := 'BRASIL';
      end;

      Result.Email := cupom.cliente.clienteEmail;
      // Manual do Contribuinte:
      // Nota 1: No caso de NFC-e informar indIEDest=9=N�o Contribuinte e n�o informar a tag IE do destinat�rio;

      Result.indIEDest := inNaoContribuinte;
    end;
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir dados do Cliente.' + #13 + e.Message);
  end;

end;

function TPreenchimentoNfce.DefineEmit: TEmit;
var
  Empresa: TEmpresaVO;
  EmpresaCtr: TEmpresaCtr;
begin
  EmpresaCtr := TEmpresaCtr.Create(FBanco);
  try
    Empresa := EmpresaCtr.GetEmpresa;
    try
      Result := TEmit.Create;
      Result.CNPJCPF := Empresa.EmpresaCnpj;
      Result.xNome := Empresa.EmpresaRazaoSocial;
      Result.xFant := Empresa.EmpresaNomeFantasia;

      // Endere�o do emitente.
      with Result.EnderEmit do
      begin
        CEP := Empresa.EmpresaCep.ToInteger;
        xLgr := Empresa.EmpresaEndereco;
        nro := Empresa.EmpresaNumero.ToString;
        xCpl := Empresa.EmpresaComplemento;
        xBairro := Empresa.EmpresaBairro;
        cMun := Empresa.EmpresaCodIbgeCidade;
        xMun := Empresa.EmpresaCidade;
        UF := Empresa.EmpresaUf;
        cPais := 1058;
        xPais := 'BRASIL';

        fone := Empresa.EmpresaFone;
      end;

      Result.IEST := EmptyStr;

      // Inscri��o Estadual do emitente.
      Result.IE := Empresa.empresaie;

      //CRT da Empresa
      if Empresa.empresaregimecontabil.ToInteger = 1 then
        Result.CRT := crtSimplesNacional
      else
        Result.CRT := crtRegimeNormal;

    except
      on E: Exception do
        raise Exception.Create('Erro ao definir dados do Emitente.' + #13 + e.Message);
    end;
  finally
    FreeAndNil(EmpresaCtr);
  end;
end;

function TPreenchimentoNfce.DefineNumeracao: integer;
var
  Inutilizacoes: TInutilizacoes_CTR;
  Serie: TSerieNfceVO;
  SerieCtr: TSerieNfceCtr;
begin

  Inutilizacoes := TInutilizacoes_CTR.Create(FBanco);
  SerieCtr := TSerieNfceCtr.Create(FBanco);
  Serie := SerieCtr.GetSerieNfce;
  try
    Result := Inutilizacoes.getNumero(Serie.SerieNfceNumeracao + 1, Serie.SeriesNfceId);
  finally
    FreeAndNil(Inutilizacoes);
    FreeAndNil(SerieCtr);
    FreeAndNil(Serie);
  end;
end;

function TPreenchimentoNfce.SetCodigoNumerico: integer;
var
  numTemp: string;
begin
  numTemp := IntToStr(defineNumeracao + 1);
  if Length(numTemp) >= 8 then
    numTemp := copy(numTemp, 1, 7);
  Result := StrToInt(numTemp);
end;

function TPreenchimentoNfce.DefineIde(Cupom: TCupomVO): TIde;
var
  Empresa: TEmpresaVO;
  EmpresaCtr: TEmpresaCtr;
begin
  EmpresaCtr := TEmpresaCtr.Create(FBanco);
  Empresa := EmpresaCtr.GetEmpresa;
  try

    Result := TIde.Create;

    Result.cUF := Empresa.EmpresaCodIbgeUf;

    Result.cNF := cupom.Nfce.NfceNumero + 1;

    Result.natOp := 'VENDA DE MERCADORIA';

    Result.indPag := GetFormaPagamento(Cupom);

    Result.modelo := 65;

    Result.serie := 1;

    Result.nNF := cupom.Nfce.NfceNumero;

    Result.dEmi := Cupom.CupomDataEmissao;

    Result.dSaiEnt := Cupom.CupomDataEmissao; // getTimeInternet(0);

    Result.hSaiEnt := Cupom.CupomDataEmissao; // getTimeInternet(0);

    Result.tpNF := tnSaida;

    Result.idDest := doInterna;

    Result.cMunFG := Empresa.EmpresaCodIbgeCidade;

    Result.tpImp := tiNFCe;

    Result.tpEmis := teNormal;

    // Identifica��o do Ambiente.
    {if ambiente = 0 then
      Result.tpAmb := taProducao
    else}
    Result.tpAmb := GetAmbiente;

    // Finalidade de emiss�o da NFC-e.
    Result.finNFe := fnNormal; // Normal;
    // Indica opera��o com Consumidor final - Este deve sempre ser Cosumidor final para NFC-e.
    Result.indFinal := cfConsumidorFinal;

    // Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o.
    Result.indPres := pcPresencial;

    //  FIXO : Processo de emiss�o da NF-e  ->  0=Emiss�o de NF-e com aplicativo do contribuinte;
    Result.procEmi := peAplicativoContribuinte;

    // Informar a vers�o do aplicativo emissor de NF-e.
    Result.verProc := '2.2';

  except
    on E: Exception do
      raise Exception.Create('Erro ao definir dados Ide.' + #13 + e.Message);
  end;
end;

function TPreenchimentoNfce.DefineInfNFe: TinfNFe;
begin
  try
    Result := TInfNFe.Create;
    Result.versao := 4.0;
  except
    on E: Exception do
      raise Exception.Create('N�o � poss�vel transmitir esta NFC-e. ' + 'Vers�o do layout indefinida.');
  end;
end;

destructor TPreenchimentoNfce.Destroy;
begin
  FreeAndNil(FNfceDao);
  inherited;
end;

function TPreenchimentoNfce.GetAmbiente: TpcnTipoAmbiente;
var
  Configuracao: TConfiguracoes_CTR;
begin
  Configuracao := TConfiguracoes_CTR.Create(FBanco);
  try
    result := TpcnTipoAmbiente(integer(Configuracao.GetConfiguracaoValor(122)));
  finally
    FreeAndNil(Configuracao);
  end;
end;

function TPreenchimentoNfce.GetCaminho: string;
begin
  if Debughook > 0 then
    Result := 'D:\automacao\projetos\development\projects\facil_nfce\Build'
  else
    Result := 'C:\Facil\Facil NFCE';
end;

function TPreenchimentoNfce.GetFormaPagamento(Cupom: TCupomVO): TpcnIndicadorPagamento;
begin
  if not Assigned(Cupom.FormasPagamento) then
    exit(ipNenhum);

   case (Cupom.FormasPagamento.FormasPagamentoId) of
    1:
      Result := ipVista;
    2:
      Result := ipPrazo;
    3:
      Result := ipOutras;
    else
      Result := ipNenhum;
   end;
end;

function TPreenchimentoNfce.GetListaNfce(Filter: TCustomCriterion; Order: TOrder): TObjectList<TNfceVO>;
begin
  Result := FNfceDao.GetList(Filter, Order);
end;

function TPreenchimentoNfce.GetListaNfceOrdenada(Order: TOrder): TObjectList<TNfceVO>;
begin
  Result := FNfceDao.GetList(Order);
end;

function TPreenchimentoNfce.PreencheNfce(Cupom: TCupomVO): TACBrNFe;
var
  dataEmissao: TDate;
  AReceber: TObjectList<TAreceberVO>;
  AReceberCtr: TAReceberCtr;
  ItensCupom: TObjectList<TItensCuponsVO>;
  ItensCupomCtr: TItensCuponsCtr;
  i, j, NumeroItem: Integer;
  tempStr: string;
  origem_cst: TpcnOrigemMercadoria;
  vendaCartao: TVendasCartoesVO;
  vendaCartao_CTR: TVendasCartoesCTR;
  fsvTotTrib, fsvBC, fsvICMS, fsvBCST, fsvST, fsvProd, fsvFrete: Currency;
  fsvSeg, fsvDesc, fsvII, fsvIPI, fsvPIS, fsvCOFINS, fsvOutro, fsvNF: Currency;
begin
  ItensCupomCtr := TItensCuponsCtr.Create(FBanco);
  AReceberCtr := TAReceberCtr.Create(FBanco);

  try
    ACBrNFe.NotasFiscais.Clear;

    with ACBrNFe.NotasFiscais.Add.NFe do
    begin
      infNFe := DefineInfNFe;

      if not Assigned(Ide) then
        Ide := TIde.Create;

      Ide := DefineIde(Cupom);

      Emit := DefineEmit;

      Dest := defineDest(Cupom);

      fsvTotTrib := 0;
      fsvBC := 0;
      fsvICMS := 0;
      fsvBCST := 0;
      fsvST := 0;
      fsvProd := 0;
      fsvFrete := 0;
      fsvSeg := 0;
      fsvDesc := 0;
      fsvII := 0;
      fsvIPI := 0;
      fsvPIS := 0;
      fsvCOFINS := 0;
      fsvOutro := 0;
      NumeroItem := 1;

      ItensCupom := ItensCupomCtr.GetItensCupons(Cupom.CuponsId);

      // ITENS DO CUPOM
      for i := 0 to Pred(ItensCupom.Count) do
      begin
        // Os itens cancelados n�o podem estar na Result
        if ItensCupom[i].ItemCancelado then
          Continue;
        with det.New do
        begin
          Prod := defineProd(Cupom, ItensCupom[i], NumeroItem);

          Imposto := defineImposto(ItensCupom[i]);

          Total := defineTotal(ItensCupom[i]);

          Transp := defineTransp;

          if Assigned(ItensCupom[i].Produto.CodigoAnp) and
           (Trim(ItensCupom[i].Produto.CodigoAnp.CodigoAnpDescricao.Trim) <> EmptyStr) then
          begin
            infAdProd := ItensCupom[i].Produto.ProdutoNome;
          end;

          // Informa o serial no Result
          if (Assigned(ItensCupom[i].ProdutosSeriais) and (ItensCupom[i].ProdutosSeriais.produtosSerial <> EmptyStr)) then
            infAdProd := infAdProd + ItensCupom[i].ProdutosSeriais.produtosserial;

          fsvTotTrib := fsvTotTrib + Imposto.vTotTrib;
          fsvBC := fsvBC + Imposto.ICMS.vBC;
          fsvICMS := fsvICMS + Imposto.ICMS.vICMS;
          fsvBCST := fsvBCST + Imposto.ICMS.vBCST;
          fsvST := fsvST + Imposto.ICMS.vICMSST;
          fsvProd := fsvProd + Prod.vProd;
          fsvFrete := fsvFrete + Prod.vFrete;
          fsvSeg := fsvSeg + Prod.vSeg;
          fsvDesc := fsvDesc + Prod.vDesc;
          fsvII := fsvII + Imposto.II.vII;
          fsvIPI := fsvIPI + Imposto.IPI.vIPI;
          fsvPIS := fsvPIS + Imposto.PIS.vPIS;
          fsvCOFINS := fsvCOFINS + Imposto.COFINS.vCOFINS;
          fsvOutro := fsvOutro + Prod.vOutro;
          NumeroItem := NumeroItem + 1;

        end;
      end;
      fsvNF := (fsvProd + fsvST + fsvFrete + fsvSeg + fsvOutro + fsvII + fsvIPI) - fsvDesc;

       // FORMAS DE PAGAMENTO - MODALIDADES
      I := I - 1;
      AReceber := AReceberCtr.GetAreceber(ItensCupom[I].CuponsId);
      for j := 0 to AReceber.Count - 1 do
      begin
        with pag.New do
        begin
          case AReceber[j].modalidadesId of
            1:
              begin
                tPag := fpDinheiro;
              end;

            2:
              begin
                tPag := fpCheque;
              end;

            3:
              begin
                tPag := fpCheque;
              end;

            4:
              begin
                vendaCartao := TVendasCartoesVO.Create;
                vendaCartao_CTR := TVendasCartoesCTR.Create(FBanco);
                vendaCartao := vendaCartao_CTR.RecuperaCartaoAreceber(AReceber[j].areceberid);

                if vendaCartao.VendaCredito then
                begin
                  tPag := fpCartaoCredito;
                end
                else
                  tPag := fpCartaoDebito;

                if not Cupom.CupomUtilizaTef then
                begin
                  tpIntegra := tiPagNaoIntegrado;
                  CNPJ := getOperadoraCartaoCNPJ(vendaCartao.OperadorasCartaoId);
                end
                else
                begin
                  tpIntegra := tiPagIntegrado;
                  tBand := validaBandeira(vendaCartao.VendaRede);
                  cAut := vendaCartao.VendaNSU;
                  CNPJ := getOperadoraCartaoCNPJ(vendaCartao.VendaInstituicao);
                end;
              end;

            5:
              begin
                tPag := fpCreditoLoja;
              end;

            6:
              begin
                tPag := fpValeAlimentacao;
              end;

            7:
              begin
                tPag := fpBoletoBancario;
              end;

          else
            begin
              tPag := fpOutro;
            end;
          end;

          if tPag = fpDinheiro then
            vPag := areceber[j].arecebervalor + Cupom.cupomtotaltroco
          else
            vPag := areceber[j].arecebervalor;
        end;

        pag.vTroco := Cupom.cupomtotaltroco;
      end;

      autXML := defineAutXML(Cupom);

      Total.ICMSTot.vTotTrib := fsvTotTrib;
      Total.ICMSTot.vBC := fsvBC;
      Total.ICMSTot.vICMS := fsvICMS;
      Total.ICMSTot.vBCST := fsvBCST;
      Total.ICMSTot.vST := fsvST;
      Total.ICMSTot.vProd := fsvProd;
      Total.ICMSTot.vFrete := fsvFrete;
      Total.ICMSTot.vSeg := fsvSeg;
      Total.ICMSTot.vDesc := fsvDesc;
      Total.ICMSTot.vII := fsvII;
      Total.ICMSTot.vIPI := fsvIPI;
      Total.ICMSTot.vPIS := fsvPIS;
      Total.ICMSTot.vCOFINS := fsvCOFINS;
      Total.ICMSTot.vOutro := fsvOutro;
      Total.ICMSTot.vNF := fsvNF;
    end;
    //ACBrNFe.NotasFiscais[0].NFe.procNFe.chNFe := Cupom.Nfce.NfceChaveAcesso;
    Result := ACBrNFe;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;
  end;
end;

function TPreenchimentoNfce.GetListaNfce(Filter: TCustomCriterion): TObjectList<TNfceVo>;
begin
  Result := FNfceDao.GetList(Filter);
end;

function TPreenchimentoNfce.Save(Item: TNfceVO): boolean;
begin
  Result := FNfceDao.Save(Item);
end;

function TPreenchimentoNfce.Update(Item: TNfceVO): boolean;
begin
  Result := FNfceDao.Update(Item);
end;

function TPreenchimentoNfce.defineProd(Cupom: TCupomVO; ItemCupom: TItensCuponsVO; NumeroItem: Integer): TProd;
begin
  try
    Result := TProd.Create;
    Result.cProd := ItemCupom.produto.produtosId.ToString;
    Result.nItem := NumeroItem;

    if (Pos('789', ItemCupom.produto.produtoCodBarras) = 1) or (Pos('790', ItemCupom.produto.produtoCodBarras) = 1) then
    begin
      //Verifica se � um EAN 13 v�lido, sen�o o campo cEAN n�o ser� preenchido
      if ValidaEAN13(ItemCupom.produto.produtoCodBarras) then
      begin
        Result.cEAN := ItemCupom.produto.produtoCodBarras;
        Result.cEANTrib := ItemCupom.produto.produtoCodBarras;
      end
      else
      begin
        Result.cEAN := 'SEM GTIN';
        Result.cEANTrib := 'SEM GTIN';
      end;
    end
    else
    begin
      Result.cEAN := 'SEM GTIN';
      Result.cEANTrib := 'SEM GTIN';
    end;

    Result.xProd := ItemCupom.produto.produtoNome;

    if (Assigned(ItemCupom.produto.CodigoAnp)) and
      (Trim(ItemCupom.produto.CodigoAnp.codigoAnpDescricao.Trim) <> EmptyStr) then
    begin
      Result.xProd := ItemCupom.produto.CodigoAnp.codigoAnpDescricao;
    end
    else
      Result.xProd := ItemCupom.produto.produtonome;

    Result.NCM := ItemCupom.produto.ProdutoNCM;

    Result.CEST := ItemCupom.produto.produtocest;
    Result.CFOP := ItemCupom.produto.ProdutoCFOP;
    Result.uCom := ItemCupom.produto.unidades.unidadenome;

    Result.qCom := ItemCupom.itemqte;
    Result.vUnCom := RoundTo(ItemCupom.itemvalorunitario, -2);
    Result.vProd := RoundTo(ItemCupom.itemvalortotal, -2);

    if (Assigned(ItemCupom.produto.CodigoAnp)) and
    (Trim(ItemCupom.produto.CodigoAnp.codigoanpid) = '210203001') and
    (ItemCupom.produto.unidades.unidadesigla = 'UN') then
    begin
      Result.qTrib := ItemCupom.itemqte * ItemCupom.produto.produtopesoliquido;
      Result.uTrib := 'KG';

      if Result.qTrib <= 0 then
        Result.qTrib := 1;

      // Para evitar erro de divis�o por zero
      if ItemCupom.produto.produtopesoliquido > 0 then
        Result.vUnTrib := RoundTo(ItemCupom.itemvalorunitario / ItemCupom.produto.produtopesoliquido, -2)
      else
        Result.vUnTrib := RoundTo(ItemCupom.itemvalorunitario, -2)
    end
    else
    begin
      Result.qTrib := ItemCupom.itemqte;
      Result.uTrib := ItemCupom.produto.unidades.UnidadeSigla;
      Result.vUnTrib := RoundTo(ItemCupom.itemvalorunitario, -2);
    end;

    // Valor do Desconto
    Result.vDesc := 0;

    if ItemCupom.itemvalordescacre < 0 then
      Result.vDesc := (ItemCupom.itemvalordescacre *  - 1);

    if ItemCupom.itemdescacrerateio < 0 then
      Result.vDesc := Result.vDesc + (ItemCupom.itemdescacrerateio *  - 1);

    // Valor do Acrescimo
    Result.vOutro := 0;

    if ItemCupom.itemvalordescacre > 0 then
      Result.vOutro := (ItemCupom.itemvalordescacre * 1);

    if ItemCupom.itemdescacrerateio > 0 then
      Result.vOutro := Result.vOutro + (ItemCupom.itemdescacrerateio * 1);

    //****************************************************************
    // INICIO - DETALHAMENTO ESPEC�FICO DE COMBUST�VEIS
    //****************************************************************
    if VerificaCFOPCombustivel(ItemCupom.produto.ProdutoCFOP) then
    begin
      with Result.comb do
      begin
        cProdANP := StrToInt(ItemCupom.produto.CodigoAnp.codigoanpid);
        Result.comb.descANP := ItemCupom.produto.CodigoAnp.codigoanpdescricao;
        Result.comb.cProdANP := StrToInt(ItemCupom.produto.CodigoAnp.codigoanpid);
        UFcons := Cupom.cliente.clienteuf;

        if cProdANP = 210203001 then
        begin
          Result.comb.pGLP := ItemCupom.produto.produtopercglp;
          Result.comb.pGNn := ItemCupom.produto.produtopercgnn;
          Result.comb.pGNi := ItemCupom.produto.produtopercgni;
          Result.comb.vPart := ItemCupom.produto.produtovalorpartida;
        end;
      end;
    end;
    Result.IndTot := itSomaTotalNFe;
  except
    on E: Exception do
    begin
      if (ItemCupom <> nil) and (ItemCupom.produto <> nil) and (ItemCupom.produto.produtonome <> '') then
        raise Exception.Create('Erro ao definir dados do Produto ' + #13 + e.Message)
      else
        raise Exception.Create('Erro ao definir dados de Produto ');
    end;
  end;
end;

function TPreenchimentoNfce.VerificaCFOPCombustivel(CFOP: string): boolean;
begin
  Result := False;
  if (StrToInt(CFOP) >= 5651) and (StrToInt(CFOP) <= 5666) then
    Result := True;
end;

function TPreenchimentoNfce.ValidaEAN13(EAN: string): Boolean;
var
  i, SomaPar, SomaImpar: Integer;
begin
  try
    //Se o tamanho n�o for 13, de cara j� � inv�lido
    if Length(EAN) <> 13 then
      Result := False
    else
    begin
      //C�lculo do d�gito do EAN 13
      SomaPar := 0;
      SomaImpar := 0;

      for i := 1 to 12 do
      begin
        if (i mod 2) = 0 then
          SomaPar := SomaPar + StrToInt(EAN[i])
        else
          SomaImpar := SomaImpar + StrToInt(EAN[i]);
      end;
      SomaPar := SomaPar * 3;

      i := 0;

      while i < (SomaPar + SomaImpar) do
        Inc(i, 10);

      if EAN[13] = IntToStr(i - (SomaPar + SomaImpar)) then
        Result := True
      else
        Result := False;
    end;
  except
    Result := False;
  end;
end;

function TPreenchimentoNfce.DefineImposto(ItemCupom: TItensCuponsVo): TImposto;
var
  origem_cst: TpcnOrigemMercadoria;
begin
  try
    Result := TImposto.Create;
    // IMPOSTOS TOTAIS SOBRE O ITEM.
    Result.vTotTrib := calculaImpostosItem(ItemCupom);

    // ORIGEM DA MERCADORIA
    origem_cst := defineOrgCST(ItemCupom.CstId);

    // ICMS
    if Empresa.empresaRegimeContabil.ToInteger = 1 then
    begin
      Result.ICMS := defineCSOSN(ItemCupom);
    end
    else
    begin
      Result.ICMS := defineICSM(ItemCupom);
      // COFINS
      Result.COFINS := defineCOFINS(ItemCupom);
    end;

    // PIS
    Result.PIS := definePIS(ItemCupom);

    // Result.IPI :=

    Result.ICMS.orig := origem_cst;

  except
    on E: Exception do
      raise Exception.Create('Erro ao definir Impostos sobre a mercadoria. ' + #13 + e.Message);
  end;

end;

function TPreenchimentoNfce.calculaImpostosItem(item: TItensCuponsVo): Currency;
var
  ncmValorFederal: Currency;
  ncmValorEstadual: Currency;
  ncmValorMunicipal: Currency;
  aException: Exception;
begin
  try
    if item <> nil then
    begin
      ncmValorFederal := (item.itemvalortotal + item.itemvalordescacre + item.itemdescacrerateio) * item.produto.ProdutoNCMAliqNac / 100;
      ncmValorEstadual := (item.itemvalortotal + item.itemvalordescacre + item.itemdescacrerateio) * item.produto.ProdutoNCMAliqEst / 100;
      ncmValorMunicipal := (item.itemvalortotal + item.itemvalordescacre + item.itemdescacrerateio) * item.produto.ProdutoNCMAliqMun / 100;

      Result := RoundTo(ncmValorFederal + ncmValorEstadual + ncmValorMunicipal, -2);
    end
    else
      Exception.RaiseOuterException(Exception.Create('Imposs�vel transmitir. Erro ao definir item. '));
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir Valor NCM do item. ' + #13 + e.Message);
  end;
end;

function TPreenchimentoNfce.DefineOrgCST(cst: string): TpcnOrigemMercadoria;
begin
  try
    //Seta o c�digo de origem da mercadoria de acordo com a CST do produto
    if Empresa.empresaRegimeContabil.ToInteger <> 1 then
    begin
      if (cst = '100') or (cst = '110') or (cst = '120') or (cst = '130') or (cst = '140') or (cst = '141') or (cst = '150') or (cst = '151') or (cst = '160') or (cst = '170') or (cst = '190') then
        Result := oeEstrangeiraImportacaoDireta
      else if (cst = '200') or (cst = '210') or (cst = '220') or (cst = '230') or (cst = '240') or (cst = '241') or (cst = '250') or (cst = '251') or (cst = '260') or (cst = '270') or (cst = '290') then
        Result := oeEstrangeiraAdquiridaBrasil
      else
        Result := oeNacional;
    end
    else
      Result := oeNacional;
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir a Origem da Mercadoria. ' + #13 + e.Message);
  end;

end;

function TPreenchimentoNfce.DefineCSOSN(ItemCupom: TItensCuponsVO): TICMS;
begin
  try
    Result := TICMS.Create;
    if ItemCupom.csosnId <> '' then
    begin
      //ICMS.CST := cstVazio;
      //CSOSN 101
      if (ItemCupom.csosnId = '101') then
      begin
        Result.CSOSN := csosn101;
        Result.pCredSN := Empresa.empresaicmssimples;
        Result.vCredICMSSN := ItemCupom.itemvalortotal * (Empresa.empresaicmssimples / 100);
      end
      else        //CSOSN 102
      if (ItemCupom.csosnId = '102') then
        Result.CSOSN := csosn102
      else        //CSOSN 103
      if (ItemCupom.csosnId = '103') then
        Result.CSOSN := csosn103
      else        //CSOSN 201
      if (ItemCupom.csosnId = '201') then
      begin
        // ==== Verificar =======
        Result.CSOSN := csosn201;
        Result.modBCST := dbisMargemValorAgregado;
      end
      else        //CSOSN 202 OU 203
      if (ItemCupom.csosnId = '202') or (ItemCupom.csosnId = '203') then
      begin
        if (ItemCupom.csosnId = '202') then
          Result.CSOSN := csosn202;

        if (ItemCupom.csosnId = '203') then
          Result.CSOSN := csosn203;
      end
      else        //CSOSN 300
      if (ItemCupom.csosnId = '300') then
        Result.CSOSN := csosn300
      else        //CSOSN 400
      if (ItemCupom.csosnId = '400') then
        Result.CSOSN := csosn400
      else        //CSOSN 500
      if ItemCupom.csosnId = '500' then
      begin
        Result.CSOSN := csosn500;

        Result.vBCSTRet := 0;
        Result.vICMSSTRet := 0;
      end
      else        //CSOSN 900
      if ItemCupom.csosnId = '900' then
      begin
        Result.CSOSN := csosn900;
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pRedBC := ItemCupom.produto.produtoicmsred;
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);

        Result.vBCST := 0;
        Result.pICMSST := 0;
        Result.vICMSST := 0;

        //No Manual prev� que devem ser informados todos os campos poss�veis para
        //o ICMS, mas no caso desse sistema o caso 900 (CSOSN) ou 090 (CST) � tratado
        //para n�o haver lan�amento nenhum de tributa��o.
        //Assim n�o ha necessidade de informar todos os campos.
      end;
    end
    else
      Exception.RaiseOuterException(Exception.Create('N�o � poss�vel' +
        ' transmitir esta NFC-e. H� produto(s) com CSOSN indefinido. Verifique o cadastro.'));
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir CSOSN. Entre em contato com o Suporte.' + #13 + e.Message);
  end;

end;

function TPreenchimentoNfce.DefineICSM(ItemCupom: TItensCuponsVO): TICMS;
begin

  try
    Result := TICMS.Create;
  //No caso de ser CRT 3 - Tributa��o Normal, a empresa n�o � optante
  //pelo Simples Nacional (Lucro Real ou Presumido).
  //Nesse caso informa-se a tributa��o de ICMS baseada no CST

    if ItemCupom.cstId <> '' then
    begin

    //CST com final 00
      if Copy(ItemCupom.cstId, 2, 2) = '00' then
      begin
        Result.CST := cst00;
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);
      end

    //CST com final 10
      else if Copy(ItemCupom.cstId, 2, 2) = '10' then
      begin
        Result.CST := cst10;
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);

        Result.modBCST := dbisMargemValorAgregado;

        Result.vBCST := 0;
        Result.pICMSST := 0;
        Result.vICMSST := 0;
      end

    //CST com final 20
      else if Copy(ItemCupom.cstId, 2, 2) = '20' then
      begin
        Result.CST := cst20;
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.pRedBC := ItemCupom.produto.produtoicmsred;
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);

        Result.vICMSDeson := 0;
      end

    //CST com final 30
      else if Copy(ItemCupom.cstId, 2, 2) = '30' then
      begin
        Result.CST := cst30;

        Result.modBCST := dbisMargemValorAgregado;
        Result.vBCST := 0;
        Result.pICMSST := 0;
        Result.vICMSST := 0;

        Result.vICMSDeson := 0;
        Result.motDesICMS := mdiOutros;
      end

    //CST com final 40
      else if Copy(ItemCupom.cstId, 2, 2) = '40' then
      begin
        Result.CST := cst40;
        Result.vICMSDeson := 0;
        Result.motDesICMS := mdiOutros;
      end

    //CST com final 41
      else if Copy(ItemCupom.cstId, 2, 2) = '41' then
      begin
        Result.CST := cst41;
        Result.vICMSDeson := 0;
        Result.motDesICMS := mdiOutros;
      end

    //CST com final 50
      else if Copy(ItemCupom.cstId, 2, 2) = '50' then
      begin
        Result.CST := cst50;
        Result.vICMSDeson := 0;
        Result.motDesICMS := mdiOutros;
      end

    //CST com final 51
      else if Copy(ItemCupom.cstId, 2, 2) = '51' then
        Result.CST := cst51

    //CST com final 60
      else if Copy(ItemCupom.cstId, 2, 2) = '60' then
      begin
        Result.CST := cst60;
        Result.vBCSTRet := 0;
        Result.vICMSSTRet := 0;
      end

    //CST com final 70
      else if Copy(ItemCupom.cstId, 2, 2) = '70' then
      begin
        Result.CST := cst70;
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.pRedBC := ItemCupom.produto.produtoicmsred;
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);

        Result.vBCST := 0;
        Result.pICMSST := 0;
        Result.vICMSST := 0;

        Result.modBCST := dbisMargemValorAgregado;
      end

    //CST com final 90
      else if Copy(ItemCupom.cstId, 2, 2) = '90' then
      begin
        Result.CST := cst90;
      //No Manual prev� que devem ser informados todos os campos poss�veis para o ICMS
      // === Verificar ===
        Result.modBC := dbiValorOperacao; //Provisoriamente est�tico
        Result.vBC := RoundTo(ItemCupom.itembasecalculo, -2);
        Result.pICMS := ItemCupom.aliquotavalor;
        Result.vICMS := RoundTo(ItemCupom.itemvaloricms, -2);

        Result.vBCST := 0;
        Result.pICMSST := 0;
        Result.vICMSST := 0;
        Result.modBCST := dbisMargemValorAgregado;
      end;
    end
    else
      Exception.RaiseOuterException(Exception.Create('N�o � poss�vel transmitir esta NFC-e. H� produto(s) com CST indefinido. Verifique o cadastro.'));
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir CSOSN. Entre em contato com o Suporte.' + #13 + e.Message);
  end;

end;

function TPreenchimentoNfce.defineCOFINS(ItemCupom: TItensCuponsVO): TCOFINS;
begin
  try
    Result := TCOFINS.Create;
    if Empresa.empresaregimecontabil.ToInteger = 1 then
      Result.CST := cof99
    else
    begin
      if ItemCupom.cstcofinsid <> '' then
      begin
        //CST 01 e 02 - Tributado por al�quota
        if (ItemCupom.cstcofinsid = '01') or (ItemCupom.cstcofinsid = '02') then
        begin
          if (ItemCupom.cstcofinsid = '01') then
            Result.CST := cof01;

          if (ItemCupom.cstcofinsid = '02') then
            Result.CST := cof02;

          Result.vBC := ItemCupom.itenscupombasecofins;
          Result.pCOFINS := ItemCupom.produtocofins;
          Result.vCOFINS := ItemCupom.itenscupomvalorcofins;
        end

        //CST 03 - Tributado por unidade
        else if (ItemCupom.cstcofinsid = '03') then
        begin
          Result.CST := cof03;
          Result.qBCProd := ItemCupom.itemqte;
          Result.vAliqProd := ItemCupom.produtocofins;
          Result.vCOFINS := ItemCupom.itemqte * ItemCupom.produtocofins;
        end

        //COFINS n�o tribut�vel
        else if (ItemCupom.cstcofinsid = '04') then
          Result.CST := cof04
        else if (ItemCupom.cstcofinsid = '05') then
          Result.CST := cof99
        else if (ItemCupom.cstcofinsid = '06') then
          Result.CST := cof06
        else if (ItemCupom.cstcofinsid = '07') then
          Result.CST := cof07
        else if (ItemCupom.cstcofinsid = '08') then
          Result.CST := cof08
        else if (ItemCupom.cstcofinsid = '09') then
          Result.CST := cof09
        else if (ItemCupom.cstcofinsid = '49') then
          Result.CST := cof49
        //No caso de n�o ter cst cadastrado ele lan�a na NFe 99 - "Outros"
        else
          Result.CST := cof99;
      end
      else
        Exception.RaiseOuterException(Exception.Create('N�o � poss�vel transmitir esta NFC-e. H� produto(s) com COFINS indefinido. Verifique o cadastro.'));
    end;
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir COFINS. Entre em contato com o Suporte.' + #13 + e.Message);
  end;
end;

function TPreenchimentoNfce.definePIS(ItemCupom: TItensCuponsVo): TPIS;
begin
  try
    Result := TPIS.Create;
    //Se a Empresa � optante pelo Simples, n�o faz lan�amento de PIS
    if Empresa.empresaregimecontabil.ToInteger = 1 then
      Result.CST := pis99
    else
    begin
      if ItemCupom.cstpisid <> '' then
      begin
        //CST 01 e 02 - Tributado por al�quota
        if (ItemCupom.cstpisid = '01') or (ItemCupom.cstpisid = '02') then
        begin
          if (ItemCupom.cstpisid = '01') then
            Result.CST := pis01;

          if (ItemCupom.cstpisid = '02') then
            Result.CST := pis02;

          Result.vBC := ItemCupom.itenscupombasepis;
          Result.pPIS := ItemCupom.produtopis;
          Result.vPIS := ItemCupom.itenscupomvalorpis;
        end

        //CST 03 - Tributado por unidade
        else if (ItemCupom.cstpisid = '03') then
        begin
          Result.CST := pis03;
          Result.qBCProd := ItemCupom.itemqte;
          Result.vAliqProd := ItemCupom.produtopis;
          Result.vPIS := ItemCupom.itemqte * ItemCupom.produtopis;

        end

        //PIS n�o tribut�vel
        else if (ItemCupom.cstpisid = '04') then
          Result.CST := pis04
        else          //No caso de CST 05, que n�o � prevista na tag PIS, informa-se
        // o CST 99 (outros) e cria-se a tag PISST abaixo
        if (ItemCupom.cstpisid = '05') then
          Result.CST := pis99
        else if (ItemCupom.cstpisid = '06') then
          Result.CST := pis06
        else if (ItemCupom.cstpisid = '07') then
          Result.CST := pis07
        else if (ItemCupom.cstpisid = '08') then
          Result.CST := pis08
        else if (ItemCupom.cstpisid = '09') then
          Result.CST := pis09
        else if (ItemCupom.cstpisid = '49') then
          Result.CST := pis49
        //No caso de n�o ter cst cadastrado ele lan�a na NFe 99 - "Outros"
        else
          Result.CST := pis99;
      end
      else
         Exception.RaiseOuterException(Exception.Create('N�o � poss�vel transmitir esta NFC-e. H� produto(s) com CST PIS indefinido. Verifique o cadastro.'));
    end;
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir CST. Entre em contato com o Suporte.' + #13 + e.Message);
  end;
end;

function TPreenchimentoNfce.defineTotal(ItemCupom: TItensCuponsVO): TTotal;
begin

  try
    Result := TTotal.Create;

    if Empresa.empresaregimecontabil.ToInteger = 1 then
      Result.ICMSTot.vPIS := 0
    else
    begin
      if ((ItemCupom.cstpisid = '01') or (ItemCupom.cstpisid = '02')) then
        Result.ICMSTot.vPIS := Result.ICMSTot.vPIS + ItemCupom.itenscupomvalorpis
      else if (ItemCupom.cstpisid = '03') then
        Result.ICMSTot.vPIS := Result.ICMSTot.vPIS + (ItemCupom.itemqte * ItemCupom.produtopis);
    end;

    // COFINS
    if Empresa.empresaregimecontabil.ToInteger = 1 then
      Result.ICMSTot.vCOFINS := 0
    else
    begin
      if ((ItemCupom.cstcofinsid = '01') or (ItemCupom.cstcofinsid = '02')) then
        Result.ICMSTot.vCOFINS := Result.ICMSTot.vCOFINS + ItemCupom.itenscupomvalorcofins
      else if (ItemCupom.cstcofinsid = '03') then
        Result.ICMSTot.vCOFINS := Result.ICMSTot.vCOFINS + (ItemCupom.itemqte * ItemCupom.produtocofins);
    end;
  except
    on E: Exception do
      raise;

  end;

end;

function TPreenchimentoNfce.validaBandeira(bandeira: string): TpcnBandeiraCartao;
begin

  try
    if Pos('VISA', bandeira) <> 0 then
      Result := bcVisa
    else if Pos('MASTERCARD', bandeira) <> 0 then
      Result := bcMasterCard
    else if Pos('AMERICAN EXPRESS', bandeira) <> 0 then
      Result := bcAmericanExpress
    else if Pos('ELO', bandeira) <> 0 then
      Result := bcElo
    else if Pos('SOROCRED', bandeira) <> 0 then
      Result := bcSorocred
    else if Pos('DINERSCLUB', bandeira) <> 0 then
      Result := bcDinersClub
    else if Pos('HIPERCARD', bandeira) <> 0 then
      Result := bcHipercard
    else
      Result := bcOutros;
  except
    on E: Exception do

  end;

end;

function TPreenchimentoNfce.defineAutXML(cupom: TCupomVO): TautXMLCollection;
begin

  try
    Result := TautXMLCollection.Create;
    if Empresa.empresauf <> 'BA' then
      if (cupom.cliente.clientecpfcnpj.Trim <> '11566074000106') and (Empresa.empresacnpj.Trim <> '11566074000106') then
        Result.New.CNPJCPF := '11566074000106'; //F�cil Sistemas

    if Assigned(Empresa.Contador)then
    begin
      if (cupom.Cliente.clientecpfcnpj.Trim <> Empresa.contador.contadorcpf.Trim) and (Empresa.empresacnpj.Trim <> Empresa.contador.contadorcpf.Trim) then
        if Empresa.Contador.contadorcpf.Trim <> '' then
          Result.New.CNPJCPF := Empresa.contador.contadorcpf.Trim; //CPF do Contador

      if (cupom.cliente.clientecpfcnpj.Trim <> Empresa.contador.contadorcnpjesc.Trim) and (Empresa.empresacnpj.Trim <> Empresa.contador.contadorcnpjesc.Trim) then
        if Empresa.contador.contadorcnpjesc.Trim <> '' then
          Result.New.CNPJCPF := Empresa.contador.contadorcnpjesc.Trim; //CNPJ do Escrit�rio do Contador
    end;
  except
    on E: Exception do
      raise Exception.Create('Erro ao definir dados do contador autorizado a '+
        'acessar o XML. Entre em contato com o Suporte.' + #13 + e.Message);
  end;
end;

function TPreenchimentoNfce.getOperadoraCartaoCNPJ(operadoraId: Integer): String;
var
  operadoraCTR: TOperadorasCartaoCTR;
  operadoraVO: TOperadorasCartaoVO;
begin
  try
    try
      operadoraCTR := TOperadorasCartaoCTR.Create(FBanco);
      operadoraVO := TOperadorasCartaoVO.Create;

      Result := operadoraCTR.getOperadora(operadoraid).OperadoraCNPJ;

      if result = '' then
        ShowMessage('N�o � poss�vel transmitir! CNPJ da Operadora de Cart�o n�o definido. Entre em contato com o Suporte.');

    except
      on E: Exception do

    end;
  finally
    if Assigned(operadoraCTR) then
      FreeAndNil(operadoraCTR);
    if Assigned(operadoraVO) then
      FreeAndNil(operadoraVO);
  end;
end;

function TPreenchimentoNfce.defineTransp: TTransp;
begin
  try
    Result := TTransp.Create;
    Result.modFrete := mfSemFrete;
  except
    on E: Exception do
    begin
      raise;
    end;
  end;
end;

function TPreenchimentoNfce.getOperadoraCartaoCNPJ(operadora_instituicao: string): string;
var
  operadoraCTR: TOperadorasCartaoCTR;
  operadoraVO: TOperadorasCartaoVO;
begin
  try
    try
      operadoraCTR := TOperadorasCartaoCTR.Create(FBanco);
      operadoraVO := TOperadorasCartaoVO.Create;

      Result := operadoraCTR.getOperadora(operadora_instituicao).OperadoraCNPJ;

      if result = '' then
        ShowMessage('N�o � poss�vel transmitir! CNPJ da Operadora de Cart�o n�o definido. Entre em contato com o Suporte.');

    except
      on E: Exception do

    end;
  finally
    if Assigned(operadoraCTR) then
      FreeAndNil(operadoraCTR);
    if Assigned(operadoraVO) then
      FreeAndNil(operadoraVO);
  end;
end;

function TPreenchimentoNfce.ConsultaPorChave(const Chave: string): TACBrNFe;
begin
  ACBrNFe.NotasFiscais.Clear;
  ACBrNFe.WebServices.Consulta.NFeChave := Chave;
  ACBrNFe.WebServices.Consulta.Executar;
  Result := ACBrNFe;
end;

function TPreenchimentoNfce.ConsultaPorXml(const Xml: string): TACBrNFe;
begin
  ACBrNFe.NotasFiscais.Clear;
  ACBrNFe.NotasFiscais.LoadFromFile(Xml);
  ACBrNFe.Consultar;
  Result := ACBrNFe;
end;

function TPreenchimentoNfce.Codifica(Action, Src: String): String;
Label Fim; //Fun��o para criptografar e descriptografar string's

var Dest      ,
    Key : String;

    KeyLen    ,
    KeyPos    ,
    OffSet    ,
    SrcPos    ,
    SrcAsc    ,
    TmpSrcAsc ,
    Range     : Integer;
begin
  try
    if (Src = '') Then
    begin
      Result:= '';

      Goto Fim;
    end;

    Key    := 'FFA6SD5FA6SDF16E5Y1DFG1F6G5JK1MV321K6Y54KJDBG21S6D51 3V210 654F6A5SDF46A5S4F6D5F4H9DG541BZS398';
    Dest   := '';
    KeyLen := Length(Key);
    KeyPos := 0;
    Range  := 256;

    if (Action = UpperCase('C')) then
    begin
      Randomize;

      OffSet := Random(Range);

      Dest := Format('%1.2x',[OffSet]);

      for SrcPos := 1 to Length(Src) do
      begin
       // Application.ProcessMessages;

        SrcAsc := (Ord(Src[SrcPos]) + OffSet) Mod 255;

        if KeyPos < KeyLen then
          KeyPos := KeyPos + 1
        else
          KeyPos := 1;

        SrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
        Dest   := Dest + Format('%1.2x',[SrcAsc]);
        OffSet := SrcAsc;
      end;
    end
    else
    if (Action = UpperCase('D')) then
    begin
      OffSet := StrToInt('$'+ copy(Src,1,2));
      SrcPos := 3;

      repeat
        SrcAsc := StrToInt('$'+ copy(Src,SrcPos,2));

        if (KeyPos < KeyLen) then
          KeyPos := KeyPos + 1
        else
          KeyPos := 1;

        TmpSrcAsc := SrcAsc Xor Ord(Key[KeyPos]);

        if TmpSrcAsc <= OffSet then
          TmpSrcAsc := 255 + TmpSrcAsc - OffSet
        else
          TmpSrcAsc := TmpSrcAsc - OffSet;

        Dest   := Dest + Chr(TmpSrcAsc);
        OffSet := SrcAsc;
        SrcPos := SrcPos + 2;

      until (SrcPos >= Length(Src));
    end;

    Result:= Dest;

    Fim:
  except
    Result:= '1';
  end;
end;

end.

