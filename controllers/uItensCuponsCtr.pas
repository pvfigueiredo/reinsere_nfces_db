unit uItensCuponsCtr;

interface

uses
  UGenericoDAO, SysUtils, Generics.Collections, uItensCuponsVO,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq, UBanco, uItensCuponsDao;

type
  TItensCuponsCtr = class
    constructor Create(Banco: TBanco);
  private
    Dao: TItensCuponsDao;

  public
    function GetItensCupons(CupomId: integer): TObjectList<TItensCuponsVO>;
  end;
implementation

{ TItensCuponsCtr }

constructor TItensCuponsCtr.Create(Banco: TBanco);
begin
  Dao := TItensCuponsDao.Create(Banco);
end;

function TItensCuponsCtr.GetItensCupons(
  CupomId: integer): TObjectList<TItensCuponsVO>;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FCuponsId'] = CupomId;
  Result := Dao.GetObjectList(Filter);
end;

end.
