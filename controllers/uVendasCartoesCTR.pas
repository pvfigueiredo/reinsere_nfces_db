unit uVendasCartoesCTR;

interface
uses
  SysUtils, uVendasCartoesDao, Generics.Collections, UBanco,
  Aurelius.Criteria.Base, Aurelius.Criteria.Linq, uVendasCartoesVO;

type
  TVendasCartoesCTR = class
  private
    FBanco: TBanco;
    Dao: TVendasCartoesDao;
  public
    constructor Create(Banco: TBanco);
    function RecuperaCartaoAreceber(AReceberId: integer): TVendasCartoesVO;
  end;

implementation

{ TVendasCartoesCTR }

constructor TVendasCartoesCTR.Create(Banco: TBanco);
begin
  FBanco := Banco;
  Dao :=  TVendasCartoesDao.Create(Banco);
end;

function TVendasCartoesCTR.RecuperaCartaoAreceber(
  AReceberId: integer): TVendasCartoesVO;
var
  Filter: TCustomCriterion;
begin
  Filter := Linq['FAReceberId'] = AReceberId;
  Result := Dao.Select(Filter);
end;

end.
