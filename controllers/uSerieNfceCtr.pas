unit uSerieNfceCtr;

interface
uses
  uSerieNfceVO, uSerieNfceDao, Generics.Collections, SysUtils, UBanco;

type
  TSerieNfceCtr = class
    constructor Create(Banco: TBanco);
    destructor Destroy; override;
  private
    FSerieNfce: TSerieNfceDAO;
  public
    function GetSerieNfce: TSerieNfceVO;
  end;
implementation

{ TSerieNfceCtr }

constructor TSerieNfceCtr.Create(Banco: TBanco);
begin
  FSerieNfce := TSerieNfceDAO.Create(Banco);
end;

destructor TSerieNfceCtr.Destroy;
begin
  FreeAndNil(FSerieNfce);
  inherited;
end;

function TSerieNfceCtr.GetSerieNfce: TSerieNfceVO;
begin
  Result := FSerieNfce.GetSerieNfce;
end;

end.
