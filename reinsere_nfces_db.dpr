program reinsere_nfces_db;

uses
  Vcl.Forms,
  uMainForm in 'view\uMainForm.pas' {frmMain},
  UAureliusConnection in '..\classes_facil_sistemas\MVC\aurelius_connection\UAureliusConnection.pas' {AureliusConnection: TDataModule},
  Database.Session in '..\classes_facil_sistemas\MVC\implementation\Database.Session.pas',
  Database.Session.Types in '..\classes_facil_sistemas\MVC\Interfaces\Database.Session.Types.pas',
  UICTR in '..\classes_facil_sistemas\MVC\Interfaces\UICTR.pas',
  UIDAO in '..\classes_facil_sistemas\MVC\Interfaces\UIDAO.pas',
  UGenericoDAO in '..\classes_facil_sistemas\MVC\implementation\UGenericoDAO.pas',
  uConnection in '..\classes_facil_sistemas\MVC\implementation\uConnection.pas',
  uLogs in '..\classes_facil_sistemas\MVC\log\uLogs.pas',
  uAReceberDao in 'dao\uAReceberDao.pas',
  uCupomDao in 'dao\uCupomDao.pas',
  uEmpresaDao in 'dao\uEmpresaDao.pas',
  uItensCuponsDao in 'dao\uItensCuponsDao.pas',
  uNfceDao in 'dao\uNfceDao.pas',
  uSerieNfceDAO in 'dao\uSerieNfceDAO.pas',
  uVendasCartoesDao in 'dao\uVendasCartoesDao.pas',
  uAreceberCtr in 'controllers\uAreceberCtr.pas',
  uConfiguracoesCtr in 'controllers\uConfiguracoesCtr.pas',
  uCupomCtr in 'controllers\uCupomCtr.pas',
  uEmpresaCtr in 'controllers\uEmpresaCtr.pas',
  uItensCuponsCtr in 'controllers\uItensCuponsCtr.pas',
  uMainCtr in 'controllers\uMainCtr.pas',
  uManipulaNfce in 'controllers\uManipulaNfce.pas',
  uNfceCtr in 'controllers\uNfceCtr.pas',
  uOperadorasCartaoCTR in 'controllers\uOperadorasCartaoCTR.pas',
  uSerieNfceCtr in 'controllers\uSerieNfceCtr.pas',
  uVendasCartoesCTR in 'controllers\uVendasCartoesCTR.pas',
  uXml in 'controllers\uXml.pas',
  uAreceberVO in 'models\uAreceberVO.pas',
  uClientesVO in 'models\uClientesVO.pas',
  uCodigoAnpVO in 'models\uCodigoAnpVO.pas',
  uConfiguracoesVO in 'models\uConfiguracoesVO.pas',
  uContadoresVO in 'models\uContadoresVO.pas',
  uCupomVo in 'models\uCupomVo.pas',
  uEmpresaVO in 'models\uEmpresaVO.pas',
  uFormasPagamentoVO in 'models\uFormasPagamentoVO.pas',
  uItensCuponsVO in 'models\uItensCuponsVO.pas',
  uMovimentosVO in 'models\uMovimentosVO.pas',
  uNfceAnteriorVO in 'models\uNfceAnteriorVO.pas',
  UNfceVO in 'models\UNfceVO.pas',
  uOperadorasCartaoVO in 'models\uOperadorasCartaoVO.pas',
  uProdutosSeriaisVO in 'models\uProdutosSeriaisVO.pas',
  uProdutosVO in 'models\uProdutosVO.pas',
  uSerieNfceVO in 'models\uSerieNfceVO.pas',
  uUnidadesVO in 'models\uUnidadesVO.pas',
  uVendasCartoesVO in 'models\uVendasCartoesVO.pas',
  UConfiguracoes_CTR in 'unitsLegadas\UConfiguracoes_CTR.pas',
  UConfiguracoes_DAO in 'unitsLegadas\UConfiguracoes_DAO.pas',
  UConfiguracoes_VO in 'unitsLegadas\UConfiguracoes_VO.pas',
  UConfiguracoesSistema in 'unitsLegadas\UConfiguracoesSistema.pas',
  UInutilizacoes_CTR in 'unitsLegadas\UInutilizacoes_CTR.pas',
  UInutilizacoes_DAO in 'unitsLegadas\UInutilizacoes_DAO.pas',
  UInutilizacoes_VO in 'unitsLegadas\UInutilizacoes_VO.pas',
  uPreenchimentoNfce in 'controllers\uPreenchimentoNfce.pas',
  uArquivo in 'models\uArquivo.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
