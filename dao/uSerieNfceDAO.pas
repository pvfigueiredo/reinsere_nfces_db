unit uSerieNfceDAO;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces, Aurelius.Criteria.Projections,

  UGenericoDAO, uSerieNfceVO, uConnection, UBanco, UAureliusConnection, Database.Session.Types,
  Database.Session;

type
  TSerieNfceDAO = class(TGenericoDAO<TSerieNfceVO>)
    destructor Destroy; override;
  public
    function GetSerieNfce: TSerieNfceVO;
  end;
implementation

{ TSerieNfceDAO }

destructor TSerieNfceDAO.Destroy;
begin
  FreeAndNil(FConexao);
  inherited;
end;

function TSerieNfceDAO.GetSerieNfce: TSerieNfceVO;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TSerieNfceVO>.UniqueResult;

  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;

  end;
end;

end.
