unit uVendasCartoesDao;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces,Aurelius.Criteria.Base,


  UAureliusConnection, Database.Session.Types,
  Database.Session,uConnection,
  UGenericoDao, uVendasCartoesVO, UBanco;

type
  TVendasCartoesDao = class(TGenericoDAO<TVendasCartoesVO>)
  private
    FBanco: TBanco;
  public
    constructor Create(Banco: TBanco); overload;
    function GetObjectList(Filter: TCustomCriterion):
      TObjectList<TVendasCartoesVO>; overload;
    function Select(Filter: TCustomCriterion): TVendasCartoesVO; overload;


  end;

implementation

{ TVendasCartoesDao }

constructor TVendasCartoesDao.Create(Banco: TBanco);
begin
  inherited Create(Banco);
  FBanco := Banco;
end;

function TVendasCartoesDao.GetObjectList(
  Filter: TCustomCriterion): TObjectList<TVendasCartoesVO>;
var
 Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      Exit(nil);
    Result := nil;
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TVendasCartoesVO>.Add(Filter).Refreshing.List;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;

  end;

end;

function TVendasCartoesDao.Select(Filter: TCustomCriterion): TVendasCartoesVO;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      Exit(nil);
    Result := nil;
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TVendasCartoesVO>.Add(Filter).UniqueResult;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;

  end;

end;

end.
