unit uEmpresaDao;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces,

  UGenericoDAO, uConnection, UBanco, UAureliusConnection, uEmpresaVO,
  Database.Session.Types, Database.Session;
type
  TEmpresaDao = class(TGenericoDAO<TEmpresaVO>)
    constructor Create(Banco: TBanco);
  private
    FConexao: TConnection;
  public
    function GetEmpresa: TEmpresaVO;
  end;
implementation

{ TEmpresaDao }

constructor TEmpresaDao.Create(Banco: TBanco);
begin
  FConexao := TConnection.Create(Banco);
end;

function TEmpresaDao.GetEmpresa: TEmpresaVO;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TEmpresaVO>.Refreshing.UniqueResult;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;
  end;

end;

end.
