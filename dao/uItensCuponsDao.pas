unit uItensCuponsDao;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces,Aurelius.Criteria.Base,


  UAureliusConnection, Database.Session.Types,
  Database.Session,uConnection,
  UGenericoDao, uItensCuponsVO, UBanco;

type
  TItensCuponsDao = class(TGenericoDAO<TItensCuponsVO>)
  private
    FBanco: TBanco;
  public
    constructor Create(Banco: TBanco); overload;
    function GetObjectList(Filter: TCustomCriterion): TObjectList<TItensCuponsVO>;

  end;
implementation

{ TItensCuponsDao }

constructor TItensCuponsDao.Create(Banco: TBanco);
begin
  inherited Create(Banco);
  FBanco := Banco;
end;

function TItensCuponsDao.GetObjectList(
  Filter: TCustomCriterion): TObjectList<TItensCuponsVO>;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      Exit(nil);
    Result := nil;
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TItensCuponsVO>.Add(Filter).Refreshing.List;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;

  end;

end;

end.
