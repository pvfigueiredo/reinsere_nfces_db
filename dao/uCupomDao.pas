unit uCupomDao;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces, Aurelius.Criteria.Projections,

  UGenericoDAO, uCupomVO, uConnection, UBanco, UAureliusConnection, Database.Session.Types,
  Database.Session;

type
  TCupomDao = class(TGenericoDAO<TCupomVO>)
    destructor Destroy; override;
  public
    function SelectCupom(WhereKey, WhereValue: string): TCupomVO;
  end;

implementation

{ TCupomDao }

destructor TCupomDao.Destroy;
begin
  FreeAndNil(FConexao);
  inherited;
end;

function TCupomDao.SelectCupom(WhereKey, WhereValue: string): TCupomVO;
var
  Session: IDatabaseSession;
  Lista: TObjectList<TCupomVO>;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Lista := Session.ObjectManager.Find<TCupomVO>.Where(Linq[WhereKey] = WhereValue).
      Refreshing.List;
    Result := Lista[0];
  except
    on E: EArgumentOutOfRangeException do
    begin
      Result:= nil;
      exit;
    end;
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;
  end;

end;

end.
