unit uNfceDao;

interface
uses
  Aurelius.Engine.ObjectManager, SysUtils, Aurelius.SQL.Firebird,
  Aurelius.Engine.DatabaseManager, Aurelius.Sql.Register, Aurelius.Criteria.Linq,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Stan.Option, Generics.Collections,
  Aurelius.Drivers.Interfaces, Aurelius.Criteria.base,

  UGenericoDAO, uConnection, UBanco, uNfceVO, UAureliusConnection,
  Database.Session.Types, Database.Session;

type
  TNfceDao = class(TGenericoDAO<TNfceVO>)
  public
    function GetList(WhereKey, WhereValue: string): TObjectList<TNfceVO>; overload;
    function GetList(Order: TOrder): TObjectList<TNfceVO>; overload;
    function GetList(Filter: TCustomCriterion): TObjectList<TNfceVO>; overload;
    function GetList(Filter: TCustomCriterion;
      Order: TOrder): TObjectList<TNfceVO>; overload;
    function Select(Filter: TCustomCriterion): TNfceVO;
  end;

implementation

{ TNfceDao }

function TNfceDao.GetList(WhereKey, WhereValue: string): TObjectList<TNfceVO>;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TNfceVO>.Where(Linq[WhereKey] =
      WhereValue).Refreshing.List;
  except
    on E: Exception do
    begin
      result := nil;
      raise;
    end;

  end;

end;
function TNfceDao.Select(Filter: TCustomCriterion): TNfceVO;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      Exit(nil);
    Result := nil;
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TNfceVO>.Add(Filter).UniqueResult;
  except
    on E: Exception do
    begin
      Result := nil;
      raise;
    end;

  end;

end;

function TNfceDao.GetList(Order: TOrder): TObjectList<TNfceVO>;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TNfceVO>.AddOrder(Order).Refreshing.List;
  except
    on E: Exception do
    begin
      result := nil;
      raise;
    end;

  end;

end;

function TNfceDao.GetList(Filter: TCustomCriterion;
  Order: TOrder): TObjectList<TNfceVO>;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TNfceVO>.Where(Filter).AddOrder(Order).
      Refreshing.List;
  except
    on E: Exception do
    begin
      result := nil;
      raise;
    end;

  end;

end;

function TNfceDao.GetList(Filter: TCustomCriterion): TObjectList<TNfceVO>;
var
  Session: IDatabaseSession;
begin
  try
    if not FConexao.CriaConexao then
      exit(nil);
    Session := TDatabaseSession.Create(FConexao.Connection);
    Result := Session.ObjectManager.Find<TNfceVO>.Where(Filter).Refreshing.List;
  except
    on E: Exception do
    begin
      result := nil;
      raise;
    end;

  end;

end;

end.
