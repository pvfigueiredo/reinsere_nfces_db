unit uCodigoAnpVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
 [Entity]
 [Automapping]
 [Table('codigo_anp')]
 [Id('FCodigoAnpId', TIdGenerator.None)]
  TCodigoAnpVO = class
  private
    FCodigoAnpId: string;
    FCodigoAnpDescricao: string;
  public
    property CodigoAnpId: string read FCodigoAnpId write FCodigoAnpId;
    property CodigoAnpDescricao: string read FCodigoAnpDescricao write FCodigoAnpDescricao;
  end;
implementation
end.