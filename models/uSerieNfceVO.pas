unit uSerieNfceVO;

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;

type
  [Entity]
  [Automapping]
  [Table('SERIES_NFCE')]
  [Id('FSeriesNfceId', TIdGenerator.None)]
  TSerieNfceVO = class
  private
    FSeriesNfceId,
    FSerieNfceNumeracao: integer;
    FSerieContingencia: boolean;
    FCaixaNfceNumero: integer;
  public
    property SeriesNfceId: integer read FSeriesNfceId
      write FSeriesNfceId;
    property SerieNfceNumeracao: integer read FSerieNfceNumeracao
      write FSerieNfceNumeracao;
    property SerieContingencia: boolean read FSerieContingencia
      write FSerieContingencia;
    property CaixaNfceNumero: integer read FCaixaNfceNumero
      write FCaixaNfceNumero;
  end;
implementation

end.
