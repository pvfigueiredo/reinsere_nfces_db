unit uNfceVO;

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type
  [Entity]
  [Automapping]
  [Table('NFCE')]
  [Sequence('NFCE')]
  [Id('FNfceId', TIdGenerator.IdentityOrSequence)]
  TNfceVO = class
    protected
      FNfceId,
      FNfceNumero: integer;
      FNfceModelo: string;
      FNfceSerie: integer;
      [Column('NFCE_CHAVEACESSO')]
      FNfceChaveAcesso: string;
      FNfceCodigoRetorno,
      FNfceProtocolo,
      FNfceRecibo: string;
      FNfceDataEmissao,
      FNfceDataTransmissao: TDateTime;
      FNfceValorImpostos,
      FNfceValorImpostosFederais: Currency;
      [Column('NFCE_BACKUP')]
      FNfceBackUp: boolean;
    public
      property NfceId: integer read FNfceId write FNfceId;
      property NfceNumero: integer read FNfceNumero write FNfceNumero;
      property NfceModelo: string read FNfceModelo write FNfceModelo;
      property NfceSerie: integer read FNfceSerie write FNfceSerie;
      property NfceChaveAcesso: string read FNfceChaveAcesso
        write FNfceChaveAcesso;
      property NfceCodigoRetorno: string read FNfceCodigoRetorno
        write FNfceCodigoRetorno;
      property NfceProtocolo: string read FNfceProtocolo write FNfceProtocolo;
      property NfceRecibo: string read FNfceRecibo write FNfceRecibo;
      property NfceDataEmissao: TDateTime read FNfceDataEmissao
        write FNfceDataEmissao;
      property NfceDataTransmissao: TDateTime read FNfceDataTransmissao
        write FNfceDataTransmissao;
      property NfceValorImpostos: Currency read FNfceValorImpostos
        write FNfceValorImpostos;
      property NfceValorImpostosFederais: Currency read FNfceValorImpostosFederais
        write FNfceValorImpostosFederais;
      property NfceBackUp: boolean read FNfceBackUp write FNfceBackUp;
  end;

implementation

end.
