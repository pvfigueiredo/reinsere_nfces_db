unit uOperadorasCartaoVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
 [Entity]
 [Automapping]
 [Table('operadoras_cartao')]
 [Id('FOperadorasCartaoId', TIdGenerator.None)]
  TOperadorasCartaoVO = class
  private
    FOperadoraNome: string;
    FOperadoraComissao: double;
    FOperadorasCartaoId: integer;
    FOperadoraDC: string;
    FOperadoraCnpj: string;
    FOperadoraInstituicao: string;
  public
    property OperadoraNome: string read FOperadoraNome write FOperadoraNome;
    property OperadoraComissao: double read FOperadoraComissao write FOperadoraComissao;
    property OperadorasCartaoId: integer read FOperadorasCartaoId write FOperadorasCartaoId;
    property OperadoraDC: string read FOperadoraDC write FOperadoraDC;
    property OperadoraCnpj: string read FOperadoraCnpj write FOperadoraCnpj;
    property OperadoraInstituicao: string read FOperadoraInstituicao write FOperadoraInstituicao;
  end;
implementation
end.