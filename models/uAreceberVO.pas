unit uAreceberVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('Areceber')]
  [Id('FAreceberId', TIdGenerator.None)]
  TAreceberVO = class
  private
    FAreceberValor: double;
    FAreceberSaldo: double;
    FAreceberPago: double;
    FAreceberId: integer;
    FCuponsId: integer;
    FNfeId: integer;
    FModalidadesId: integer;
    FAreceberParcela: integer;
    FAreceberEmissao: TDate;
    FAreceberVencimento: TDate;
    FAreceberPagamento: TDate;
    FAreceberEcfTotParcial: string;
    FAreceberDocumento: string;
  public
    property AreceberValor: double read FAreceberValor write FAreceberValor;
    property AreceberSaldo: double read FAreceberSaldo write FAreceberSaldo;
    property AreceberPago: double read FAreceberPago write FAreceberPago;
    property AreceberId: integer read FAreceberId write FAreceberId;
    property CuponsId: integer read FCuponsId write FCuponsId;
    property NfeId: integer read FNfeId write FNfeId;
    property ModalidadesId: integer read FModalidadesId write FModalidadesId;
    property AreceberParcela: integer read FAreceberParcela write FAreceberParcela;
    property AreceberEmissao: TDate read FAreceberEmissao write FAreceberEmissao;
    property AreceberVencimento: TDate read FAreceberVencimento write FAreceberVencimento;
    property AreceberPagamento: TDate read FAreceberPagamento write FAreceberPagamento;
    property AreceberEcfTotParcial: string read FAreceberEcfTotParcial write FAreceberEcfTotParcial;
    property AreceberDocumento: string read FAreceberDocumento write FAreceberDocumento;
  end;
implementation
end.