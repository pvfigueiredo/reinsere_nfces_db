unit uUnidadesVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('unidades')]
  [Id('FUnidadesId', TIdGenerator.None)]
  TUnidadesVO = class
  private
    FUnidadesId: integer;
    FUnidadeNome: string;
    FUnidadeFracionada: boolean;
    FUnidadeSigla: string;
  public
    property UnidadesId: integer read FUnidadesId write FUnidadesId;
    property UnidadeNome: string read FUnidadeNome write FUnidadeNome;
    property UnidadeFracionada: boolean read FUnidadeFracionada write FUnidadeFracionada;
    property UnidadeSigla: string read FUnidadeSigla write FUnidadeSigla;
  end;
implementation
end.