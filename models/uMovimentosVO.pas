unit uMovimentosVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('movimentos')]
  [Id('FMovimentosId', TIdGenerator.None)]
  TMovimentosVO = class
  private
    FMovDataAbertura: TDateTime;
    FMovTotalSuprimento: double;
    FMovTotalSangria: double;
    FMovTotalNaoFiscal: double;
    FMovTotalVenda: double;
    FMovTotalDesconto: double;
    FMovTotalAcrescimo: double;
    FMovTotalRecebido: double;
    FMovTotalCancelado: double;
    FMovTotalTroco: double;
    FMovDataFechamento: TDateTime;
    FMovimentosId: integer;
    FFuncionariosId: integer;
    FTurnosId: integer;
    FMovQteFechamentos: integer;
    FMovEcfNumUsuario: integer;
    FMovEcfCaixa: integer;
    FMovEcfNumFabrica: string;
    FMovEcfMfAdicional: boolean;
    FMovEcfModelo: string;
    FMovimentoEcfMarca: string;
    FMovStatus: string;
    FMovScanImportado: boolean;
  public
    property MovDataAbertura: TDateTime read FMovDataAbertura write FMovDataAbertura;
    property MovTotalSuprimento: double read FMovTotalSuprimento write FMovTotalSuprimento;
    property MovTotalSangria: double read FMovTotalSangria write FMovTotalSangria;
    property MovTotalNaoFiscal: double read FMovTotalNaoFiscal write FMovTotalNaoFiscal;
    property MovTotalVenda: double read FMovTotalVenda write FMovTotalVenda;
    property MovTotalDesconto: double read FMovTotalDesconto write FMovTotalDesconto;
    property MovTotalAcrescimo: double read FMovTotalAcrescimo write FMovTotalAcrescimo;
    property MovTotalRecebido: double read FMovTotalRecebido write FMovTotalRecebido;
    property MovTotalCancelado: double read FMovTotalCancelado write FMovTotalCancelado;
    property MovTotalTroco: double read FMovTotalTroco write FMovTotalTroco;
    property MovDataFechamento: TDateTime read FMovDataFechamento write FMovDataFechamento;
    property MovimentosId: integer read FMovimentosId write FMovimentosId;
    property FuncionariosId: integer read FFuncionariosId write FFuncionariosId;
    property TurnosId: integer read FTurnosId write FTurnosId;
    property MovQteFechamentos: integer read FMovQteFechamentos write FMovQteFechamentos;
    property MovEcfNumUsuario: integer read FMovEcfNumUsuario write FMovEcfNumUsuario;
    property MovEcfCaixa: integer read FMovEcfCaixa write FMovEcfCaixa;
    property MovEcfNumFabrica: string read FMovEcfNumFabrica write FMovEcfNumFabrica;
    property MovEcfMfAdicional: boolean read FMovEcfMfAdicional write FMovEcfMfAdicional;
    property MovEcfModelo: string read FMovEcfModelo write FMovEcfModelo;
    property MovimentoEcfMarca: string read FMovimentoEcfMarca write FMovimentoEcfMarca;
    property MovStatus: string read FMovStatus write FMovStatus;
    property MovScanImportado: boolean read FMovScanImportado write FMovScanImportado;
  end;
implementation
end.