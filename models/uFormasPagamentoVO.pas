unit uFormasPagamentoVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('formas_pagamento')]
  [Id('FFormasPagamentoId', TIdGenerator.None)]
  TFormasPagamentoVO = class
  private
    FFormasPagamentoId: integer;
    FFormaPagamentoNome: string;
  public
    property FormasPagamentoId: integer read FFormasPagamentoId write FFormasPagamentoId;
    property FormaPagamentoNome: string read FFormaPagamentoNome write FFormaPagamentoNome;
  end;
implementation
end.