unit uConfiguracoesVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
 [Entity]
 [Automapping]
 [Table('configuracoes')]
 [Id('FConfiguracoesId', TIdGenerator.None)]
  TConfiguracoesVO = class
  private
    FConfiguracoesId: integer;
    FConfigTipo: integer;
    FConfigValor: string;
    FConfigNome: string;
    FConfigObs: string;
    FReadonly: boolean;
  public
    property ConfiguracoesId: integer read FConfiguracoesId write FConfiguracoesId;
    property ConfigTipo: integer read FConfigTipo write FConfigTipo;
    property ConfigValor: string read FConfigValor write FConfigValor;
    property ConfigNome: string read FConfigNome write FConfigNome;
    property ConfigObs: string read FConfigObs write FConfigObs;
    property aReadonly: boolean read FReadonly write FReadonly;
  end;
implementation
end.