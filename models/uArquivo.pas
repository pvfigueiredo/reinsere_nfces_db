unit uArquivo;

interface

uses
  SysUtils;

type
  TArquivo = class
  private
    FCaminhoAbsoluto,
    FCaminhoRede,
    FHost,
    FExtensaoArquivo: string;
    FArquivo: string;

    function GetCaminhoRede: string;
    function GetHost: string;
    function GetArquivo: string;
    function GetExtensaoArquivo: string;
  public
    property CaminhoAbsoluto: string read FCaminhoAbsoluto write FCaminhoAbsoluto;
    property CaminhoRede: string read FCaminhoRede write FCaminhoRede;
    property Host: string read FHost write FHost;
    property Arquivo: string read FArquivo write FArquivo;
    property ExtensaoArquivo: string read FExtensaoArquivo write FExtensaoArquivo;

    constructor Create(Caminho: string);
  end;
implementation

{ TArquivo }

constructor TArquivo.Create(Caminho: string);
begin
  FCaminhoAbsoluto := Caminho;

end;

function TArquivo.GetArquivo: string;
begin

end;

function TArquivo.GetCaminhoRede: string;
begin

end;

function TArquivo.GetExtensaoArquivo: string;
begin

end;

function TArquivo.GetHost: string;
begin

end;

end.
