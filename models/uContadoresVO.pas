unit uContadoresVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('contadores')]
  [Id('FContadoresId', TIdGenerator.None)]
  TContadoresVO = class
  private
    FContadorCpf: string;
    FContadoresId: integer;
    FContadorCrcUf: string;
    FContadorCnpjEsc: string;
    FContadorEndNumero: integer;
    FContadorNome: string;
    FContadorIdentidade: string;
    FContadorCrc: string;
    FContadorEndereco: string;
    FContadorComplemento: string;
    FContadorBairro: string;
    FContadorTelefone: string;
    FContadorFax: string;
    FContadorEmail: string;
    FContadorCidade: string;
    FContadorCodIbge: string;
    FContadorCep: string;
  public
    property ContadorCpf: string read FContadorCpf write FContadorCpf;
    property ContadoresId: integer read FContadoresId write FContadoresId;
    property ContadorCrcUf: string read FContadorCrcUf write FContadorCrcUf;
    property ContadorCnpjEsc: string read FContadorCnpjEsc write FContadorCnpjEsc;
    property ContadorEndNumero: integer read FContadorEndNumero write FContadorEndNumero;
    property ContadorNome: string read FContadorNome write FContadorNome;
    property ContadorIdentidade: string read FContadorIdentidade write FContadorIdentidade;
    property ContadorCrc: string read FContadorCrc write FContadorCrc;
    property ContadorEndereco: string read FContadorEndereco write FContadorEndereco;
    property ContadorComplemento: string read FContadorComplemento write FContadorComplemento;
    property ContadorBairro: string read FContadorBairro write FContadorBairro;
    property ContadorTelefone: string read FContadorTelefone write FContadorTelefone;
    property ContadorFax: string read FContadorFax write FContadorFax;
    property ContadorEmail: string read FContadorEmail write FContadorEmail;
    property ContadorCidade: string read FContadorCidade write FContadorCidade;
    property ContadorCodIbge: string read FContadorCodIbge write FContadorCodIbge;
    property ContadorCep: string read FContadorCep write FContadorCep;
  end;
implementation
end.