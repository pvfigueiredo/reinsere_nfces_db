 unit uCupomVo;

interface

uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections, uItensCuponsVO,
  uFormasPagamentoVO, uClientesVO, uNfceVO;

type
  [Entity]
  [Automapping]
  [Table('CUPONS')]
  [Id('FCuponsId', TIdGenerator.None)]
  TCupomVO = class
  private
    FCuponsId: integer;
    FMovimentosId: integer;
    FFuncionariosId: integer;
    [JoinColumn('CLIENTES_ID', [], 'CLIENTES_ID')]
    FCliente: TClientesVO;
    FCupomNomeConsFinal: string;
    FCupomCpfCnpjConsFinal: string;
    FCupomStatus: string;
    FCupomEcfCaixa: integer;
    FCupomEcfNumFabrica: string;
    FCupomEcfMfAdicional: boolean;
    FCupomEcfModelo: string;
    FCupomEcfNumUsuario: integer;
    FCupomEcfCcf: integer;
    FCupomEcfCoo: integer;
    FCupomDataEmissao: TDateTime;
    FCupomTotalBruto: currency;
    FCupomDescAcre: currency;
    FCupomTotalLiquido: currency;
    FCupomTotalRecebido: currency;
    FCupomTotalTroco: currency;
    FCupomCancelado: boolean;
    FCupomDav: boolean;
    FCupomPreVenda: boolean;
    FClienteFidelidadeId: integer;
    FCupomEcfMarca: string;
    FCupomDescAcreItem: currency;
    //FCupomNumNotaManual: string;
    FDavIdentificador: string;
    FCupomObs: string;
    FDependentesId: integer;
    FCupomValImportacao: integer;
    FCupomLogImportacao: string;
    FCupomUtilizaTef: boolean;
    FCupomStatusTef: integer;
    FCupomIdPai: integer;
    FCupomStatusImportacao: integer;
    FCupomOrigem: string;
    FCupomValorCancelamento: currency;
    FDescontoScanntech: currency;
    FCupomAbandonado: boolean;
    [JoinColumn('FORMAS_PAGAMENTO_ID', [], 'FORMAS_PAGAMENTO_ID')]
    FFormasPagamento: TFormasPagamentoVO;
    [JoinColumn('NFCE_ID', [], 'NFCE_ID')]
    FNfce: TNfceVO;
    FCupomValorBaseIcms: currency;
    FCupomValorIcms: currency;
    FCupomContingencia: boolean;
    FCupomNfceGerada: boolean;
    FCupomNfceErro: string;
  public

    property CuponsId: integer read FCuponsId write FCuponsId;
    property MovimentosId: integer read FMovimentosId write FMovimentosId;
    property FuncionariosId: integer read FFuncionariosId write FFuncionariosId;
    property Cliente: TClientesVO read FCliente write FCliente;
    property CupomNomeConsFinal: string read FCupomNomeConsFinal write
      FCupomNomeConsFinal;
    property CupomCpfCnpjConsFinal: string read FCupomCpfCnpjConsFinal write
      FCupomCpfCnpjConsFinal;
    property CupomStatus: string read FCupomStatus write FCupomStatus;
    property CupomEcfCaixa: integer read FCupomEcfCaixa write FCupomEcfCaixa;
    property CupomEcfNumFabrica: string read FCupomEcfNumFabrica write
      FCupomEcfNumFabrica;
    property CupomEcfMfAdicional: boolean read FCupomEcfMfAdicional write
      FCupomEcfMfAdicional;
    property CupomEcfModelo: string read FCupomEcfModelo write FCupomEcfModelo;
    property CupomEcfNumUsuario: integer read FCupomEcfNumUsuario write
      FCupomEcfNumUsuario;
    property CupomEcfCcf: integer read FCupomEcfCcf write FCupomEcfCcf;
    property CupomEcfCoo: integer read FCupomEcfCoo write FCupomEcfCoo;
    property CupomDataEmissao: TDateTime read FCupomDataEmissao write FCupomDataEmissao;
    property CupomTotalBruto: currency read FCupomTotalBruto write FCupomTotalBruto;
    property CupomDescAcre: currency read FCupomDescAcre write FCupomDescAcre;
    property CupomTotalLiquido: currency read FCupomTotalLiquido write FCupomTotalLiquido;
    property CupomTotalRecebido: currency read FCupomTotalRecebido write
      FCupomTotalRecebido;
    property CupomTotalTroco: currency read FCupomTotalTroco write FCupomTotalTroco;
    property CupomCancelado: boolean read FCupomCancelado write FCupomCancelado;
    property CupomDav: boolean read FCupomDav write FCupomDav;
    property CupomPreVenda: boolean read FCupomPreVenda write FCupomPreVenda;
    property ClienteFidelidadeId: integer read FClienteFidelidadeId write
      FClienteFidelidadeId;
    property CupomEcfMarca: string read FCupomEcfMarca write FCupomEcfMarca;
    property CupomDescAcreItem: currency read FCupomDescAcreItem write FCupomDescAcreItem;
    //property CupomNumNotaManual: string read FCupomNumNotaManual write
    //  FCupomNumNotaManual;
    property DavIdentificador: string read FDavIdentificador write FDavIdentificador;
    property CupomObs: string read FCupomObs write FCupomObs;
    property dependentesId: integer read FDependentesId write FdependentesId;
    property CupomValImportacao: integer read FCupomValImportacao write
      FCupomValImportacao;
    property CupomLogImportacao: string read FCupomLogImportacao write
      FCupomLogImportacao;
    property CupomUtilizaTef: boolean read FCupomUtilizaTef write FCupomUtilizaTef;
    property CupomStatusTef: integer read FCupomStatusTef write FCupomStatusTef;
    property CupomIdPai: integer read FCupomIdPai write FCupomIdPai;
    property CupomStatusImportacao: integer read FCupomStatusImportacao write
      FCupomStatusImportacao;
    property CupomOrigem: string read FCupomOrigem write FCupomOrigem;
    property CupomValorCancelamento: currency read FCupomValorCancelamento write
      FCupomValorCancelamento;
    property DescontoScanntech: currency read FDescontoScanntech write FDescontoScanntech;
    property CupomAbandonado: boolean read FCupomAbandonado write FCupomAbandonado;
    property FormasPagamento: TFormasPagamentoVO read FFormasPagamento write FFormasPagamento;
    property Nfce: TNfceVO read FNfce write FNfce;
    property CupomValorBaseIcms: currency read FCupomValorBaseIcms write
      FCupomValorBaseIcms;
    property CupomValorIcms: currency read FCupomValorIcms write FCupomValorIcms;
    property CupomContingencia: boolean read FCupomContingencia write FCupomContingencia;
    property CupomNfceGerada: boolean read FCupomNfceGerada write FCupomNfceGerada;
    property CupomNfceErro: string read FCupomNfceErro write FCupomNfceErro;
  end;

implementation

end.

