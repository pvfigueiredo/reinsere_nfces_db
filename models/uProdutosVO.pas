unit uProdutosVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections, uCodigoAnpVO,
  uUnidadesVO;
type 
 [Entity]
 [Automapping]
 [Table('produtos')]
 [Id('FProdutosId', TIdGenerator.None)]
  TProdutosVO = class
  private
    FProdutoCodBarras: string;
    FProdutoPreco: double;
    FProdutoEstoque: double;
    FProdutoPrecoCusto: double;
    FProdutoNcmAliqNac: double;
    FProdutosId: integer;
    FAliquotasId: integer;
    [JoinColumn('UNIDADES_ID', [], 'UNIDADES_ID')]
    FUnidades: TUnidadesVO;
    FTiposMercadoriaId: integer;
    FProdutoNome: string;
    FProdutoNomeFiscal: string;
    FProdutoComposto: boolean;
    FProdutoNcm: string;
    FProdutoCfop: string;
    FProdutoIat: string;
    FProdutoIppt: string;
    FCstId: string;
    FProdutoBalanca: boolean;
    FProdutoNcmAliqEst: double;
    FProdutoNcmAliqMun: double;
    FProdutoDesativado: boolean;
    FProdutoPrecoAtacado: double;
    FProdutoQteEmbalagem: double;
    FProdutoUtilizaSerial: boolean;
    FKitsId: integer;
    FProdutoUtilizaGrade: boolean;
    FProdutoBaixaEstoque: boolean;
    FProdutoMovimentaEstoque: integer;
    FProdutoMovimentaQuant: double;
    FProdutoCodFab: string;
    FCsosnId: string;
    FCstPisId: string;
    FCstCofinsId: string;
    FProdutoPis: double;
    FProdutoCofins: double;
    FCodigoNaturezaPiscofinsId: string;
    FProdutoIcmsRed: double;
    [JoinColumn('CODIGO_ANP_ID', [], 'CODIGO_ANP_ID')]
    FCodigoAnp: TCodigoAnpVO;
    FProdutoPercGnn: double;
    FProdutoPercGni: double;
    FProdutoPercGlp: double;
    FProdutoValorPartida: double;
    FProdutoPesoLiquido: double;
    FProdutoCest: string;
    FProdutoCodbarrasEmb: string;
  public
    property ProdutoCodBarras: string read FProdutoCodBarras write FProdutoCodBarras;
    property ProdutoPreco: double read FProdutoPreco write FProdutoPreco;
    property ProdutoEstoque: double read FProdutoEstoque write FProdutoEstoque;
    property ProdutoPrecoCusto: double read FProdutoPrecoCusto write FProdutoPrecoCusto;
    property ProdutoNcmAliqNac: double read FProdutoNcmAliqNac write FProdutoNcmAliqNac;
    property ProdutosId: integer read FProdutosId write FProdutosId;
    property AliquotasId: integer read FAliquotasId write FAliquotasId;
    property Unidades: TUnidadesVO read FUnidades write FUnidades;
    property TiposMercadoriaId: integer read FTiposMercadoriaId write FTiposMercadoriaId;
    property ProdutoNome: string read FProdutoNome write FProdutoNome;
    property ProdutoNomeFiscal: string read FProdutoNomeFiscal write FProdutoNomeFiscal;
    property ProdutoComposto: boolean read FProdutoComposto write FProdutoComposto;
    property ProdutoNcm: string read FProdutoNcm write FProdutoNcm;
    property ProdutoCfop: string read FProdutoCfop write FProdutoCfop;
    property ProdutoIat: string read FProdutoIat write FProdutoIat;
    property ProdutoIppt: string read FProdutoIppt write FProdutoIppt;
    property CstId: string read FCstId write FCstId;
    property ProdutoBalanca: boolean read FProdutoBalanca write FProdutoBalanca;
    property ProdutoNcmAliqEst: double read FProdutoNcmAliqEst write FProdutoNcmAliqEst;
    property ProdutoNcmAliqMun: double read FProdutoNcmAliqMun write FProdutoNcmAliqMun;
    property ProdutoDesativado: boolean read FProdutoDesativado write FProdutoDesativado;
    property ProdutoPrecoAtacado: double read FProdutoPrecoAtacado write FProdutoPrecoAtacado;
    property ProdutoQteEmbalagem: double read FProdutoQteEmbalagem write FProdutoQteEmbalagem;
    property ProdutoUtilizaSerial: boolean read FProdutoUtilizaSerial write FProdutoUtilizaSerial;
    property KitsId: integer read FKitsId write FKitsId;
    property ProdutoUtilizaGrade: boolean read FProdutoUtilizaGrade write FProdutoUtilizaGrade;
    property ProdutoBaixaEstoque: boolean read FProdutoBaixaEstoque write FProdutoBaixaEstoque;
    property ProdutoMovimentaEstoque: integer read FProdutoMovimentaEstoque write FProdutoMovimentaEstoque;
    property ProdutoMovimentaQuant: double read FProdutoMovimentaQuant write FProdutoMovimentaQuant;
    property ProdutoCodFab: string read FProdutoCodFab write FProdutoCodFab;
    property CsosnId: string read FCsosnId write FCsosnId;
    property CstPisId: string read FCstPisId write FCstPisId;
    property CstCofinsId: string read FCstCofinsId write FCstCofinsId;
    property ProdutoPis: double read FProdutoPis write FProdutoPis;
    property ProdutoCofins: double read FProdutoCofins write FProdutoCofins;
    property CodigoNaturezaPiscofinsId: string read FCodigoNaturezaPiscofinsId write FCodigoNaturezaPiscofinsId;
    property ProdutoIcmsRed: double read FProdutoIcmsRed write FProdutoIcmsRed;
    property CodigoAnp: TCodigoAnpVO read FCodigoAnp write FCodigoAnp;
    property ProdutoPercGnn: double read FProdutoPercGnn write FProdutoPercGnn;
    property ProdutoPercGni: double read FProdutoPercGni write FProdutoPercGni;
    property ProdutoPercGlp: double read FProdutoPercGlp write FProdutoPercGlp;
    property ProdutoValorPartida: double read FProdutoValorPartida write FProdutoValorPartida;
    property ProdutoPesoLiquido: double read FProdutoPesoLiquido write FProdutoPesoLiquido;
    property ProdutoCest: string read FProdutoCest write FProdutoCest;
    property ProdutoCodbarrasEmb: string read FProdutoCodbarrasEmb write FProdutoCodbarrasEmb;
  end;
implementation
end.