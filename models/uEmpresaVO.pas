unit uEmpresaVO;

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections, uContadoresVO;
type
  [Entity]
  [Automapping]
  [Table('empresa')]
  [Id('FEmpresaId', TIdGenerator.None)]
  TEmpresaVO = class
  private
    FEmpresaIe: string;
    FEmpresaIeSt: string;
    FEmpresaIm: string;
    FEmpresaCalculoCreditoSn: double;
    FEmpresaId: integer;
    [JoinColumn('CONTADORES_ID', [], 'CONTADORES_ID')]
    FContadores: TContadoresVO;
    FEmpresaCnpj: string;
    FEmpresaSuframa: string;
    FEmpresaEmail: string;
    FEmpresaNumero: integer;
    FEmpresaBairro: string;
    FEmpresaUf: string;
    FEmpresaCodIbgeCidade: integer;
    FEmpresaCodIbgeUf: integer;
    FEmpresaRegistrosConsulta: integer;
    FEmpresaVencIcms: integer;
    FEmpresaResponsavel: string;
    FEmpresaRazaoSocial: string;
    FEmpresaNomeFantasia: string;
    FEmpresaLogotipo: string;
    FEmpresaEndereco: string;
    FEmpresaComplemento: string;
    FEmpresaCep: string;
    FEmpresaCidade: string;
    FEmpresaFone: string;
    FEmpresaEmailSenha: string;
    FEmpresaCrt: string;
    FEmpresaRegimeContabil: string;
    FEmpresaCnae: string;
    FEmpresaIcmsSimples: double;
  public
    property EmpresaIe: string read FEmpresaIe write FEmpresaIe;
    property EmpresaIeSt: string read FEmpresaIeSt write FEmpresaIeSt;
    property EmpresaIm: string read FEmpresaIm write FEmpresaIm;
    property EmpresaCalculoCreditoSn: double read FEmpresaCalculoCreditoSn write FEmpresaCalculoCreditoSn;
    property EmpresaId: integer read FEmpresaId write FEmpresaId;
    property Contador: TContadoresVO read FContadores write FContadores;
    property EmpresaCnpj: string read FEmpresaCnpj write FEmpresaCnpj;
    property EmpresaSuframa: string read FEmpresaSuframa write FEmpresaSuframa;
    property EmpresaEmail: string read FEmpresaEmail write FEmpresaEmail;
    property EmpresaNumero: integer read FEmpresaNumero write FEmpresaNumero;
    property EmpresaBairro: string read FEmpresaBairro write FEmpresaBairro;
    property EmpresaUf: string read FEmpresaUf write FEmpresaUf;
    property EmpresaCodIbgeCidade: integer read FEmpresaCodIbgeCidade write FEmpresaCodIbgeCidade;
    property EmpresaCodIbgeUf: integer read FEmpresaCodIbgeUf write FEmpresaCodIbgeUf;
    property EmpresaRegistrosConsulta: integer read FEmpresaRegistrosConsulta write FEmpresaRegistrosConsulta;
    property EmpresaVencIcms: integer read FEmpresaVencIcms write FEmpresaVencIcms;
    property EmpresaResponsavel: string read FEmpresaResponsavel write FEmpresaResponsavel;
    property EmpresaRazaoSocial: string read FEmpresaRazaoSocial write FEmpresaRazaoSocial;
    property EmpresaNomeFantasia: string read FEmpresaNomeFantasia write FEmpresaNomeFantasia;
    property EmpresaLogotipo: string read FEmpresaLogotipo write FEmpresaLogotipo;
    property EmpresaEndereco: string read FEmpresaEndereco write FEmpresaEndereco;
    property EmpresaComplemento: string read FEmpresaComplemento write FEmpresaComplemento;
    property EmpresaCep: string read FEmpresaCep write FEmpresaCep;
    property EmpresaCidade: string read FEmpresaCidade write FEmpresaCidade;
    property EmpresaFone: string read FEmpresaFone write FEmpresaFone;
    property EmpresaEmailSenha: string read FEmpresaEmailSenha write FEmpresaEmailSenha;
    property EmpresaCrt: string read FEmpresaCrt write FEmpresaCrt;
    property EmpresaRegimeContabil: string read FEmpresaRegimeContabil write FEmpresaRegimeContabil;
    property EmpresaCnae: string read FEmpresaCnae write FEmpresaCnae;
    property EmpresaIcmsSimples: double read FEmpresaIcmsSimples write FEmpresaIcmsSimples;
  end;
implementation
end.
