unit uProdutosSeriaisVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
 [Entity]
 [Automapping]
 [Table('produtos_seriais')]
 [Id('FProdutosSeriaisId', TIdGenerator.None)]
  TProdutosSeriaisVO = class
  private
    FProdutosSeriaisId: integer;
    FProdutosId: integer;
    FProdutosSerial: string;
    FProdutosSerialDescricao: string;
    FProdutosSerialDisponivel: boolean;
    FProdutosSerialInclusao: TDateTime;
    FProdutosSerialAlteracao: TDateTime;
    FExcluido: boolean;
  public
    property ProdutosSeriaisId: integer read FProdutosSeriaisId write FProdutosSeriaisId;
    property ProdutosId: integer read FProdutosId write FProdutosId;
    property ProdutosSerial: string read FProdutosSerial write FProdutosSerial;
    property ProdutosSerialDescricao: string read FProdutosSerialDescricao write FProdutosSerialDescricao;
    property ProdutosSerialDisponivel: boolean read FProdutosSerialDisponivel write FProdutosSerialDisponivel;
    property ProdutosSerialInclusao: TDateTime read FProdutosSerialInclusao write FProdutosSerialInclusao;
    property ProdutosSerialAlteracao: TDateTime read FProdutosSerialAlteracao write FProdutosSerialAlteracao;
    property Excluido: boolean read FExcluido write FExcluido;
  end;
implementation
end.