unit uItensCuponsVO;

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections, uProdutosVo,
  uProdutosSeriaisVO;
type

  [Entity]
  [Automapping]
  [Table('ITENS_CUPONS')]
  [Id('FItensCuponsId', TIdGenerator.None)]
  TItensCuponsVO = class
  private
    FItemQte: double;
    FItemValorUnitario: double;
    FItemValorDescAcre: double;
    FItemValorTotal: double;
    FItemDescAcreRateio: double;
    FAliquotaValor: double;
    FItemBaseCalculo: double;
    FItensCuponsId: integer;
    FCuponsId: integer;
    [JoinColumn('PRODUTOS_ID', [], 'PRODUTOS_ID')]
    FProdutos: TProdutosVO;
    FItemItem: integer;
    FAliquotaTotParcial: string;
    FFuncionariosId: integer;
    FItemCancelado: boolean;
    FItemMovimentouEstoque: boolean;
    FItemCfop: string;
    FAliquotasId: integer;
    [JoinColumn('PRODUTOS_SERIAIS_ID', [], 'PRODUTOS_SERIAIS_ID')]
    FProdutosSeriais: TProdutosSeriaisVO;
    FKitsId: integer;
    FGradeProdutosId: integer;
    FProdutoNome: string;
    FItemIdFilho: integer;
    FItemStatusEstoque: integer;
    FProdutoMovimentaEstoque: integer;
    FProdutoMovimentaQuant: double;
    FScanPromocoesId: integer;
    FDescontoScanntech: double;
    FCstPisId: string;
    FCstCofinsId: string;
    FProdutoPis: double;
    FProdutoCofins: double;
    FCodigoNaturezaPiscofinsId: string;
    FItenscupomBasePis: double;
    FItenscupomValorPis: double;
    FItenscupomBaseCofins: double;
    FItenscupomValorCofins: double;
    FCstId: string;
    FCsosnId: string;
    FItemValorIcms: double;
  public
    property ItemQte: double read FItemQte write FItemQte;
    property ItemValorUnitario: double read FItemValorUnitario write FItemValorUnitario;
    property ItemValorDescAcre: double read FItemValorDescAcre write FItemValorDescAcre;
    property ItemValorTotal: double read FItemValorTotal write FItemValorTotal;
    property ItemDescAcreRateio: double read FItemDescAcreRateio write FItemDescAcreRateio;
    property AliquotaValor: double read FAliquotaValor write FAliquotaValor;
    property ItemBaseCalculo: double read FItemBaseCalculo write FItemBaseCalculo;
    property ItensCuponsId: integer read FItensCuponsId write FItensCuponsId;
    property CuponsId: integer read FCuponsId write FCuponsId;
    property Produto: TProdutosVO read FProdutos write FProdutos;
    property ItemItem: integer read FItemItem write FItemItem;
    property AliquotaTotParcial: string read FAliquotaTotParcial write FAliquotaTotParcial;
    property FuncionariosId: integer read FFuncionariosId write FFuncionariosId;
    property ItemCancelado: boolean read FItemCancelado write FItemCancelado;
    property ItemMovimentouEstoque: boolean read FItemMovimentouEstoque write FItemMovimentouEstoque;
    property ItemCfop: string read FItemCfop write FItemCfop;
    property AliquotasId: integer read FAliquotasId write FAliquotasId;
    property ProdutosSeriais: TProdutosSeriaisVO read FProdutosSeriais write FProdutosSeriais;
    property KitsId: integer read FKitsId write FKitsId;
    property GradeProdutosId: integer read FGradeProdutosId write FGradeProdutosId;
    property ProdutoNome: string read FProdutoNome write FProdutoNome;
    property ItemIdFilho: integer read FItemIdFilho write FItemIdFilho;
    property ItemStatusEstoque: integer read FItemStatusEstoque write FItemStatusEstoque;
    property ProdutoMovimentaEstoque: integer read FProdutoMovimentaEstoque write FProdutoMovimentaEstoque;
    property ProdutoMovimentaQuant: double read FProdutoMovimentaQuant write FProdutoMovimentaQuant;
    property ScanPromocoesId: integer read FScanPromocoesId write FScanPromocoesId;
    property DescontoScanntech: double read FDescontoScanntech write FDescontoScanntech;
    property CstPisId: string read FCstPisId write FCstPisId;
    property CstCofinsId: string read FCstCofinsId write FCstCofinsId;
    property ProdutoPis: double read FProdutoPis write FProdutoPis;
    property ProdutoCofins: double read FProdutoCofins write FProdutoCofins;
    property CodigoNaturezaPiscofinsId: string read FCodigoNaturezaPiscofinsId write FCodigoNaturezaPiscofinsId;
    property ItenscupomBasePis: double read FItenscupomBasePis write FItenscupomBasePis;
    property ItenscupomValorPis: double read FItenscupomValorPis write FItenscupomValorPis;
    property ItenscupomBaseCofins: double read FItenscupomBaseCofins write FItenscupomBaseCofins;
    property ItenscupomValorCofins: double read FItenscupomValorCofins write FItenscupomValorCofins;
    property CstId: string read FCstId write FCstId;
    property CsosnId: string read FCsosnId write FCsosnId;
    property ItemValorIcms: double read FItemValorIcms write FItemValorIcms;
  end;
implementation
end.
