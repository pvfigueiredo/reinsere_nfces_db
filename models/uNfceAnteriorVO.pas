unit uNfceAnteriorVO;

interface
uses
  uNfceVO, SysUtils, Aurelius.Mapping.Attributes, Generics.Collections;

type
  TNfceAnteriorVO = class(TNfceVO)
    private
    [Exclude]
    FNfceIdAnterior: integer;
    public
      property NfceIdAnterior: integer read FNfceIdAnterior write FNfceIdAnterior;
  end;
implementation

end.
