unit uVendasCartoesVO; 

interface

uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;

type  
  [Entity]
  [Automapping]
  [Table('vendas_cartoes')]
  [Id('FVendasCartoesId', TIdGenerator.None)]
  TVendasCartoesVO = class
  private
    FVendaRede: string;
    FVendaDataHoraTransacao: TDateTime;
    FVendaValor: double;
    FVendasCartoesId: integer;
    FAreceberId: integer;
    FOperadorasCartaoId: integer;
    FVendaCodigoAutorizacao: integer;
    FVendaQteParcelas: integer;
    FVendaAdministradora: string;
    FVendaTef: boolean;
    FVendaDocVinculado: string;
    FVendaNsu: string;
    FVendaCopiaComprovante: string;
    FVendaNumCartao: string;
    FVendaCredito: boolean;
    FVendaNsuTransacaoCancelada: string;
    FVendaTransacaoCancelada: boolean;
    FVendaDesconto: double;
    FVendaSaque: double;
    FVendaInstituicao: string;
  public
    property VendaRede: string read FVendaRede write FVendaRede;
    property VendaDataHoraTransacao: TDateTime read FVendaDataHoraTransacao write FVendaDataHoraTransacao;
    property VendaValor: double read FVendaValor write FVendaValor;
    property VendasCartoesId: integer read FVendasCartoesId write FVendasCartoesId;
    property AreceberId: integer read FAreceberId write FAreceberId;
    property OperadorasCartaoId: integer read FOperadorasCartaoId write FOperadorasCartaoId;
    property VendaCodigoAutorizacao: integer read FVendaCodigoAutorizacao write FVendaCodigoAutorizacao;
    property VendaQteParcelas: integer read FVendaQteParcelas write FVendaQteParcelas;
    property VendaAdministradora: string read FVendaAdministradora write FVendaAdministradora;
    property VendaTef: boolean read FVendaTef write FVendaTef;
    property VendaDocVinculado: string read FVendaDocVinculado write FVendaDocVinculado;
    property VendaNsu: string read FVendaNsu write FVendaNsu;
    property VendaCopiaComprovante: string read FVendaCopiaComprovante write FVendaCopiaComprovante;
    property VendaNumCartao: string read FVendaNumCartao write FVendaNumCartao;
    property VendaCredito: boolean read FVendaCredito write FVendaCredito;
    property VendaNsuTransacaoCancelada: string read FVendaNsuTransacaoCancelada write FVendaNsuTransacaoCancelada;
    property VendaTransacaoCancelada: boolean read FVendaTransacaoCancelada write FVendaTransacaoCancelada;
    property VendaDesconto: double read FVendaDesconto write FVendaDesconto;
    property VendaSaque: double read FVendaSaque write FVendaSaque;
    property VendaInstituicao: string read FVendaInstituicao write FVendaInstituicao;
  end;
implementation
end.