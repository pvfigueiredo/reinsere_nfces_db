unit uClientesVO; 

interface
uses
  Aurelius.Mapping.Attributes, SysUtils, Generics.Collections;
type 
  [Entity]
  [Automapping]
  [Table('clientes')]
  [Id('FClientesId', TIdGenerator.None)]
  TClientesVO = class
  private
    FClienteComplemento: string;
    FClienteLimiteDisponivel: double;
    FClientesId: integer;
    FClienteNascimentoFundacao: TDate;
    FClienteNumero: integer;
    FClienteBairro: string;
    FClienteUf: string;
    FClienteCodIbgeCidade: integer;
    FClienteStatus: integer;
    FClienteNome: string;
    [Column('CLIENTE_NOME1')]
    FClienteNome1: string;
    FClienteCpfCnpj: string;
    FClienteRgIe: string;
    FClienteEmail: string;
    FClienteEndereco: string;
    FClienteCep: string;
    FClienteCidade: string;
    FClienteTelefone: string;
    [Column('CLIENTE_F_J')]
    FClienteFJ: string;
    FClienteValidaLimite: boolean;
    FClienteObs: string;
    FClienteDesativado: boolean;
    FClienteClasseNome: string;
    FClienteClasseCor: string;
  public
    property ClienteComplemento: string read FClienteComplemento write FClienteComplemento;
    property ClienteLimiteDisponivel: double read FClienteLimiteDisponivel write FClienteLimiteDisponivel;
    property ClientesId: integer read FClientesId write FClientesId;
    property ClienteNascimentoFundacao: TDate read FClienteNascimentoFundacao write FClienteNascimentoFundacao;
    property ClienteNumero: integer read FClienteNumero write FClienteNumero;
    property ClienteBairro: string read FClienteBairro write FClienteBairro;
    property ClienteUf: string read FClienteUf write FClienteUf;
    property ClienteCodIbgeCidade: integer read FClienteCodIbgeCidade write FClienteCodIbgeCidade;
    property ClienteStatus: integer read FClienteStatus write FClienteStatus;
    property ClienteNome: string read FClienteNome write FClienteNome;
    property ClienteNome1: string read FClienteNome1 write FClienteNome1;
    property ClienteCpfCnpj: string read FClienteCpfCnpj write FClienteCpfCnpj;
    property ClienteRgIe: string read FClienteRgIe write FClienteRgIe;
    property ClienteEmail: string read FClienteEmail write FClienteEmail;
    property ClienteEndereco: string read FClienteEndereco write FClienteEndereco;
    property ClienteCep: string read FClienteCep write FClienteCep;
    property ClienteCidade: string read FClienteCidade write FClienteCidade;
    property ClienteTelefone: string read FClienteTelefone write FClienteTelefone;
    property ClienteFJ: string read FClienteFJ write FClienteFJ;
    property ClienteValidaLimite: boolean read FClienteValidaLimite write FClienteValidaLimite;
    property ClienteObs: string read FClienteObs write FClienteObs;
    property ClienteDesativado: boolean read FClienteDesativado write FClienteDesativado;
    property ClienteClasseNome: string read FClienteClasseNome write FClienteClasseNome;
    property ClienteClasseCor: string read FClienteClasseCor write FClienteClasseCor;
  end;
implementation
end.