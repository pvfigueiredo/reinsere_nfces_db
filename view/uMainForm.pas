unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, acPathDialog, Vcl.StdCtrls, AeroButtons,
  JvExControls, JvButton, JvTransparentButton, Vcl.Buttons, PngSpeedButton,
  Vcl.Mask, AdvUtil, Vcl.Grids, AdvObj, BaseGrid, AdvGrid, Generics.Collections,
  Vcl.Graphics, Vcl.FileCtrl,

  uNfceVO, uMainCtr, uNfceCtr, uCupomVO, ULogs;

type
  TfrmMain = class(TForm)
    opdCaminhoDB: TOpenDialog;
    gbBancoDados: TGroupBox;
    lblCaminhoDB: TLabel;
    edtCaminhoDB: TEdit;
    spbtnPath: TPngSpeedButton;
    lblCnpjDB: TLabel;
    medtCnpj: TMaskEdit;
    advGridNfce: TAdvStringGrid;
    btnCarregaGrid: TButton;
    edtNfcesSemRegistro: TEdit;
    lblNfcesSemRegistro: TLabel;
    edtNfcesSemChave: TEdit;
    lblNfcesSemChave: TLabel;
    Button1: TButton;
    btnSalvaNfce: TButton;
    gbFiltro: TGroupBox;
    chbNfceFaltantes: TCheckBox;
    chbSemChave: TCheckBox;
    btnProtocolaXml: TButton;
    opdXml: TOpenDialog;
    gbProtocola: TGroupBox;
    procedure spbtnPathClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCarregaGridClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnSalvaNfceClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnGeraXmlClick(Sender: TObject);
    procedure btnSalvaModificacoesClick(Sender: TObject);
    procedure advGridNfceEditCellDone(Sender: TObject; ACol, ARow: Integer);
    procedure btnAplicaFiltroClick(Sender: TObject);
    procedure chbNfceFaltantesClick(Sender: TObject);
    procedure chbSemChaveClick(Sender: TObject);
    procedure btnProtocolaXmlClick(Sender: TObject);

  private
    { Private declarations }
    Nfces: TObjectList<TNfceVO>;
    FCupons: TObjectList<TCupomVO>;
    CaminhoDB: string;
    MainCtr: TMainCtr;
    NfcesSemRegistro: TObjectList<TNfceVO>;
    NfceAnterior: TList<TPair<integer, TNfceVO>>;
    NfcesSemChave: TObjectList<TNfceVO>;
    ChavesGeradas: boolean;
    FDirSalvaXml: string;
    function GetCaminhoDB: string;
    function ValidaCaminhoDB: boolean;
    function CalculaDadosNfce(Linha: integer): TNfceVO;
    function PreencheNfceVazia(NfceNumero: integer): TNfceVO;
    function CriaNfces: boolean;
    function SalvaChavesNfce: boolean;
    function GetCaminhoApp: string;
    function PreencheNfceSemRegistro(const I, J: integer): TNfceVO;

    procedure ConfiguraGrid;
    procedure CarregaGrid;
    procedure InsereLinhaGrid(Nfce: TNfceVO; Linha: integer);
    procedure InsereLinhaVazia(Linha: integer); overload;
    procedure InsereLinhaVazia(Linha, NfceNum: integer); overload;
    procedure CarregaNfcesSemCadastro(var Linha: integer);
    procedure CarregaNfcesSemChave(var Linha: integer);
    procedure RecarregaGrid;
    procedure GeraXml;
    procedure SalvaModifica�oes;
    procedure AtualizaNfcesSemRegistro(ACol, ARow, Pos: integer);
    procedure AtualizaNfcesSemChave(ACol, ARow, Pos: integer);
    procedure CarregaGridComFiltro;
    procedure GeraListaSemRegistro;
    procedure GeraListaSemChave;
    procedure RecarregaDB;
    procedure SalvaNfceSemRegistro;
    procedure SalvaNfceSemChave;
    procedure ProtocolaXmls(DirXmls: string);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.advGridNfceEditCellDone(Sender: TObject; ACol,
  ARow: Integer);
begin
  if ARow <> 0 then
  begin
    advGridNfce.Cells[11, ARow] := '1';
    case StrToInt(advGridNfce.Cells[9, ARow]) of
      -1 : AtualizaNfcesSemRegistro(ACol, ARow, StrToInt(advGridNfce.Cells[10, ARow]));
      0: AtualizaNfcesSemChave(ACol, ARow, StrToInt(advGridNfce.Cells[10, ARow]));
    end;

  end;
end;

procedure TfrmMain.AtualizaNfcesSemChave(ACol, ARow, Pos: integer);
begin
  with advGridNfce do
  begin
    NfcesSemChave[Pos].NfceNumero := StrToInt(Cells[2,ARow]);
    NfcesSemChave[Pos].NfceModelo := Cells[3,ARow];
    NfcesSemChave[Pos].NfceSerie :=  StrToInt(Cells[4,ARow]);
    NfcesSemChave[Pos].NfceChaveAcesso := Cells[5,ARow];
    NfcesSemChave[Pos].NfceCodigoRetorno := Cells[6,ARow];
    NfcesSemChave[Pos].NfceProtocolo := Cells[7,ARow];
    NfcesSemChave[Pos].NfceDataEmissao := StrToDateTime(Cells[8,ARow]);
  end;
end;

procedure TfrmMain.AtualizaNfcesSemRegistro(ACol, ARow, Pos: integer);
begin
  with advGridNfce do
  begin
    NfcesSemRegistro[Pos].NfceNumero := StrToInt(Cells[2,ARow]);
    NfcesSemRegistro[Pos].NfceModelo := Cells[3,ARow];
    NfcesSemRegistro[Pos].NfceSerie :=  StrToInt(Cells[4,ARow]);
    NfcesSemRegistro[Pos].NfceChaveAcesso := Cells[5,ARow];
    NfcesSemRegistro[Pos].NfceCodigoRetorno := Cells[6,ARow];
    NfcesSemRegistro[Pos].NfceProtocolo := Cells[7,ARow];
    NfcesSemRegistro[Pos].NfceDataEmissao := StrToDateTime(Cells[8,ARow]);
  end;
end;

procedure TfrmMain.btnAplicaFiltroClick(Sender: TObject);
begin
  CarregaGridComFiltro;
end;

procedure TfrmMain.btnCarregaGridClick(Sender: TObject);
begin
  if not CaminhoDB.EndsWith('DB_PAF_ECF.FDB') then
  begin
    ShowMessage('Favor selecionar um Banco de Dados do NFC-e.');
    abort;
  end;
  chbNfceFaltantes.Checked := False;
  chbSemChave.Checked := False;
  RecarregaDB;

  Nfces := MainCtr.GetListaNfce;
  GeraListaSemRegistro;
  GeraListaSemChave;
  CarregaGrid;
  medtCnpj.Text := MainCtr.GetCnpj;
  btnCarregaGrid.Caption := 'Recarregar';
end;

procedure TfrmMain.btnProtocolaXmlClick(Sender: TObject);
var
  DirXml: string;
begin
  if CaminhoDB = EmptyStr then
  begin
    ShowMessage('Favor selecionar um banco do NFC-e antes de protocolar os XMLs.');
    abort;
  end;
  SelectDirectory('Selecione um direct�rio', 'My Computer', DirXml);
  ProtocolaXmls(DirXml);
end;

procedure TfrmMain.btnGeraXmlClick(Sender: TObject);
begin
  GeraXml;
end;

procedure TfrmMain.btnSalvaModificacoesClick(Sender: TObject);
begin
  SalvaModifica�oes;
end;

procedure TfrmMain.btnSalvaNfceClick(Sender: TObject);
begin
  if SalvaChavesNfce then
  begin
    ShowMessage('Chaves salvas com sucesso!');
    RecarregaGrid;
  end;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  if advGridNfce.RowCount > 1 then
  begin
    ChavesGeradas := CriaNfces;
    if chbNfceFaltantes.Checked or chbSemChave.Checked then
      CarregaGridComFiltro
    else
      CarregaGrid;
  end;

end;

function TfrmMain.CalculaDadosNfce(Linha: integer): TNfceVO;
begin
  with advGridNfce do
  begin
    Result := TNfceVO.Create;
    Result.NfceNumero := StrToInt(Cells[2, Linha]);
    Result.NfceModelo := '65';
    Result.NfceSerie := StrToInt(Cells[4, Linha - 1]);
    Result.NfceChaveAcesso :=  Cells[5, Linha];
    Result.NfceCodigoRetorno := Cells[6, Linha];
    Result.NfceProtocolo := Cells[7, Linha];
    Result.NfceDataEmissao := StrToDate(Copy(Cells[8, Linha - 1], 0, 10));
  end;
end;

procedure TfrmMain.CarregaGrid;
var
  Nfce: TNfceVO;
  NfceSemChave,
  Linha: integer;
  I: integer;
begin
  try
    if (not Assigned(Nfces)) or (Nfces.Count = 0) then
      Nfces := MainCtr.GetListaNfce;

    advGridNfce.RowCount := MainCtr.GetUltimaNumeracaoNfce + 1;

    Linha := 1;
    for Nfce in Nfces  do
    begin
      InsereLinhaGrid(Nfce, Linha);
      Inc(Linha);
    end;

    for Nfce in NfcesSemRegistro do
    begin
      InsereLinhaGrid(Nfce, Linha);
      Inc(Linha);
    end;

    edtNfcesSemRegistro.Text := IntToStr(NfcesSemRegistro.Count);
    edtNfcesSemChave.Text := IntToStr(NfcesSemChave.Count);

  except
    on E: Exception do
    begin
      raise;
    end;

  end;
end;

procedure TfrmMain.CarregaGridComFiltro;
var
  Linha: integer;
begin
  advGridNfce.Clear;
  advGridNfce.RowCount := 0;
  Linha := 1;
  ConfiguraGrid;
  if (chbNfceFaltantes.Checked)then
  begin
    advGridNfce.RowCount := advGridNfce.RowCount + NfceAnterior.Count;
    CarregaNfcesSemCadastro(Linha);
  end;
  if (chbSemChave.Checked)then
  begin
    advGridNfce.RowCount := advGridNfce.RowCount + NfcesSemChave.Count;
    CarregaNfcesSemChave(Linha);
  end;
end;

procedure TfrmMain.CarregaNfcesSemCadastro(var Linha: integer);
var
  Nfce: TNfceVO;
  NfceCtr: TNfceCtr;
  I: integer;
begin
  try

    for I := 0 to Pred(NfcesSemRegistro.Count) do
    begin
      InsereLinhaGrid(NfceAnterior[I].Value, Linha);
      advGridNfce.Cells[9, Linha] := '-1';
      advGridNfce.Cells[10, Linha] := I.ToString;
      Inc(Linha);
    end;

  except
    on E: Exception do
    begin
      raise;
    end;

  end;

end;

procedure TfrmMain.CarregaNfcesSemChave(var Linha: integer);
var
  I: integer;
begin
  try

    for I:= 0 to NfcesSemChave.Count - 1 do
    begin
      InsereLinhaGrid(NfcesSemChave[I], Linha);
      advGridNfce.Cells[9, Linha] := '0';
      advGridNfce.Cells[10, Linha] := I.ToString;
      Inc(Linha);
    end;

  except
    on E: Exception do
    begin
      raise;
    end;

  end;

end;

procedure TfrmMain.chbNfceFaltantesClick(Sender: TObject);
begin
  if chbNfceFaltantes.Checked or chbSemChave.Checked then
    CarregaGridComFiltro
  else
    CarregaGrid;
end;

procedure TfrmMain.chbSemChaveClick(Sender: TObject);
begin
  if chbSemChave.Checked or chbNfceFaltantes.Checked then
    CarregaGridComFiltro
  else
    CarregaGrid;
end;

procedure TfrmMain.ConfiguraGrid;
begin
  with advGridNfce do
  begin
    ColCount := 12;

    ColWidths[0] := 10;
    ColWidths[1] := 50;
    ColWidths[2] := 50;
    ColWidths[3] := 50;
    ColWidths[4] := 50;
    ColWidths[5] := 350;
    ColWidths[6] := 50;
    ColWidths[7] := 100;
    ColWidths[8] := 140;
    ColWidths[9] := 0;
    ColWidths[10] := 0;
    ColWidths[11] := 0;

    Cells[0,0] := EmptyStr;
    Cells[1,0] := 'ID NFC-e';
    Cells[2,0] := 'N�';
    Cells[3,0] := 'Modelo';
    Cells[4,0] := 'S�rie';
    Cells[5,0] := 'Chave de acesso';
    Cells[6,0] := 'C�d. Retorno';
    Cells[7,0] := 'Protocolo';
    Cells[8,0] := 'Data de Emiss�o';
    Cells[9,0] := 'Tipo';//-1: n�o registrada no banco, 0: sem chave, 1: normal
    Cells[10,0] := 'posicao lista';//posi��o na sua respectiva lista
    Cells[11,0] := 'Modifado';
  end;

end;

function TfrmMain.CriaNfces: boolean;
var
  Linha: integer;
  Nfce: TNfceVO;
  Par: TPair<integer, TNfceVO>;
  I: integer;
begin
  try
    for Par in NfceAnterior do
    begin
      Par.Value.NfceChaveAcesso := MainCtr.
        CalculaChaveAcesso(Par.Value);
      Nfces.Add(Par.Value);
    end;

    for Nfce in Nfces do
    begin
      if Nfce.NfceChaveAcesso = EmptyStr then
      begin
        NfcesSemChave[NfcesSemChave.IndexOf(Nfce)].NfceChaveAcesso := MainCtr.
          CalculaChaveAcesso(Nfce);
        Nfce.NfceChaveAcesso := MainCtr.CalculaChaveAcesso(Nfce);
      end;
    end;

    ShowMessage('Chaves geradas com sucesso!');
    Result := True;
  except
    on E: Exception do
    begin
      Result := False;
      ShowMessage(E.Message);
    end;

  end;

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Constraints.MinWidth := 895;
  Constraints.MinHeight := 566;
  ConfiguraGrid;
  ChavesGeradas := False;
  NfcesSemRegistro := TObjectList<TNfceVO>.Create;
  NfcesSemChave :=  TObjectList<TNfceVO>.Create;
  NfceAnterior := TList<TPair<integer, TNfceVO>>.Create;
  FCupons := TObjectList<TCupomVO>.Create;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Nfces);
  FreeAndNil(MainCtr);
  FreeAndNil(NfcesSemRegistro);
  FreeAndNil(NfceAnterior);
end;

procedure TfrmMain.GeraListaSemChave;
var
  Nfce: TNfceVo;
begin
  for Nfce in Nfces do
    if (Nfce.NfceChaveAcesso = EmptyStr) or (Nfce.NfceCodigoRetorno = '217') then
      NfcesSemChave.Add(Nfce);
end;

procedure TfrmMain.GeraListaSemRegistro;
var
  I: integer;
  J: integer;
  n: integer;
  Numeracao: integer;
  NfceSemRegistro: TNfceVO;
begin
  Numeracao := MainCtr.GetUltimaNumeracaoNfce;
  if Numeracao = Nfces.Count then
    exit;
  J := 0;
  try
    for I := Nfces[0].NfceNumero to Numeracao - 1 do
    begin
      if (I + 1) > Nfces.Last.NfceNumero then
      begin
        NfcesSemRegistro.Add(PreencheNfceSemRegistro(I, J));
        n := Nfces[J].NfceNumero;
        continue;
      end;
      if (I + 1) < Nfces[J].NfceNumero then
      begin
        NfcesSemRegistro.Add(PreencheNfceSemRegistro(I, J));
        n := Nfces[J].NfceNumero;
        continue;
      end;
      if J < Nfces.Count - 1 then
        Inc(J);
    end;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;

end;

procedure TfrmMain.GeraXml;
var
  Cupom: TCupomVO;
  Count: integer;
  DirXml: string;
begin
  SelectDirectory('Selecione um direct�rio', 'My Computer', DirXml);
  Count := 0;
  for Cupom in FCupons do
  begin

    Inc(Count);
  end;
  if Count > 0 then
    ShowMessage('XMLs gerados com sucesso!')
  else
    ShowMessage('N�o foi gerado nenhum XML.');
end;

function TfrmMain.GetCaminhoApp: string;
begin
  Result := ExtractFileDir(Application.ExeName);
end;

function TfrmMain.GetCaminhoDB: string;
begin
  result := opdCaminhoDB.FileName;
end;

procedure TfrmMain.InsereLinhaGrid(Nfce: TNfceVO; Linha: integer);
begin
  with advGridNfce do
  begin
    Cells[1, Linha] := IntToStr(Nfce.NfceId);
    Cells[2, Linha] := IntToStr(Nfce.NfceNumero);
    Cells[3, Linha] := Nfce.NfceModelo;
    Cells[4, Linha] := IntToStr(Nfce.NfceSerie);
    Cells[5, Linha] := Nfce.NfceChaveAcesso;
    Cells[6, Linha] := Nfce.NfceCodigoRetorno;
    Cells[7, Linha] := Nfce.NfceProtocolo;
    Cells[8, Linha] := DateTimeToStr(Nfce.NfceDataEmissao);
    Cells[9, Linha] := '1';
  end;
end;

procedure TfrmMain.InsereLinhaVazia(Linha, NfceNum: integer);
var
  NfceVazia: TNfceVO;
begin
  NfceVazia := PreencheNfceVazia(NfceNum);
  InsereLinhaGrid(NfceVazia, Linha);
end;

procedure TfrmMain.InsereLinhaVazia(Linha: integer);
var
  NfceVazia: TNfceVO;
begin
  NfceVazia := PreencheNfceVazia(Linha);
  InsereLinhaGrid(NfceVazia, Linha);
end;

procedure TfrmMain.spbtnPathClick(Sender: TObject);
begin
  if opdCaminhoDB.Execute then
    CaminhoDB := GetCaminhoDB;
  if not ValidaCaminhoDB then
    Abort;
  edtCaminhoDB.Text := CaminhoDB;
  MainCtr := TMainCtr.Create(CaminhoDB);
end;

function TfrmMain.PreencheNfceSemRegistro(const I, J: integer): TNfceVO;
var
  Par: TPair<integer, TNfceVO>;
begin
  Result := TNfceVO.Create;
  Result.NfceNumero := I + 1;
  if J = 0 then
  begin
    Par.Key := Nfces[J].NfceId;
    Result.NfceDataEmissao := MainCtr.DateTimeToDate(Nfces[J].NfceDataEmissao);
    Result.NfceModelo := Nfces[J].NfceModelo;
    Result.NfceSerie := Nfces[J].NfceSerie;
    Par.Value := Result;
    NfceAnterior.Add(Par);
  end
  else
  begin
    Par.Key := Nfces[J - 1].NfceId;
    Result.NfceDataEmissao := MainCtr.DateTimeToDate(
      Nfces[J - 1].NfceDataEmissao);
    Result.NfceModelo := Nfces[J - 1].NfceModelo;
    Result.NfceSerie := Nfces[J - 1].NfceSerie;
    Par.Value := Result;
    NfceAnterior.Add(Par);
  end;

end;

function TfrmMain.PreencheNfceVazia(NfceNumero: integer): TNfceVO;
begin
  Result := TNfceVO.Create;

  Result.NfceId := 0;
  Result.NfceNumero := NfceNumero;
  Result.NfceModelo := '65';
  Result.NfceSerie := 0;
  Result.NfceChaveAcesso := '--';
  Result.NfceCodigoRetorno := '--';
  Result.NfceProtocolo := '--';
  Result.NfceDataEmissao := 0;

end;

procedure TfrmMain.ProtocolaXmls(DirXmls: string);
begin
  try
    if DirXmls <> EmptyStr then
    begin
      MainCtr.AssinaXml(DirXmls);
    end;
  except
    on E: Exception do
    begin
      TLog.SalvaLog('TfrmMain.btnProtocolaXmlClick' + E.Message, GetCaminhoApp,
        'Log.txt');
    end;
  end;
end;

procedure TfrmMain.RecarregaDB;
begin
  if not Assigned(Nfces) then
    exit;
  Nfces.Clear;
  NfcesSemRegistro.Clear;
  NfcesSemChave.Clear;
  NfceAnterior.Clear;
end;

procedure TfrmMain.RecarregaGrid;
begin
  Nfces.Clear;
  advGridNfce.Clear;
  ConfiguraGrid;
  if chbNfceFaltantes.Checked or chbSemChave.Checked then
    CarregaGridComFiltro
  else
    CarregaGrid;
end;

function TfrmMain.SalvaChavesNfce: boolean;
begin
  try
    FCupons.Clear;
    SelectDirectory('Selecione um direct�rio', 'My Computer', FDirSalvaXml);

    if chbNfceFaltantes.Checked then
      SalvaNfceSemRegistro;

    if chbSemChave.Checked then
      SalvaNfceSemChave;

    if (chbNfceFaltantes.Checked and chbSemChave.Checked) and
      not(chbNfceFaltantes.Checked and chbSemChave.Checked) then
    begin
      SalvaNfceSemRegistro;
      SalvaNfceSemChave;
    end;

    Result := True;
  except
    on E: Exception do
    begin
      TLog.SalvaLog('TfrmMain.SalvaChavesNfce' + E.Message, GetCaminhoApp,
        'Log.txt');
    end;

  end;
end;

procedure TfrmMain.SalvaModifica�oes;
var
  Linha: integer;
begin
  //Salva modificacoes feitas no Grid
  with advGridNfce do
  begin
    for Linha := 1 to RowCount - 1 do
    begin
      if StrToInt(Cells[11, Linha]) = 1 then
      begin

      end;
    end;
  end;
end;

procedure TfrmMain.SalvaNfceSemChave;
var
  Nfce: TNfceVO;
  Cupom: TCupomVO;
  DigValue: string;
begin
  try
    for Nfce in NfcesSemChave do
    begin
      Cupom := MainCtr.GetCupom('FNfce', Nfce.NfceId.ToString);
      Cupom.CupomValImportacao := 4;
      Cupom.CupomStatusImportacao := 2;
      MainCtr.GetRetornoNfce(Nfce, DigValue);
      Nfce.NfceRecibo := 'SNF';
      Cupom.CupomDataEmissao := Nfce.NfceDataEmissao;
      MainCtr.GeraXml(Cupom, DigValue, FDirSalvaXml);
      MainCtr.SalvaCupom(Cupom);
      MainCtr.AtualizaNfce(Nfce);
    end;
  except
    on E: Exception do
    begin
      raise Exception.CreateFmt('Erro ao salvar o NFC-e %s.',
        [Nfce.NfceId.ToString]);
    end;

  end;
end;

procedure TfrmMain.SalvaNfceSemRegistro;
var
  Nfce: TNfceVO;
  Cupom: TCupomVO;
  Par: TPair<integer, TNfceVO>;
  DigValue: string;
begin
  try
    for Par in NfceAnterior do
    begin
      Par.Value.NfceRecibo := 'SFN';
      MainCtr.SalvaNfce(Par.Value);
      MainCtr.GetRetornoNfce(Par.Value, DigValue);
      Cupom := MainCtr.GetCupom('FNfce', Par.Value.NfceId.ToString);

      if not Assigned(Cupom) then
        Cupom := MainCtr.GetCupom(Par.Value, Par.Key);

      if not Assigned(Cupom)then
        continue;

      if Assigned(Cupom.Nfce) then
        continue;

      Cupom.Nfce := Par.Value;

      if not Assigned(Cupom.FormasPagamento) then
        Cupom.FormasPagamento := MainCtr.FormaPagamentoFactory;

      if Cupom.FormasPagamento.FormasPagamentoId = 0 then
        Cupom.FormasPagamento.FormasPagamentoId := 1;

      Cupom.CupomDataEmissao :=  Par.Value.NfceDataEmissao;
      MainCtr.GeraXml(Cupom, DigValue, FDirSalvaXml);
      FCupons.Add(Cupom);

      MainCtr.AtualizaNfce(Par.Value);
      Cupom.CupomValImportacao := 4;
      Cupom.CupomStatusImportacao := 2;
      MainCtr.SalvaCupom(Cupom);
    end;
  except
    on E: Exception do
    begin
      raise Exception.CreateFmt('Erro ao salvar o NFC-e %s no cupom %s',
        [Cupom.Nfce.NfceId.ToString, Cupom.CuponsId.ToString]);
    end;
  end;
end;

function TfrmMain.ValidaCaminhoDB: boolean;
begin
   result := CaminhoDB.EndsWith('.FDB');
end;

end.
