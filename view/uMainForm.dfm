object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Reinsere NFC-e'
  ClientHeight = 566
  ClientWidth = 895
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    895
    566)
  PixelsPerInch = 96
  TextHeight = 13
  object gbBancoDados: TGroupBox
    Left = 5
    Top = 0
    Width = 882
    Height = 162
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Dados Banco de Dados'
    TabOrder = 0
    DesignSize = (
      882
      162)
    object lblCaminhoDB: TLabel
      Left = 7
      Top = 16
      Width = 121
      Height = 13
      Caption = 'Caminho Banco de Dados'
    end
    object spbtnPath: TPngSpeedButton
      Left = 482
      Top = 35
      Width = 26
      Height = 23
      OnClick = spbtnPathClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
        F80000000473424954080808087C0864880000000970485973000000A6000000
        A601DD7DFF380000001974455874536F667477617265007777772E696E6B7363
        6170652E6F72679BEE3C1A000002574944415478DAED94CB6B13511487CF99C9
        344DD24E423BA92D318A628AA2F84090AA0B89A855B01B41AD5D145DB8D580F9
        03ECC64577222E44145735F8C08DD88548046B40441B0B5293505B6D346D13CC
        C399269364E67847E9C285C419D25D0FDCC5BDFCF8BE39672E17E91D5C04806B
        6C49F0EF9A038430ECA1C760B29009B20DE02B55078220ECA509B302329137B2
        4FD89A6F984378034578645660723E30BC2228B29DDC5C3A7533F26DA4A97551
        68DD71900984A6F2757501CAF1BB4889A00CC8BB566746D5EB48C923ABF70F00
        6FAD09D6048D4BD14498AB6C85F9EA36C563CBD8FD2D299BCF3EDB1CC15BB95F
        8D96866A01B78DF33AC1B95406399527AD4B98C5339DA3A28B97AD0B0CF8A43A
        5C3FBE815C020750D3017215841E27C1741ED5F8424E0DADBF12B12430C67273
        F1863C18E0DB0C78A280E5F759AEEA75102D95911BD8A889B1452CB4D563714B
        828FCA3E48C2E5E5BE6E721AFBB1245F3C1BD0DC3CB2775C46FA5CC49F077A74
        3192A87F35043596B199118CE7CF2BA2E7A86BB348A0B3CF8BA4F8D2B95E4D64
        7C601DC0640E0BFD7EDD73671A7386E0013B3F6D4630511AA8551C83FCF60EE2
        8CF61FCEF0550149EDF580239E83F2313FB54BAD04F73EF1AC03BACA41F27590
        BDAFDEFF15BC2A9E0C7CA80D854E6DA28EE7690EDC2D005BDC04DF64AC053CBA
        E060F3F8AE208C7FC1674C60ED968EBCC8C60EF9A84FD5007776FECD306ED458
        8ACFFE58D6F75B16845F66A4764D88ED9674DF2E899C76FECF795A418AA631AB
        68181A392CDDB72CF8DDC5D38C937308611DE9820DC1A503AA0C385552E9D2E8
        89AE1923F30BC09726FE8756F20F0000000049454E44AE426082}
    end
    object lblCnpjDB: TLabel
      Left = 167
      Top = 62
      Width = 84
      Height = 13
      Caption = 'CNPJ Cadastrado'
    end
    object lblNfcesSemRegistro: TLabel
      Left = 7
      Top = 62
      Width = 132
      Height = 13
      Caption = 'Qtde de NFC-es s/ Registro'
    end
    object lblNfcesSemChave: TLabel
      Left = 7
      Top = 108
      Width = 139
      Height = 13
      Caption = 'Qtde NFC-e s/ Chave ou 217'
    end
    object edtCaminhoDB: TEdit
      Left = 7
      Top = 35
      Width = 475
      Height = 21
      TabOrder = 0
      TextHint = 'Selecione o banco do NFC-e'
    end
    object medtCnpj: TMaskEdit
      Left = 167
      Top = 81
      Width = 114
      Height = 21
      EditMask = '##.###.###/####-##;0;_'
      MaxLength = 18
      TabOrder = 1
      Text = ''
    end
    object btnCarregaGrid: TButton
      Left = 506
      Top = 35
      Width = 79
      Height = 23
      Caption = 'Carregar'
      TabOrder = 2
      OnClick = btnCarregaGridClick
    end
    object edtNfcesSemRegistro: TEdit
      Left = 7
      Top = 81
      Width = 130
      Height = 21
      Hint = 'N'#186' de NFC-es sem Reg.'
      TabOrder = 3
    end
    object edtNfcesSemChave: TEdit
      Left = 7
      Top = 127
      Width = 130
      Height = 21
      TabOrder = 4
    end
    object Button1: TButton
      Left = 167
      Top = 124
      Width = 105
      Height = 25
      Caption = 'Gerar Chave'
      TabOrder = 5
      OnClick = Button1Click
    end
    object btnSalvaNfce: TButton
      Left = 278
      Top = 124
      Width = 105
      Height = 25
      Caption = 'Atualizar Registros'
      TabOrder = 6
      OnClick = btnSalvaNfceClick
    end
    object gbFiltro: TGroupBox
      Left = 591
      Top = 16
      Width = 266
      Height = 73
      Anchors = [akTop, akRight]
      Caption = 'Filtros'
      TabOrder = 7
      object chbNfceFaltantes: TCheckBox
        Left = 7
        Top = 19
        Width = 234
        Height = 17
        Caption = 'NFC-es sem registro no Banco de Dados'
        TabOrder = 0
        OnClick = chbNfceFaltantesClick
      end
      object chbSemChave: TCheckBox
        Left = 7
        Top = 34
        Width = 250
        Height = 28
        Caption = 'NFC-es com retorno igual a 217 ou sem chave'
        TabOrder = 1
        OnClick = chbSemChaveClick
      end
    end
    object gbProtocola: TGroupBox
      Left = 591
      Top = 95
      Width = 122
      Height = 62
      Caption = 'Protocolar XMLs'
      TabOrder = 8
      object btnProtocolaXml: TButton
        Left = 19
        Top = 28
        Width = 89
        Height = 25
        Caption = 'Protocolar Xmls'
        TabOrder = 0
        OnClick = btnProtocolaXmlClick
      end
    end
  end
  object advGridNfce: TAdvStringGrid
    Left = 5
    Top = 168
    Width = 886
    Height = 353
    Cursor = crDefault
    Anchors = [akLeft, akTop, akRight, akBottom]
    DrawingStyle = gdsClassic
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ScrollBars = ssBoth
    TabOrder = 1
    HoverRowCells = [hcNormal, hcSelected]
    OnEditCellDone = advGridNfceEditCellDone
    ActiveCellFont.Charset = DEFAULT_CHARSET
    ActiveCellFont.Color = clWindowText
    ActiveCellFont.Height = -11
    ActiveCellFont.Name = 'Tahoma'
    ActiveCellFont.Style = [fsBold]
    ControlLook.FixedGradientHoverFrom = clGray
    ControlLook.FixedGradientHoverTo = clWhite
    ControlLook.FixedGradientDownFrom = clGray
    ControlLook.FixedGradientDownTo = clSilver
    ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownHeader.Font.Color = clWindowText
    ControlLook.DropDownHeader.Font.Height = -11
    ControlLook.DropDownHeader.Font.Name = 'Tahoma'
    ControlLook.DropDownHeader.Font.Style = []
    ControlLook.DropDownHeader.Visible = True
    ControlLook.DropDownHeader.Buttons = <>
    ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownFooter.Font.Color = clWindowText
    ControlLook.DropDownFooter.Font.Height = -11
    ControlLook.DropDownFooter.Font.Name = 'Tahoma'
    ControlLook.DropDownFooter.Font.Style = []
    ControlLook.DropDownFooter.Visible = True
    ControlLook.DropDownFooter.Buttons = <>
    Filter = <>
    FilterDropDown.Font.Charset = DEFAULT_CHARSET
    FilterDropDown.Font.Color = clWindowText
    FilterDropDown.Font.Height = -11
    FilterDropDown.Font.Name = 'Tahoma'
    FilterDropDown.Font.Style = []
    FilterDropDown.TextChecked = 'Checked'
    FilterDropDown.TextUnChecked = 'Unchecked'
    FilterDropDownClear = '(All)'
    FilterEdit.TypeNames.Strings = (
      'Starts with'
      'Ends with'
      'Contains'
      'Not contains'
      'Equal'
      'Not equal'
      'Larger than'
      'Smaller than'
      'Clear')
    FixedRowHeight = 22
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = [fsBold]
    FloatFormat = '%.2f'
    HoverButtons.Buttons = <>
    HoverButtons.Position = hbLeftFromColumnLeft
    HTMLSettings.ImageFolder = 'images'
    HTMLSettings.ImageBaseName = 'img'
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'Tahoma'
    PrintSettings.Font.Style = []
    PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
    PrintSettings.FixedFont.Color = clWindowText
    PrintSettings.FixedFont.Height = -11
    PrintSettings.FixedFont.Name = 'Tahoma'
    PrintSettings.FixedFont.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'Tahoma'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'Tahoma'
    PrintSettings.FooterFont.Style = []
    PrintSettings.PageNumSep = '/'
    SearchFooter.FindNextCaption = 'Find &next'
    SearchFooter.FindPrevCaption = 'Find &previous'
    SearchFooter.Font.Charset = DEFAULT_CHARSET
    SearchFooter.Font.Color = clWindowText
    SearchFooter.Font.Height = -11
    SearchFooter.Font.Name = 'Tahoma'
    SearchFooter.Font.Style = []
    SearchFooter.HighLightCaption = 'Highlight'
    SearchFooter.HintClose = 'Close'
    SearchFooter.HintFindNext = 'Find next occurrence'
    SearchFooter.HintFindPrev = 'Find previous occurrence'
    SearchFooter.HintHighlight = 'Highlight occurrences'
    SearchFooter.MatchCaseCaption = 'Match case'
    ShowDesignHelper = False
    SortSettings.DefaultFormat = ssAutomatic
    Version = '8.1.3.0'
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      22)
  end
  object opdCaminhoDB: TOpenDialog
    Left = 800
    Top = 144
  end
  object opdXml: TOpenDialog
    Left = 581
    Top = 224
  end
end
