unit UInutilizacoes_DAO;

interface

uses UPadrao_DAO, UConexoes, FireDAC.Comp.Client, System.SysUtils, UInutilizacoes_VO,
     Generics.Collections, USerieNfceCTR, USerieNfceVO;

type
  TInutilizacoes_DAO = class (TPadrao_DAO)

  private
    function getSerie: Integer;

  public

    function verificaIntervalos(Numero, Serie: integer):Integer;
    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Fun��o que retorna as inutiliza��es que n�o foram exportadas.
    ///	</summary>
    ///	<param name="ecf">
    ///	  informa��es do caixa.
    ///	</param>
    ///	<returns>
    ///	  TList<TInutilizacoes_VO>.
    ///	</returns>
    {$ENDREGION}
    function getInutilizacoes: TList<TInutilizacoes_VO>;
    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Procedimento que atualiza o campo "INUTILIZACAO_EXPORTADA" da tabela "INUTILIZACOES".
    ///	</summary>
    ///	<param name="ecf">
    ///	  informa��es do caixa.
    ///	</param>
    ///	<param name="inutilizacoes">
    ///	  Lista de inutiliza��es.
    ///	</param>
    {$ENDREGION}
    procedure AtualizaValExporInut(inutilizacoes: TList<TInutilizacoes_VO>);
  end;

implementation

{ TInutilizacoes_DAO }

function TInutilizacoes_DAO.verificaIntervalos(Numero, Serie: integer): Integer;
var
  qryTemp : TFDQuery;
begin
  try
    try
      qryTemp := TFDQuery.Create(nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT                                           ');
        SQL.Add('    INUTILIZACAO_NUMERO_INICIAL                 ,');
        SQL.Add('    INUTILIZACAO_NUMERO_FINAL                    ');
        SQL.Add('FROM                                             ');
        SQL.Add('    INUTILIZACOES                                ');
        SQL.Add('WHERE                                            ');
        SQL.Add('    INUTILIZACAO_NUMERO_INICIAL  <= :pNumero AND ');
        SQL.Add('    INUTILIZACAO_NUMERO_FINAL    >= :pNUMERO     ');

        ParamByName('pNumero').AsInteger := Numero;

        Open;

        if IsEmpty then
          Result := Numero
        else
          Result := verificaIntervalos(Numero+1, Serie);

      end;

    except on
      E: Exception do
        result := -1;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);

    FreeAndNil(qryTemp);
  end;
end;

procedure TInutilizacoes_DAO.AtualizaValExporInut(inutilizacoes: TList<TInutilizacoes_VO>);
Var
  qryTemp : TFDQuery;
  I : Integer;
begin
  try
    try
      qryTemp := TFDQuery.Create(nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      for I := 0 to inutilizacoes.Count -1 do
      begin
        with qryTemp do
        begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE INUTILIZACOES I                            ');
          SQL.Add('   SET I.INUTILIZACAO_EXPORTADA = -1              ');
          SQL.Add(' WHERE I.INUTILIZACOES_ID = :pINUTILIZACOES_ID    ');

          ParamByName('pINUTILIZACOES_ID').AsInteger := inutilizacoes.Items[i].inutilizacoes_id;

          ExecSQL;
        end;
      end;
    except
      on e : Exception do
      begin
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);
    FreeAndNil(qryTemp);
  end;

end;

function TInutilizacoes_DAO.getInutilizacoes: TList<TInutilizacoes_VO>;
Var
  qryTemp : TFDQuery;
  Inutilizacoes : TInutilizacoes_VO;
  SerieNfceCTR : TSerieNfceCTR;
  serienfce : TSerieNfceVO;
begin
  try
    try
      qryTemp := TFDQuery.Create(nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      Result := TList<TInutilizacoes_VO>.Create;

      SerieNfceCTR := nil;

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add(' SELECT I.INUTILIZACOES_ID, I.INUTILIZACAO_NUMERO_INICIAL,      ');
        SQL.Add('        I.INUTILIZACAO_NUMERO_FINAL, I.INUTILIZACAO_DATA,       ');
        SQL.Add('        I.INUTILIZACAO_JUSTIFICATIVA, I.INUTILIZACAO_EXPORTADA  ');
        SQL.Add(' FROM INUTILIZACOES I                                           ');
        SQL.Add(' WHERE I.INUTILIZACAO_EXPORTADA = 0                             ');

        Open;

        if not IsEmpty then
        begin

          SerieNfceCTR := TSerieNfceCTR.Create(Banco);

          serienfce  := SerieNfceCTR.getSerieNfce;

          first;
          while not Eof do
          begin
            Inutilizacoes                             := TInutilizacoes_VO.Create;
            Inutilizacoes.inutilizacoes_id            := FieldByName('INUTILIZACOES_ID').AsInteger;
            Inutilizacoes.inutilizacao_numero_inicial := FieldByName('INUTILIZACAO_NUMERO_INICIAL').AsInteger;
            Inutilizacoes.inutilizacao_numero_final   := FieldByName('INUTILIZACAO_NUMERO_FINAL').AsInteger;
            Inutilizacoes.inutilizacao_data           := FieldByName('INUTILIZACAO_DATA').AsDateTime;
            Inutilizacoes.inutilizacao_justificativa  := FieldByName('INUTILIZACAO_JUSTIFICATIVA').AsString;
            Inutilizacoes.inutilizacao_exportada      := FieldByName('INUTILIZACAO_EXPORTADA').AsInteger = -1;
            Inutilizacoes.inutilizacao_serie          := serienfce.SeriesNfceId;
            Inutilizacoes.inutilizacao_caixa          := serienfce.CaixaNfceNumero;

            Result.Add(Inutilizacoes);

            next;
          end;
        end
        else
        begin
          Result := nil;
        end;
      end;
    except
      on E: Exception do
      begin
        Result := Nil;
        raise Exception.Create('Erro na fun��o TInutilizacoes_DAO.getInutilizacoes.' + sLineBreak +
                               ' ERRO: ' + #13 + e.Message);
      end;

    end;
  finally
    TConexoes.FechaConexao(qryTemp);

    FreeAndNil(qryTemp);

    if Assigned(SerieNfceCTR) then
      FreeAndNil(SerieNfceCTR);
  end;

end;

function TInutilizacoes_DAO.getSerie: Integer;
Var
  qryTemp : TFDQuery;
  Inutilizacoes : TInutilizacoes_VO;
  SerieCaixa : Integer;
begin
  try
    try
      qryTemp := TFDQuery.Create(nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add(' SELECT FIRST 1 S.SERIES_NFCE_ID FROM SERIES_NFCE S   ');
        SQL.Add('        ORDER BY S.SERIES_NFCE_ID DESC;               ');

        Open;

        Result := FieldByName('SERIES_NFCE_ID').AsInteger;

      end;
    except
      on E: Exception do
      begin
        Result := 0;
        raise Exception.Create('Erro na fun��o TInutilizacoes_VO.getSerie.' + sLineBreak +
                               ' ERRO: ' + #13 + e.Message);
      end;

    end;
  finally
    TConexoes.FechaConexao(qryTemp);
    FreeAndNil(qryTemp);
  end;
end;

end.
