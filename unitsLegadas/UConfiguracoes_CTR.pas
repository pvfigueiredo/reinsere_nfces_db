unit UConfiguracoes_CTR;

interface

uses
  Generics.Collections, System.SysUtils, UConfiguracoes_DAO, UConfiguracoes_VO, UPadrao_CTR,
  UBanco;

type
  TConfiguracoes_CTR = Class(TPadrao_CTR)
  public
    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Retorna um lista com todas as configura��es cadastradas no banco de dados
    ///	</summary>
    ///	<returns>
    ///	  TList<TConfiguracoes> ou nil
    ///	</returns>
    ///	<remarks>
    ///	  Observa��es sobre a fun��o
    ///	</remarks>
    {$ENDREGION}
    function ListarConfiguracoes : TList<TConfiguracoes_VO>;

    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Retorna o valor da configura�ao de acordo com o ID informado
    ///	</summary>
    ///	<param name="ConfiguracaoID">
    ///	  Id da configuracao a ser retornada
    ///	</param>
    ///	<returns>
    ///	  Variante OU nil
    ///	</returns>

    {$ENDREGION}
    function GetConfiguracaoValor(ConfiguracaoID : Integer) : Variant;

    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Retorna determionada configura�ao de acordo com o ID informado
    ///	</summary>
    ///	<param name="ConfiguracaoID">
    ///	  Id da configuracao a ser retornada
    ///	</param>
    ///	<returns>
    ///	  TConfiguracaoVO OU nil
    ///	</returns>

    {$ENDREGION}
    function GetConfiguracao(ConfiguracaoID : Integer) : TConfiguracoes_VO;

    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Descri��o da fun��o (Cabe�alho)
    ///	</summary>
    ///	<param name="Lista : TList<TConfiguracoes_VO>">
    ///	  Lista de objetos do tipo TConfiguracoesVO
    ///	</param>
    ///	<returns>
    ///	  True ou False
    ///	</returns>
    {$ENDREGION}
    function UpdateCOnfiguracoes(Lista :  TList<TConfiguracoes_VO>) : Boolean;

    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Descri��o da fun��o (Cabe�alho)
    ///	</summary>
    ///	<param name="Lista : TList<TConfiguracoes_VO>">
    ///	  Lista de objetos do tipo TConfiguracoesVO
    ///	</param>
    ///	<returns>
    ///	  True ou False
    ///	</returns>
    {$ENDREGION}
    procedure VerificarConfiguracoes;

    procedure Update(Configuracao : TConfiguracoes_VO);

    procedure UpdateValor(configuracoes_id: Integer; valor: Variant; tipo : Integer);

    constructor Create (pBANCO : TBanco = Nil); override;
    Destructor  Destroy; override;

  private
    ConfiguracoesDAO : TConfiguracoes_DAO;

  end;

implementation

{ TConfiguracoes_CTR }

uses UConfiguracoesSistema, UObjFunctions;


//uses UInstancias;
constructor TConfiguracoes_CTR.Create (pBANCO : TBanco = Nil);
begin
  inherited;
  if not Assigned(ConfiguracoesDAO) then
  begin
    ConfiguracoesDAO := TConfiguracoes_DAO.Create(Self.BANCO);
  end;
end;

destructor TConfiguracoes_CTR.Destroy;
begin
  if(Assigned(ConfiguracoesDAO))then
  begin
    FreeAndNil(ConfiguracoesDAO);
  end;

  inherited;
end;

function TConfiguracoes_CTR.GetConfiguracao(
  ConfiguracaoID: Integer): TConfiguracoes_VO;
begin
  Result := ConfiguracoesDAO.GetConfiguracao(ConfiguracaoID);
end;

function TConfiguracoes_CTR.GetConfiguracaoValor(
  ConfiguracaoID: Integer): Variant;
begin
  Result := ConfiguracoesDAO.GetConfiguracao(ConfiguracaoID).config_valor;
end;

function TConfiguracoes_CTR.ListarConfiguracoes: TList<TConfiguracoes_VO>;
begin
  Result := ConfiguracoesDAO.GetConfiguracoes;
end;

procedure TConfiguracoes_CTR.Update(Configuracao: TConfiguracoes_VO);
begin
  ConfiguracoesDAO.Update(Configuracao);
end;

function TConfiguracoes_CTR.UpdateCOnfiguracoes(
  Lista: TList<TConfiguracoes_VO>): Boolean;
begin
  Result := ConfiguracoesDAO.SalvaConfiguracao(Lista);
end;

procedure TConfiguracoes_CTR.UpdateValor(configuracoes_id: Integer;
  valor: Variant; tipo: Integer);
begin
  ConfiguracoesDAO.UpdateValor(configuracoes_id, valor, tipo);
end;

procedure TConfiguracoes_CTR.VerificarConfiguracoes;
var
  ListaConfiguracoesSistema ,
  ListaConfiguracoesBanco   : TList<TConfiguracoes_VO>;
  Configuracao              : TConfiguracoes_VO;
begin
  ListaConfiguracoesSistema := ListarConfiguracoesSistema;
  ListaConfiguracoesBanco   := ListarConfiguracoes;

  for Configuracao in ListaConfiguracoesSistema do
  begin
    if not Assigned(TObjFunctions<TConfiguracoes_VO>.QuickBusca(Configuracao.configuracoes_id,ListaConfiguracoesBanco,'configuracoes_id')) then
    begin
       ConfiguracoesDAO.Insert(Configuracao);
    end;
  end;
end;

end.
