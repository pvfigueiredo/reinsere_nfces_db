unit UInutilizacoes_CTR;

interface

uses
  UPadrao_CTR, UInutilizacoes_DAO, UBanco, System.SysUtils, UInutilizacoes_VO,
  Generics.Collections;

type
  TInutilizacoes_CTR = class(TPadrao_CTR)
  private
    Inutilizacoes_DAO: TInutilizacoes_DAO;
  public
    constructor Create(pBANCO: TBanco = Nil); override;
    destructor Destroy; override;
    function getNumero(Numero, Serie: integer): Integer;
    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Fun��o que retorna as inutiliza��es que n�o foram exportadas.
    ///	</summary>
    ///	<param name="ecf">
    ///	  informa��es do caixa.
    ///	</param>
    ///	<returns>
    ///	  TList<TInutilizacoes_VO>.
    ///	</returns>
    {$ENDREGION}
    function getInutilizacoes: TList<TInutilizacoes_VO>;
    {$REGION 'Documentation'}
    ///	<summary>
    ///	  Procedimento que atualiza o campo "INUTILIZACAO_EXPORTADA" da tabela "INUTILIZACOES".
    ///	</summary>
    ///	<param name="ecf">
    ///	  informa��es do caixa.
    ///	</param>
    ///	<param name="inutilizacoes">
    ///	  Lista de inutiliza��es.
    ///	</param>
    {$ENDREGION}
    procedure AtualizaValExporInut(inutilizacoes: TList<TInutilizacoes_VO>);

  end;

implementation

{ Inutilizacoes_CTR }

constructor TInutilizacoes_CTR.Create(pBANCO: TBanco);
begin
  inherited;
  Inutilizacoes_DAO := TInutilizacoes_DAO.Create(Self.BANCO);
end;

destructor TInutilizacoes_CTR.Destroy;
begin
  if Assigned(Inutilizacoes_DAO) then
    FreeAndNil(Inutilizacoes_DAO);

  inherited;
end;

function TInutilizacoes_CTR.getInutilizacoes: TList<TInutilizacoes_VO>;
begin
  Result := inutilizacoes_dao.getInutilizacoes;
end;

function TInutilizacoes_CTR.getNumero(Numero, Serie: integer): Integer;
begin
  Result := Self.Inutilizacoes_DAO.verificaIntervalos(Numero, Serie);
end;

procedure TInutilizacoes_CTR.AtualizaValExporInut(inutilizacoes: TList<TInutilizacoes_VO>);
begin
  inutilizacoes_dao.AtualizaValExporInut(inutilizacoes);
end;

end.

