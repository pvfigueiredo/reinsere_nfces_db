unit UConfiguracoesSistema;

interface

uses
  Generics.Collections, System.SysUtils, System.Classes,UConfiguracoes_VO;


function ListarConfiguracoesSistema : TList<TConfiguracoes_VO>;

implementation

{ TModalidades_Scripts }


function ListarConfiguracoesSistema : TList<TConfiguracoes_VO>;
var
  Configuracao : TConfiguracoes_VO;
  ObsSenha     : string;
begin
  ObsSenha := '''0 - N�o Exige Senha ''||ascii_char(13)||ascii_char(10)||'' 1 - Somente Gerentes ''||ascii_char(13)||ascii_char(10)||'' 2 - Todos os Usu�rios do Sistema''';

  Result := TList<TConfiguracoes_VO>.Create;

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 1;
  Configuracao.config_nome      := 'CONSUMIDOR FINAL';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '50';
  Configuracao.config_obs       := '''Informa o ID do consumidor Final.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 2;
  Configuracao.config_nome      := 'VALOR M�NIMO DA PARCELA';
  Configuracao.config_tipo      := 4;
  Configuracao.config_valor     := '0,01';
  Configuracao.config_obs       := '''Informa o Valor m�nimo que pode ser parcelado na modalidade de Credi�rio.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 3;
  Configuracao.config_nome      := 'QUANTIDADE M�XIMA DE PARCELAS';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '12';
  Configuracao.config_obs       := '''Informa a quantidade m�xima que se pode parcelar um cupom.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 4;
  Configuracao.config_nome      := 'QUANTIDADE M�XIMA DE PARCELAS';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Permite estoque negativo nos produtos.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 5;
  Configuracao.config_nome      := 'TAMANHO DO C�DIGO DO PRODUTO NA BALAN�A';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''Tamanho do c�digo do produto que vem no c�digo de barras da balan�a.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 6;
  Configuracao.config_nome      := 'VALOR TOTAL NO C�DIGO DE BARRAS DA BALAN�A';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, indica que o VALOR TOTAL ser� informado no c�digo de barras da balan�a; Se FALSE, indica o PESO TOTAL ser� informado no c�digo de barras da balan�a.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.configuracoes_id := 7;
  Configuracao.config_nome      := 'DESCONTO M�XIMO PERMITIDO NO ITEM (%)';
  Configuracao.config_tipo      := 4;
  Configuracao.config_valor     := '10,00';
  Configuracao.config_obs       := '''Informa a porcentagem m�xima de desconto permitida no Item.''';
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'DESCONTO M�XIMO PERMITIDO NA VENDA';
  Configuracao.config_tipo      := 4;
  Configuracao.config_valor     := '10,00';
  Configuracao.config_obs       := '''Informa a porcentagem m�xima de desconto permitida na Venda''';
  Configuracao.configuracoes_id := 8;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR FUNCION�RIO DO MOVIMENTO NA VENDA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, o funcion�rio que emitiu o cupom ser� o mesmo que abriu o movimento; Se FALSE, a cada novo cupom ser� exigido informar o funcion�rio.''';
  Configuracao.configuracoes_id := 9;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ABRIR TELA DE VENDAS AO INICIAR?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ao iniciar o sistema, a tela de vendas ser� aberta automaticamente.''';
  Configuracao.configuracoes_id := 10;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA ABRIR A GAVETA?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 11;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA ACESSAR TELAS?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 12;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TIPO DE TEF CONFIGURADO';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := '''Informa o tipo de TEF configurado:''||ascii_char(13)||ascii_char(10)||'+
                                   '''0 - gpNenhum''||ascii_char(13)||ascii_char(10)||'+
                                   '''1 - gpTefDial''||ascii_char(13)||ascii_char(10)||'+
                                   '''2 - gpTefDisc''||ascii_char(13)||ascii_char(10)||'+
                                   '''3 - gpHiperTef''||ascii_char(13)||ascii_char(10)||'+
                                   '''4 - gpCliSiTef''||ascii_char(13)||ascii_char(10)||'+
                                   '''5 - gpTefGpu''||ascii_char(13)||ascii_char(10)||'+
                                   '''6 - gpVeSPague''||ascii_char(13)||ascii_char(10)||'+
                                   '''7 - gpBanese''||ascii_char(13)||ascii_char(10)||'+
                                   '''8 - gpTefAuttar''||ascii_char(13)||ascii_char(10)||'+
                                   '''9 - gpGoodCard''||ascii_char(13)||ascii_char(10)||'+
                                   '''10 - gpFoxWin''||ascii_char(13)||ascii_char(10)||'+
                                   '''11 - gpCliDTEF''||ascii_char(13)||ascii_char(10)||'+
                                   '''12 - gpPetrocard''||ascii_char(13)||ascii_char(10)||'+
                                   '''13 - gpCrediShop''||ascii_char(13)||ascii_char(10)||'+
                                   '''14 - gpTicketCar''||ascii_char(13)||ascii_char(10)||'+
                                   '''15 - gpConvCard''';
  Configuracao.configuracoes_id := 13;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA ACESSAR RELAT�RIOS?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 14;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA DESATIVAR TEF?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 15;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR A MODALIDADE DE CHEQUE COMO CREDI�RIO.';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, torna os areceberes de cada cheque como se fossem Credi�rios. Se FALSE, trata o cheque como se fosse o pagamento a cart�o.''';
  Configuracao.configuracoes_id := 16;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'HABILITAR TELAS DE CADASTROS?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, as telas de cadastros estar�o habilitadas para o usu�rio, se FALSE, o usu�rio n�o ter� acesso �s telas de cadastros e os mesmos ser�o realizados pelo integrador.''';
  Configuracao.configuracoes_id := 17;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'REALIZA CADASTROS NO PDV?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se FALSE, os cadastros de CFOP, TURNOS, NCM, UNIDADES E OPERADORAS DE CART�O, ser�o realizados pelo integrador; Se FALSE, esses cadastros ser�o realizados pelo sistema.''';
  Configuracao.configuracoes_id := 18;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA GAVETA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Indica se o sistema est� configurado para utilizar gaveta no caixa.''';
  Configuracao.configuracoes_id := 19;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'GAVETA COM SINAL INVERTIDO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, indica que a gaveta est� com sinal invertido.''';
  Configuracao.configuracoes_id := 20;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'MENSAGEM FECHAMENTO DA VENDA';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'VOLTE SEMPRE';
  Configuracao.config_obs       := '''Mensagem Que Ser� Impressa no Final do Cupom (Obs: Aceita Apenas 40 Caracteres).''';
  Configuracao.configuracoes_id := 21;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'LIBERAR CLIENTE NO FINAL DA VENDA?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 22;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'EXIBE PERGUNTA: DESEJA ATIVAR PROMO��O NO IN�CIO DA VENDA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ao iniciar a venda, o operador dever� informar se deseja ativar os pre�os promocionais ao iniciar a venda, se FALSE, n�o ser� exibido a pergunta ao operador.''';
  Configuracao.configuracoes_id := 23;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA ALTERAR PARCELAS?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 24;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SEMPRE EXIGIR QUANTIDADE?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, sempre exigir� a quantidade a ser vendida, se FALSE, exigir� a quantidade somente para produtos com unidade fracion�ria.''';
  Configuracao.configuracoes_id := 25;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA CANCELAR ITEM?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 26;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'VALOR TOTAL M�XIMO DO ITEM PERMITIDO';
  Configuracao.config_tipo      := 4;
  Configuracao.config_valor     := '0,00';
  Configuracao.config_obs       := '''Informa o Valor total m�ximo do item que poder� ser vendido. Informe 0 (Zero) para vender itens de qualquer valor.''';
  Configuracao.configuracoes_id := 27;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ARREDONDAR PARCELAS AUTOMATICAMENTE?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, ao gerar credi�rio, as parcelas ser�o arredondadas automaticamente.''';
  Configuracao.configuracoes_id := 28;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA REALIZAR ABERTURA DE CAIXA?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 29;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA CANCELAR CUPOM?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 30;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA APLICAR DESCONTO MAIOR QUE O PERMITIDO?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 31;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'HABILITAR NOTA MANUAL?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se FALSE, o Bot�o Nota Manual Ser� Desabilitado Na Tela de Fechamento de Vendas.''';
  Configuracao.configuracoes_id := 32;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'HOST DO BANCO SERVIDOR.';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'LOCALHOST';
  Configuracao.config_obs       := '''Informa o HOST do banco do Servidor (DB_PDV_DAV_PV.FDB)''';
  Configuracao.configuracoes_id := 33;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'DIRET�RIO DO BANCO DO SERVIDOR.';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\FACIL\Facil PDV\DB\DB_PDV_DAV_PV.FDB';
  Configuracao.config_obs       := '''Informa o diret�rio do banco do servidor (DB_PDV_DAV_PV.FDB)''';
  Configuracao.configuracoes_id := 34;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA SISTEMA OFF LINE?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, todas as consultas, EXCETO DAV, PR� VENDAS e NFE, ser�o realizadas no banco LOCAL do caixa; Se FALSE, todas as consultas ser�o realizadas no banco do SERVIDOR, por�m, os cupom e redu��es z ser�o sempre gravas no banco local.''';
  Configuracao.configuracoes_id := 35;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA REALIZAR FECHAMENTO PARCIAL?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 36;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA REALIZAR FECHAMENTO TOTAL?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 37;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'REALIZAR CADASTRO DE MODALIDADES?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE ser� possivel realizar cadastro de modalidades no F�cil PDV.''';
  Configuracao.configuracoes_id := 38;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'REALIZAR CADASTRO DE AL�QUOTAS?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE ser� poss�vel realizar cadastro de Al�quotas no F�cil PDV.''';
  Configuracao.configuracoes_id := 39;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'IMPRIMIR C�DIGO DO PRODUTO NO CUPOM?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'False';
  Configuracao.config_obs       := '''Se TRUE ser� imppresso o c�digo do produto no cupom, caso contr�rio ser� impresso o c�digo de barras.''';
  Configuracao.configuracoes_id := 40;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA SISTEMA COM TELA CHEIA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'True';
  Configuracao.config_obs       := '''Se TRUE, Abrir� em modo de Tela Cheia.''';
  Configuracao.configuracoes_id := 41;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ABRIR TELA DE CONSULTA AO DIGITAR LETRAS?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'True';
  Configuracao.config_obs       := '''Se TRUE, abre a tela de consulta ao digitar qualquer Letra.''';
  Configuracao.configuracoes_id := 42;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA CONSULTAR PRODUTOS(F10)??';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 43;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TEMPO (EM SEGUNDOS) QUE SER� LIMPADA A TELA DE CONSULTA DE PRODUTOS.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '5';
  Configuracao.config_obs       := '''Tempo (em segundos) que ser� Limpada/Fechada a tela de Consulta de Produtos.''';
  Configuracao.configuracoes_id := 44;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'FECHAR TELA DE CONSULTA AUTOMATICAMENTE.';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, a tela de Consulta de Produtos (Alt+C) ser� fechada automaticamente de acordo com a Configura��o "Fechar Tela Consulta (Seg)". ''';
  Configuracao.configuracoes_id := 45;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'INICIAR VENDA COM ENTER?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, a venda ser� iniciada apenas ao pressionar ENTER, caso contr�rio ser� necess�rio inserir o c�digo do produto e pressionar ENTER". ''';
  Configuracao.configuracoes_id := 46;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TEMA DA TELA DE VENDA.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''Informa o Tema da Tela de Venda:''||ascii_char(13)||ascii_char(10)||'+
                                   '''   0 - Tela Padrao ''||ascii_char(13)||ascii_char(10)||'+
                                   '''   1 - Tela Azul   ''||ascii_char(13)||ascii_char(10)||'+
                                   '''   2 - Tela Claro  ''||ascii_char(13)||ascii_char(10)||'+
                                   '''   3 - Tela Marrom ''';
  Configuracao.configuracoes_id := 47;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);


  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TEMPO DE ESPERA DA TELA FECHAMENTO.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '5';
  Configuracao.config_obs       := '''Tempo Estimado Para Fechamento da Tela de Fechamento Quando Possuir Troco.''';
  Configuracao.configuracoes_id := 48;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);


  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA TELA DE ESPERA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se True a Tela de Espera Ser� Chamada, Caso Contr�rio N�o Ser� Chamada a Tela. Esta Configura��o � Utilizada Somente na Tela de Vendas''';
  Configuracao.configuracoes_id := 49;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TEMPO PARA ATIVAR TELA DE ESPERA.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '60';
  Configuracao.config_obs       := '''Tempo Para Chamar Tela de Espera Ap�s a Inatividade na Tela de Vendas.''';
  Configuracao.configuracoes_id := 50;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'TEMPO DE TRANSI��O DE IMAGENS.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '30';
  Configuracao.config_obs       := '''Tempo em Que as Imagens da Tela de Espera Ser�o Trocadas.''';
  Configuracao.configuracoes_id := 51;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'EXIBIR NO CUPOM IMFORMA��ES COMPACTAS SOBRE IMPOSTOS?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se True Ser�o Exibidas Informa��es Compactas, Caso Contr�rio Ser� Exibida a Informa��o Completa''';
  Configuracao.configuracoes_id := 52;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'EXIBIR ALERTA SOBRE ARTIGO 97?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se True, Enquanto o Usu�rio N�o Gerar os Arquivos Referentes ao Artigo 97, Ser� Exibida uma Mensagem de Alerta Ao Acessar o Sistema, .''';
  Configuracao.configuracoes_id := 53;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'IMPRIMIR COMPROVANTE ADICIONAL NO CREDI�RIO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ser� impresso ao final da venda, o comprovante para o cliente assinar, se FALSE, o comprovante n�o ser� impresso''';
  Configuracao.configuracoes_id := 54;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'C�DIGO UTILIZADO NA VENDA?';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := '''C�DIGO DO PRODUTO UTILIZADO NA VENDA:''||ascii_char(13)||ascii_char(10)||'+
                                   '''   0 - C�digo ''||ascii_char(13)||ascii_char(10)||'+
                                   '''   1 - C�digo de Barras ''||ascii_char(13)||ascii_char(10)||'+
                                   '''   2 - Ambos * ''||ascii_char(13)||ascii_char(10)||'+
                                   ''' * Para o c�digo 2 - Ambos, caso o tamanho do c�digo do produto seja inferior a 6 caracteres ''' +
                                   ''' ser� considerado o c�digo do produto, caso o tamanho do c�digo seja superior a 6 caracteres ''' +
                                   ''' ser� considerado o c�digo de barras do produto''';

  Configuracao.configuracoes_id := 55;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA SENHA COM C�D. BARRAS (GERENTES)?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, Nas Telas de Valida��o de Funcion�rios, o Foco Ficar� na Aba AUTO, Se FALSE, Ficar� na Aba Login.''';
  Configuracao.configuracoes_id := 56;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ABRIR CONSULTA (F10) PARA PRODUTO N�O ENCONTRADO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, Caso o C�digo Digitado Pelo Usu�rio N�o Seja Encontrado, Ser� Aberta a Tela de Consulta (F10); ''' +
                                   ''' Caso Contr�rio, Ser� Exibida Uma Mensagem Para o Usu�rio: Produto N�o Encontrado.''';
  Configuracao.configuracoes_id := 57;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR CONSULTA AO DIGITAR (F10)?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, NA TELA DE CONSULTA DE PRODUTOS F(10), O USU�RIO PODER� CONSULTAR AO DIGITAR; ''' +
                                   '''SE FALSE, A CONSULTA SER� REALIZADA SOMENTE AO APERTAR ENTER''';
  Configuracao.configuracoes_id := 58;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'IMPRIMIR RELAT�RIO GERENCIAL AO FECHAR MOVIMENTO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''SE TRUE, AO FECHAR MOVIMENTO, SER� IMPRESSO UM RELAT�RIO GERENCIAL COM A SITUA��O DO CAIXA.''';
  Configuracao.configuracoes_id := 59;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ACERTAR PRE�O BALAN�A COM ACR�SCIMO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''SE TRUE, QUANDO HOUVER DIFEREN�A NO PRE�O DE BALAN�A � DADO UM ACR�SCIMO ACERTANDO O MESMO.''';
  Configuracao.configuracoes_id := 60;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'INFORMAR ENDERE�O CLIENTE NO COMPROVANTE CREDI�RIO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''SE TRUE, SER� INFORMADO O ENDERE�O DO CLIENTE NO COMPROVANTE ADICIONAL QUE � IMPRESSO PARA O CLIENTE ASSINAR.''';
  Configuracao.configuracoes_id := 61;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'MODELO BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := '''MARCA DA BALAN�A CHEKOUT A SER UTILIZADA.''';
  Configuracao.configuracoes_id := 62;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'PORTA SERIAL BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := '''PORTA SERIAL ONDE A BALAN�A CHECK-OUT SER� PROCURADA.''';
  Configuracao.configuracoes_id := 63;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'HANDSHAKING BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''HANDSHAKING BALAN�A CHECK-OUT.''';
  Configuracao.configuracoes_id := 64;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'BAUD RATE BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '6';
  Configuracao.config_obs       := '''BAUD RATE BALAN�A CHECK-OUT.''';
  Configuracao.configuracoes_id := 65;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'DATA BITS BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '3';
  Configuracao.config_obs       := '''DATA BITS BALAN�A CHECK-OUT.''';
  Configuracao.configuracoes_id := 66;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'PARITY BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''PARITY BALAN�A CHECK-OUT.''';
  Configuracao.configuracoes_id := 67;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'STOP BITS BALAN�A CHECK-OUT.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''STOP BITS BALAN�A CHECK-OUT''';
  Configuracao.configuracoes_id := 68;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZA BALAN�A CHECKOUT?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''SE TRUE, SER� BUSCADO O PESO NA BALAN�A CHECK-OUT, CASO CONTR�RIO SER� SOLICITADO A QUANTIDADE NORMALMENTE.''';
  Configuracao.configuracoes_id := 69;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'PERMITE CREDI�RIO PARA CONSUMIDOR FINAL?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''SE TRUE, SER� POSS�VEL REALIZAR VENDAS NO CREDI�RIO PARA O CONSUMIDOR FINAL.''';
  Configuracao.configuracoes_id := 70;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'VALIDAR GAVETA AO INICIAR A VENDA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se est� configura��o estiver marcada, o sistema valida se a gaveta est� aberta antes de iniciar a venda; Logo, o usu�rio n�o conseguir� iniciar a venda com a gaveta aberta;''';
  Configuracao.configuracoes_id := 71;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR C�DIGO DO SISTEMA NA ETIQUETA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se est� configura��o estiver desmarcada, o sistema vai pesquisar o produto pelo c�digo de barras no banco de dados; Se estiver marcada, vai pesquisar pelo campo produtos_id!''';
  Configuracao.configuracoes_id := 72;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  // Esta configura��o N�O VAI SER EXIBIDA NA UNIT UTCONFIGURACOES
  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR CONTROLE DE PORTA DO ACBR?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''SE TRUE, A CADA COMANDO ENVIADO, O ECF SER� DESCONECTADO DA PORTA; SE FALSE, FICAR� CONECTADO SEMPRE!''';
  Configuracao.configuracoes_id := 73;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'UTILIZAR PERCENTUAL NO DESCONTO COMO PADR�O?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''SE TRUE, O DESCONTO VIR� SELECIONADO O CHEQUEBOX DE PORCENTAGEM COMO PADR�O!''';
  Configuracao.configuracoes_id := 74;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'C�DIGO DO �LTIMO CUPOM IMPORTADO';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''Indica o cupons_id do ultimo cupom importado.''';
  Configuracao.configuracoes_id := 75;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'SENHA PARA CANCELAMENTO DE CUPOM J� IMPORTADO?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 76;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'EXIGE SENHA PARA DESABILITAR BALAN�A?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 77;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'ENVIAR ENTER AUTOMATICAMENTE AP�S CONSULTA?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''SE TRUE, AP�S SELECIONAR O PRODUTO NA CONSULTA, O MESMO SER� VENDIDO AUTOMATICAMENTE;''' +
                                   '''SE FALSE, AO SELECIONAR O PRODUTO NA CONSULTA, O MESMO FICAR� SELECIONADO NA TELA DE VENDAS, MAS N�O SER� ''' +
                                   '''VENDIDO, O USU�RIO PODER� ALTERAR O MESMO, OU ENT�O APERTAR ENTER PARA VEND�-LO''';
  Configuracao.configuracoes_id := 78;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'IGNORAR PARCELAMENTO EM CREDI�RIO?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se True, ao inserir o a modalidade tipo credi�rio, o sistema ira pular a tela de parcelamento, ''' +
                                   '''e poder� alterar a data e valores apenas no bot�o Alterar (F3)''';
  Configuracao.configuracoes_id := 79;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);


  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'PORTA PINPAD?';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'AUTO_USB';
  Configuracao.config_obs       := '''Informa a Porta do PINPAD:''||ascii_char(13)||ascii_char(10)||'+
                                   '''AUTO_USB''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM2''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM3''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM4''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM5''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM6''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM7''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM8''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM9''||ascii_char(13)||ascii_char(10)||'+
                                   '''COM10''';
  Configuracao.configuracoes_id := 80;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'MENSAGEM PINPAD?';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'FACIL SISTEMAS';
  Configuracao.config_obs       := '''MENSAGEM QUE SER� EXIBIDA NO PINPAD''';
  Configuracao.configuracoes_id := 81;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'IP SERVIDOR SITEF?';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'LOCALHOST';
  Configuracao.config_obs       := '''IP DO SERVIDOR SITEF''';
  Configuracao.configuracoes_id := 82;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'N�MERO DA LOJA?';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := '00000000';
  Configuracao.config_obs       := '''N�MERO DA LOJA''';
  Configuracao.configuracoes_id := 83;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'N�MERO DO TERMINAL?';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'SE000001';
  Configuracao.config_obs       := '''N�MERO DO TERMINAL''';
  Configuracao.configuracoes_id := 84;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'GRAVAR LOG?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''GRAVAR ARQUIVO DE LOG''';
  Configuracao.configuracoes_id := 85;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Solicitar Quantidade Ap�s Pesquisa (F10)?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''SE TRUE, AP�S REALIZAR A CONSULTA DE PRODUTOS, SER� SOLICITADO A QUANTIDADE DO PRODUTO''';
  Configuracao.configuracoes_id := 86;
  Configuracao.config_alterada  := False;
  Configuracao.config_readonly  := False;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Arquivo de resposta Tef Dial';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\TEF_DIAL\resp\intpos.001';
  Configuracao.config_obs       := '''Caminho do arquivo de resposta TefDial''';
  Configuracao.configuracoes_id := 87;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Arquivo de requisi��o Tef Dial';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\TEF_DIAL\req\intpos.001';
  Configuracao.config_obs       := '''Caminho do arquivo de requisi��o TefDial''';
  Configuracao.configuracoes_id := 88;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Arquivo de STS Tef Dial';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\TEF_DIAL\resp\intpos.sts';
  Configuracao.config_obs       := '''Caminho do arquivo STS TefDial''';
  Configuracao.configuracoes_id := 89;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Arquivo de Tempor�rio Tef Dial';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\TEF_DIAL\req\intpos.tmp';
  Configuracao.config_obs       := '''Caminho do arquivo Tempor�rio TefDial''';
  Configuracao.configuracoes_id := 90;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Nome do Gerenciador Padr�o Tef Dial';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := 'C:\TEF_DIAL\req\intpos.tmp';
  Configuracao.config_obs       := '''Caminho e Nome do Gerenciador Padr�o Tef Dial''';
  Configuracao.configuracoes_id := 91;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'N�meros de vias do comprovante Tef';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '2';
  Configuracao.config_obs       := '''Informa o n�meros de vias do comprovante Tef''';
  Configuracao.configuracoes_id := 92;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Gerar LOG do ECF?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, vai gravar um arquivo .txt no diret�rio LOGS_ECF de todas as instru��es enviadas para o ECF''';
  Configuracao.configuracoes_id := 93;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Desativar ACBR para gerar relat�rios do ECF?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, vai desativar o ACBR ao gerar os relat�rios do ECF''';
  Configuracao.configuracoes_id := 94;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Utiliza pre�o de atacado?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, o pre�o de atacado e a quantidade m�nima para ativar o pre�o ''||ascii_char(13)||ascii_char(10)||'+
                                   '''ser�o exibidos nas telas de consulta de produtos!''';
  Configuracao.configuracoes_id := 95;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Promo��o sobrep�e pre�o de atacado?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, o pre�o promocional vai sobrepor o pre�o de atacado quando houver!''';
  Configuracao.configuracoes_id := 96;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Validar �ltima Redu��o z Emitida?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, ainda durante a tela de splash, o sistema verificar� se a ''||ascii_char(13)||ascii_char(10)||'+
                                   '''�ltima redu��o emitda pelo ECf foi gravada no banco de dados!''';
  Configuracao.configuracoes_id := 97;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Utiliza Consulta ao Digitar (Clientes)?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, no fechamento de vendas, ser� utilizado a tela de consulta padr�o do sistema!''';
  Configuracao.configuracoes_id := 98;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Validar Estoque das Mat�rias Primas?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ao vender um produto composto, ser� validado o estoque das mat�rias primas do item vendido ''||ascii_char(13)||ascii_char(10)||'+
                                   ''' se FALSE, ser� validado somente o estoque do item vendido!''';
  Configuracao.configuracoes_id := 99;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Validar Pagamentos no Fechamento do Cupom?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, os pagamentos ser�o validados com o valor l�quido do cupom.''';
  Configuracao.configuracoes_id := 100;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Validar Estoque dos itens do Kit?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se FALSE, se o sistema estiver configurado para n�o permitir estoque negativo, o sistema N�O VAI VALIDAR o estoque dos itens do kit.''';
  Configuracao.configuracoes_id := 101;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Senha para vender kit sem estoque?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 102;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Utiliza grade de produtos?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ao vender um produto de grade, ser� exibido uma tela para o usu�rio selecionar a grade a ser vendida.''';
  Configuracao.configuracoes_id := 103;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Exige NSU?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, nos pagamentos nas modalidades CART�O que n�o exigirem TEF, o usu�rio ser� obrigado informar o NSU.''';
  Configuracao.configuracoes_id := 104;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Utilizar nome fiscal na impress�o do cupom?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, ser� impresso o nome fiscal do produto no cupom, caso contr�rio, ser� impresso o nome completo do produto.''';
  Configuracao.configuracoes_id := 105;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Bloquear vendas ap�s 00:00 horas?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, antes de iniciar a venda, ser� validado a data do movimento ECF para exigir a emiss�o da redu��o Z ap�s 00:00 horas.''';
  Configuracao.configuracoes_id := 106;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Permite Utilizar Tela de Consulta (F10)?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'True';
  Configuracao.config_obs       := '''Se FALSE, N�O SER� PERMITIDO utilizar a tela de consulta de produtos (F10)''';
  Configuracao.configuracoes_id := 107;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Realizar valida��o do movimento ap�s Redu��o Z?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'True';
  Configuracao.config_obs       := '''Se TRUE, ap�s a Redu��o Z, ser� gerado 1 arquivo TDM e ser� executado o F�cil Utilit�rios para validar o movimento do dia''';
  Configuracao.configuracoes_id := 108;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Enviar informa��es do ECF para F�cil Sistemas?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, a cada 20 dias, as informa��es do ECF ser�o enviadas para F�cil Sistemas''';
  Configuracao.configuracoes_id := 109;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Sair da tela de Produto N�o Encontrado com Qualquer Tecla?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, ao pressionar qualquer tecla, a tela de Produto N�o Encontrado ser� fechada.''';
  Configuracao.configuracoes_id := 110;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Data Hora da Atualiza��o Produtos';
  Configuracao.config_tipo      := 6;
  Configuracao.config_valor     := Now;
  Configuracao.config_obs       := '''Data Hora da Atualiza��o do Banco Produtos.''';
  Configuracao.configuracoes_id := 111;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Data Hora da Atualiza��o Cleinte';
  Configuracao.config_tipo      := 6;
  Configuracao.config_valor     := Now;
  Configuracao.config_obs       := '''Data Hora da Atualiza��o do Banco Cliente.''';
  Configuracao.configuracoes_id := 112;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Mant�m na base de dados o IDPAI';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := '''Mant�m na base de dados o IDPAI''';
  Configuracao.configuracoes_id := 113;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Caminho do Comunicador Web-Service.';
  Configuracao.config_tipo      := 3;
  Configuracao.config_valor     := '';
  Configuracao.config_obs       := '''Caminho do Comunicador Web-Service.''';
  Configuracao.configuracoes_id := 114;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Destacar Valor do Desconto no Cupom Fiscal?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, quando houver desconto, ser� impresso a mensagem VOC� ECONOMIZOU R$ XX,XX''';
  Configuracao.configuracoes_id := 115;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Exigir senha para aplicar acr�scimo?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '1';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 116;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Exigir senha para alterar a quantidade a ser vendida?';
  Configuracao.config_tipo      := 5;
  Configuracao.config_valor     := '0';
  Configuracao.config_obs       := ObsSenha;
  Configuracao.configuracoes_id := 117;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Permitir estoque negativo para Kits?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'TRUE';
  Configuracao.config_obs       := '''Se TRUE, na venda de Kits, o usu�rio poder�, atrav�s de senha, se configurada, vender produtos sem estoque''';
  Configuracao.configuracoes_id := 118;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Data Hora da Atualiza��o Scanntech';
  Configuracao.config_tipo      := 6;
  Configuracao.config_valor     := '09/02/2018 16:00:56';
  Configuracao.config_obs       := '''Data Hora da Atualiza��o Scanntech''';
  Configuracao.configuracoes_id := 119;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Utiliza m�dulo F�cil Scanntech?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, durante a venda ser�o feitas vefica��es das promo��es da Scanntech.''';
  Configuracao.configuracoes_id := 120;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Sistema utilizado.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := 0;
  Configuracao.config_obs       := '''Sistema utilizado.''';
  Configuracao.configuracoes_id := 121;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Ambiente NFC-e.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := 1;
  Configuracao.config_obs       := '''Ambiente para transmiss�o de NFC-e.''';
  Configuracao.configuracoes_id := 122;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Tipo de impress�o.';
  Configuracao.config_tipo      := 1;
  Configuracao.config_valor     := 4;
  Configuracao.config_obs       := '''Tipo de impress�o NFC-e: 0 -  Danfe NFC-e ou 1 - Danfe NFC-e em mensagem eletr�nica''';
  Configuracao.configuracoes_id := 123;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := 'Emiss�o em conting�ncia Offline?';
  Configuracao.config_tipo      := 2;
  Configuracao.config_valor     := 'FALSE';
  Configuracao.config_obs       := '''Se TRUE, Emite os cupons em conting�ncia Offline.''';
  Configuracao.configuracoes_id := 124;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);

  {
  Configuracao := TConfiguracoes_VO.Create;
  Configuracao.config_nome      := ;
  Configuracao.config_tipo      := ;
  Configuracao.config_valor     := ;
  Configuracao.config_obs       := ;
  Configuracao.configuracoes_id := ;
  Configuracao.config_alterada  := false;
  Configuracao.config_readonly  := false;
  Result.Add(Configuracao);
  }
end;

end.
