//******************************************************************************
// AS CONFIGURA��ES SEMPRE SER�O CONSULTADAS NO BANCO LOCAL DO CAIXA
//******************************************************************************

unit UConfiguracoes_DAO;

interface

uses
  Generics.Collections, UConfiguracoes_VO, System.SysUtils, FireDAC.Comp.Client,
  UConexoes, UPadrao_DAO;

type

  TConfiguracoes_DAO = class (TPadrao_DAO)
    function GetConfiguracoes                                    : TList<TConfiguracoes_VO>;
    function GetConfiguracao  (ConfiguracoesID : Integer)        : TConfiguracoes_VO;
    function SalvaConfiguracao(Lista : TList<TConfiguracoes_VO>) : Boolean;
    procedure Insert           (Configuracao :TConfiguracoes_VO);
    procedure Update(Configuracao : TConfiguracoes_VO);
    procedure UpdateValor(configuracoes_id : Integer; valor : Variant; tipo : Integer);
  end;

implementation

{ TConfiguracoes_DAO }

function TConfiguracoes_DAO.GetConfiguracao(ConfiguracoesID : Integer): TConfiguracoes_VO;
var
  qryTemp       : TFDQuery;
  Configuracoes : TConfiguracoes_VO;
begin
  try
    try
      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(Self.BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT *                                     ');
        SQL.Add('  FROM CONFIGURACOES C                       ');
        SQL.Add('WHERE C.CONFIGURACOES_ID = :pConfiguracoes_ID');

        ParamByName('pConfiguracoes_ID').AsInteger := ConfiguracoesID;

        Open;

        if not IsEmpty then
        begin
          Configuracoes := TConfiguracoes_VO.Create;

          Configuracoes.configuracoes_id := FieldByName('CONFIGURACOES_ID').AsInteger;
          Configuracoes.config_nome      := FieldByName('CONFIG_NOME'     ).AsString;
          Configuracoes.config_tipo      := FieldByName('CONFIG_TIPO'     ).AsInteger;
          Configuracoes.config_valor     := FieldByName('CONFIG_VALOR'    ).AsVariant;
          Configuracoes.config_obs       := FieldByName('CONFIG_OBS'      ).AsString;

        end;

        Result := Configuracoes;
      end;

    except
      on E : Exception do
      begin
        Result := Nil;

        if(Assigned(Configuracoes))then
        begin
          FreeAndNil(Configuracoes);
        end;

        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);

    FreeAndNil(qryTemp);
  end;

end;

function TConfiguracoes_DAO.GetConfiguracoes: TList<TConfiguracoes_VO>;
var
    qryTemp       : TFDQuery                ;
    Configuracoes : TConfiguracoes_VO       ;
    Lista         : TList<TConfiguracoes_VO>;
begin
  try
    try
      Lista := Nil;

      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(Self.BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT *                       ');
        SQL.Add('  FROM CONFIGURACOES C         ');
        SQL.Add('  ORDER BY C.CONFIGURACOES_ID  ');

        Open;

        if not IsEmpty then
        begin
          Lista := TList<TConfiguracoes_VO>.Create;

          while not Eof do
          begin
            Configuracoes := TConfiguracoes_VO.Create;

            Configuracoes.configuracoes_id := FieldByName('CONFIGURACOES_ID').AsInteger;
            Configuracoes.config_nome      := FieldByName('CONFIG_NOME'     ).AsString;
            Configuracoes.config_tipo      := FieldByName('CONFIG_TIPO'     ).AsInteger;
            Configuracoes.config_valor     := FieldByName('CONFIG_VALOR'    ).AsVariant;
            Configuracoes.config_obs       := FieldByName('CONFIG_OBS'      ).AsString;
            Configuracoes.config_readonly  := Boolean(FieldByName('READONLY').AsInteger);

            Lista.Add(Configuracoes);

            Next;
          end;
        end;
      end;
    except
      on E:Exception do
      begin
        Lista := Nil;
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    Result := Lista;
    TConexoes.FechaConexao(qryTemp);
    FreeAndNil(qryTemp);
  end;
end;

procedure TConfiguracoes_DAO.Insert(Configuracao: TConfiguracoes_VO);
var
  qryTemp : TFDQuery;
begin
  try
    try
      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(Self.BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('   INSERT INTO CONFIGURACOES (                   ');
        SQL.Add('                              CONFIGURACOES_ID,  ');
        SQL.Add('                              CONFIG_NOME,       ');
        SQL.Add('                              CONFIG_TIPO,       ');
        SQL.Add('                              CONFIG_VALOR,      ');
        SQL.Add('                              CONFIG_OBS,        ');
        SQL.Add('                              READONLY           ');
        SQL.Add('                              ) VALUES (         ');
        SQL.Add('                              :pCONFIGURACOES_ID,');
        SQL.Add('                              :pCONFIG_NOME,     ');
        SQL.Add('                              :pCONFIG_TIPO,     ');
        SQL.Add('                              :pCONFIG_VALOR,    ');

        SQL.Add(                               Configuracao.config_obs+',');

        SQL.Add('                              0                  ');
        SQL.Add('                              );  		            ');

        ParamByName('pCONFIGURACOES_ID').AsInteger  := Configuracao.configuracoes_id;
        ParamByName('pCONFIG_NOME'     ).AsString   := Configuracao.config_nome;
        ParamByName('pCONFIG_TIPO'     ).AsInteger  := Configuracao.config_tipo;
        ParamByName('pCONFIG_VALOR'    ).Value      := Configuracao.config_valor;

        ExecSQL;
      end;
    except
      on e : Exception do
      begin
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);
    FreeAndNil(qryTemp);
  end;       
end;

function TConfiguracoes_DAO.SalvaConfiguracao(
  Lista: TList<TConfiguracoes_VO>): Boolean;
var
  qryTemp : TFDQuery;
  I       : Integer;
begin
  try
    try
      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      for I := 0 to Lista.Count - 1 do
      begin
        if(Lista[I].config_alterada)then
        begin
          with qryTemp do
          begin
            Close;

            SQL.Clear;
            SQL.Add('UPDATE CONFIGURACOES                  ');
            SQL.Add('   SET CONFIG_VALOR = :pValor         ');
            SQL.Add('WHERE (CONFIGURACOES_ID = :pConfigID);');

            ParamByName('pConfigID').AsInteger := Lista[I].configuracoes_id    ;

            if(Lista[I].config_tipo = 2)then
            begin
              if(Lista[I].config_valor = 'TRUE')then
                ParamByName('pValor').AsString := 'TRUE'
              else
                ParamByName('pValor').AsString := 'FALSE';
            end
            else
              ParamByName('pValor'   ).Value     := Lista[I].config_valor        ;

            ExecSQL;
          end;
        end;
      end;
      Result := True;
    except
      on E:Exception do
      begin
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
        Result := False;
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);
    FreeAndNil(qryTemp);
  end;
end;

procedure TConfiguracoes_DAO.Update(Configuracao: TConfiguracoes_VO);
var
  qryTemp : TFDQuery;
begin
  try
    try
      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('UPDATE CONFIGURACOES                  ');
        SQL.Add('   SET CONFIG_VALOR = :pValor         ');
        SQL.Add('WHERE (CONFIGURACOES_ID = :pConfigID);');

        ParamByName('pConfigID').AsInteger := Configuracao.configuracoes_id;

        if(Configuracao.config_tipo = 2)then
        begin
          if(Configuracao.config_valor = 'TRUE')then
            ParamByName('pValor').AsString := 'TRUE'
          else
            ParamByName('pValor').AsString := 'FALSE';
        end
        else
          ParamByName('pValor').Value := Configuracao.config_valor;

        ExecSQL;
      end;
    except
      on e : Exception do
      begin
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);

    FreeAndNil(qryTemp);
  end;
end;

procedure TConfiguracoes_DAO.UpdateValor(configuracoes_id: Integer;
  valor: Variant; tipo : Integer);
var
  qryTemp : TFDQuery;
begin
  try
    try
      qryTemp            := TFDQuery.Create(Nil);
      qryTemp.Connection := TConexoes.AbreNovaConexao(BANCO);

      with qryTemp do
      begin
        Close;
        SQL.Clear;
        SQL.Add('UPDATE CONFIGURACOES                  ');
        SQL.Add('   SET CONFIG_VALOR = :pValor         ');
        SQL.Add('WHERE (CONFIGURACOES_ID = :pConfigID);');

        ParamByName('pConfigID').AsInteger := configuracoes_id;

        if(tipo = 2)then
        begin
          if(valor = 'TRUE')then
            ParamByName('pValor').AsString := 'TRUE'
          else
            ParamByName('pValor').AsString := 'FALSE';
        end
        else
          ParamByName('pValor').Value := valor;

        ExecSQL;
      end;
    except
      on e : Exception do
      begin
        raise Exception.Create('Erro ao Executar Instru��o SQL' + #13 + e.Message);
      end;
    end;
  finally
    TConexoes.FechaConexao(qryTemp);

    FreeAndNil(qryTemp);
  end;
end;

end.
