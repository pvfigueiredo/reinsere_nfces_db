unit UConfiguracoes_VO;
//Classe replica os campos da tabela Configurações do banco de dados do PAF_ECF 25/07/2014;


interface

uses UPadrao_VO;


type

 TConfiguracoes_VO = class
 private
   Fconfiguracoes_id : Integer;
   Fconfig_nome      : String ;
   Fconfig_tipo      : Integer;
   Fconfig_valor     : Variant ;
   Fconfig_obs       : String ;
   Fconfig_alterou   : Boolean;
   Fconfig_readonly  : Boolean;

 public

   property configuracoes_id : Integer read Fconfiguracoes_id write Fconfiguracoes_id;
   property config_nome      : String  read Fconfig_nome      write Fconfig_nome     ;
   {$REGION 'Documentation Field'}
      /// <remarks>
      /// <para><b>Tipo do Valor</b>:</para>
      /// <list type="bullet">
      /// <item>1 - Inteiro        </item>
      /// <item>2 - Boolean        </item>
      /// <item>3 - String         </item>
      /// <item>4 - Real           </item>
      /// <item>5 - Senha          </item>
      /// <item>6 - Data Hora      </item>
      /// </list>
      /// </remarks>
   {$ENDREGION}
   property config_tipo      : Integer read Fconfig_tipo      write Fconfig_tipo     ;
   property config_valor     : Variant read Fconfig_valor     write Fconfig_valor    ;
   property config_obs       : String  read Fconfig_obs       write Fconfig_obs      ;
   {$REGION 'Documentation Field'}
    ///	<value>
    ///	  Propriedade utilisada na tela de Configuração
    ///	</value>
    {$ENDREGION}
   property config_alterada  : Boolean read Fconfig_alterou   write Fconfig_alterou  ;
   property config_readonly  : Boolean read Fconfig_readonly  write Fconfig_readonly ;

end;

implementation

{ TConfiguracoes_VO }



end.
