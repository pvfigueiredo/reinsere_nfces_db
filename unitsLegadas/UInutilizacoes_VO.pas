unit UInutilizacoes_VO;

interface

uses
  Generics.Collections, System.SysUtils;

type
  TInutilizacoes_VO = Class

    private
      Finutilizacoes_id: Integer;
      Finutilizacao_numero_inicial : Integer;
      Finutilizacao_numero_final : Integer;
      Finutilizacao_data : TDateTime;
      Finutilizacao_justificativa : string;
      Finutilizacao_exportada : Boolean;
      Finutilizacao_serie  : Integer;
      Finutilizacao_caixa  : Integer;

    public

      property inutilizacoes_id: Integer read FInutilizacoes_id write FInutilizacoes_id;
      property inutilizacao_numero_inicial: Integer read Finutilizacao_numero_inicial write Finutilizacao_numero_inicial;
      property inutilizacao_numero_final: Integer read Finutilizacao_numero_final write Finutilizacao_numero_final;
      property inutilizacao_data: TDateTime read Finutilizacao_data write Finutilizacao_data;
      property inutilizacao_justificativa: string read Finutilizacao_justificativa write Finutilizacao_justificativa;
      property inutilizacao_exportada: Boolean read Finutilizacao_exportada write Finutilizacao_exportada;
      property inutilizacao_serie: Integer read Finutilizacao_serie write Finutilizacao_serie;
      property inutilizacao_caixa: Integer read Finutilizacao_caixa write Finutilizacao_caixa;

  End;

implementation

end.
